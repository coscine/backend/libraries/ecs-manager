﻿using Coscine.Configuration;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Coscine.ECSManager.Tests
{
    [TestFixture]
    public class EcsManagerTests
    {
        private readonly string _testPrefix = "Coscine-EcsManager-Tests-EcsManager";
        private readonly string _rdskeyPrefix = "coscine/global/rds/ecs-rwth/rds";
        private readonly string _rdsS3KeyPrefix = "coscine/global/rds/ecs-rwth/rds-s3";
        private readonly string _userKeyPrefix = "coscine/global/rds/ecs-rwth/users";

        private EcsManager _rdsEcsManager;
        private EcsManager _rdsS3EcsManager;
        private EcsManager _userEcsManager;

        private long _quota;

        private Guid _guid;
        private string _bucketName;

        private string _userName;
        private string _secretKey;

        private string _readUser;
        private string _writeUser;

        private IEnumerable<string> _readRights;
        private IEnumerable<string> _writeRights;

        [OneTimeSetUp]
        // OneTimeSetUp and OneTimeTearDown should not be async
        public void OneTimeSetUp()
        {
            var configuration = new ConsulConfiguration();

            _rdsEcsManager = new EcsManager
            {
                EcsManagerConfiguration = new EcsManagerConfiguration
                {
                    ManagerApiEndpoint = configuration.GetString($"{_rdskeyPrefix}/manager_api_endpoint"),
                    NamespaceName = configuration.GetString($"{_rdskeyPrefix}/namespace_name"),
                    NamespaceAdminName = configuration.GetString($"{_rdskeyPrefix}/namespace_admin_name"),
                    NamespaceAdminPassword = configuration.GetString($"{_rdskeyPrefix}/namespace_admin_password"),
                    ObjectUserName = configuration.GetString($"{_rdskeyPrefix}/object_user_name"),
                    ReplicationGroupId = configuration.GetString($"{_rdskeyPrefix}/replication_group_id"),
                },
            };

            _rdsS3EcsManager = new EcsManager
            {
                EcsManagerConfiguration = new EcsManagerConfiguration
                {
                    ManagerApiEndpoint = configuration.GetString($"{_rdsS3KeyPrefix}/manager_api_endpoint"),
                    NamespaceName = configuration.GetString($"{_rdsS3KeyPrefix}/namespace_name"),
                    NamespaceAdminName = configuration.GetString($"{_rdsS3KeyPrefix}/namespace_admin_name"),
                    NamespaceAdminPassword = configuration.GetString($"{_rdsS3KeyPrefix}/namespace_admin_password"),
                    ObjectUserName = configuration.GetString($"{_rdsS3KeyPrefix}/object_user_name"),
                    ReplicationGroupId = configuration.GetString($"{_rdsS3KeyPrefix}/replication_group_id"),
                }
            };

            _userEcsManager = new EcsManager
            {
                EcsManagerConfiguration = new EcsManagerConfiguration
                {
                    ManagerApiEndpoint = configuration.GetString($"{_userKeyPrefix}/manager_api_endpoint"),
                    NamespaceName = configuration.GetString($"{_userKeyPrefix}/namespace_name"),
                    NamespaceAdminName = configuration.GetString($"{_userKeyPrefix}/namespace_admin_name"),
                    NamespaceAdminPassword = configuration.GetString($"{_userKeyPrefix}/namespace_admin_password"),
                    ObjectUserName = configuration.GetString($"{_userKeyPrefix}/object_user_name"),
                    ReplicationGroupId = configuration.GetString($"{_userKeyPrefix}/replication_group_id"),
                }
            };

            _quota = 1;
            _guid = Guid.NewGuid();
            _bucketName = $"{_testPrefix}.{_guid}";
            _userName = $"{_testPrefix}.{_guid}";
            _secretKey = "VERY_S3cr3t_Key!!!";
            _readUser = $"{_testPrefix}.read_{_guid}";
            _writeUser = $"{_testPrefix}.write_{_guid}";
            _readRights = new List<string> { "read", "read_acl" };
            _writeRights = new List<string> { "read", "read_acl", "write", "execute", "privileged_write", "delete" };
        }

        [OneTimeTearDown]
        // OneTimeSetUp and OneTimeTearDown should not be async
        public void OneTimeTearDown()
        {
            try
            {
                _rdsEcsManager.DeleteBucket(_bucketName).Wait();
                _rdsS3EcsManager.DeleteBucket(_bucketName).Wait();
                _rdsEcsManager.DeleteObjectUser(_userName).Wait();

                _userEcsManager.DeleteObjectUser(_readUser).Wait();
                _userEcsManager.DeleteObjectUser(_writeUser).Wait();
            }
#pragma warning disable RCS1075 // Avoid empty catch clause that catches System.Exception.
            catch (Exception)
#pragma warning restore RCS1075 // Avoid empty catch clause that catches System.Exception.
            {
            }
        }

        [Test]
        public void EcsManagerTestBucketFunctions()
        {
            _rdsEcsManager.CreateBucket(_bucketName, 1).Wait();
            Assert.True(_rdsEcsManager.DeleteBucket(_bucketName).Result);
        }

        [Test]
        public void EcsManagerTestLargeBucket()
        {
            _rdsEcsManager.CreateBucket(_bucketName, _rdsEcsManager.EcsManagerConfiguration.BucketQuotaMax).Wait();
            Assert.True(_rdsEcsManager.GetBucketQuota(_bucketName).Result == _rdsEcsManager.EcsManagerConfiguration.BucketQuotaMax);
            Assert.True(_rdsEcsManager.GetBucketTotalUsedQuota(_bucketName).Result == 0);
            Assert.True(_rdsEcsManager.SetBucketQuota(_bucketName, _rdsEcsManager.EcsManagerConfiguration.BucketQuotaMax - 1).Result);
            Assert.True(_rdsEcsManager.GetBucketQuota(_bucketName).Result == _rdsEcsManager.EcsManagerConfiguration.BucketQuotaMax - 1);
            Assert.True(_rdsEcsManager.DeleteBucket(_bucketName).Result);

            _rdsS3EcsManager.CreateBucket(_bucketName, _rdsS3EcsManager.EcsManagerConfiguration.BucketQuotaMax).Wait();
            Assert.True(_rdsS3EcsManager.GetBucketQuota(_bucketName).Result == _rdsS3EcsManager.EcsManagerConfiguration.BucketQuotaMax);
            Assert.True(_rdsEcsManager.GetBucketTotalUsedQuota(_bucketName).Result == 0);
            Assert.True(_rdsS3EcsManager.SetBucketQuota(_bucketName, _rdsS3EcsManager.EcsManagerConfiguration.BucketQuotaMax - 1).Result);
            Assert.True(_rdsS3EcsManager.GetBucketQuota(_bucketName).Result == _rdsS3EcsManager.EcsManagerConfiguration.BucketQuotaMax - 1);
            Assert.True(_rdsS3EcsManager.DeleteBucket(_bucketName).Result);
        }

        [Test]
        public void UserRightsTest()
        {
            Assert.False(string.IsNullOrWhiteSpace(_userEcsManager.CreateObjectUser(_readUser, _secretKey).Result.link.href));
            Assert.False(string.IsNullOrWhiteSpace(_userEcsManager.CreateObjectUser(_writeUser, _secretKey).Result.link.href));

            _rdsS3EcsManager.CreateBucket(_bucketName, 1).Wait();

            Assert.True(_rdsS3EcsManager.SetUserAcl(_readUser, _bucketName, _readRights).Result);
            Assert.True(_rdsS3EcsManager.SetUserAcl(_writeUser, _bucketName, _writeRights).Result);

            Assert.True(_rdsS3EcsManager.DeleteBucket(_bucketName).Result);

            Assert.True(_userEcsManager.DeleteObjectUser(_writeUser).Result);
            Assert.True(_userEcsManager.DeleteObjectUser(_readUser).Result);
        }

        [Test]
        public void EcsManagerTestQuotaFunctions()
        {
            _rdsEcsManager.CreateBucket(_bucketName, _quota).Wait();
            Assert.True(_rdsEcsManager.GetBucketQuota(_bucketName).Result == _quota);
            Assert.True(_rdsEcsManager.SetBucketQuota(_bucketName, _quota + 1).Result);
            Assert.True(_rdsEcsManager.GetBucketQuota(_bucketName).Result == _quota + 1);
            Assert.True(_rdsEcsManager.DeleteBucket(_bucketName).Result);
        }

        [Test]
        public void EcsManagerTestObjectUserFunctions()
        {
            Assert.False(string.IsNullOrWhiteSpace(_rdsEcsManager.CreateObjectUser(_userName).Result.link.href));
            Assert.True(_rdsEcsManager.GetObjectUser(_userName).Result.name == _userName.ToLower());
            Assert.True(_rdsEcsManager.UpdateObjectUserKey(_userName, _secretKey).Result.secret_key == _secretKey);
            Assert.True(_rdsEcsManager.DeleteObjectUser(_userName).Result);
        }
    }
}