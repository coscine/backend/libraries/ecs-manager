﻿using Coscine.Configuration;
using System.Threading.Tasks;

namespace Coscine.ECSManager
{
    /// <summary>
    /// Class that contains all the configuration values for the EcsManager.
    /// </summary>
    public class EcsManagerConfiguration
    {
        /// <summary>
        /// Username of the EcsManager
        /// </summary>
        public string ManagerUserName { get; set; }

        /// <summary>
        /// Password of the EcsManager
        /// </summary>
        public string ManagerUserPassword { get; set; }

        /// <summary>
        /// ApiEndpoint of the EcsManager
        /// </summary>
        public string ManagerApiEndpoint { get; set; }

        /// <summary>
        /// Name of the Namespace
        /// </summary>
        public string NamespaceName { get; set; }

        /// <summary>
        /// AdminName of the Namespace
        /// </summary>
        public string NamespaceAdminName { get; set; }

        /// <summary>
        /// Password of the NamespaceAdmin
        /// </summary>
        public string NamespaceAdminPassword { get; set; }

        /// <summary>
        /// Username of the ObjectStorage
        /// </summary>
        public string ObjectUserName { get; set; }

        /// <summary>
        /// Id of the replication group
        /// </summary>
        public string ReplicationGroupId { get; set; }

        /// <summary>
        /// Minimum value for the bucket quota
        /// </summary>
        public int BucketQuotaMin { get; set; } = 1;

        /// <summary>
        /// Maximum value for the bucket quota
        /// </summary>
        public int BucketQuotaMax { get; set; } = 1000000;

        /// <summary>
        /// Initializes all members with values from Consul
        /// <param name="configuration">Configuration Object.</param>
        /// </summary>
        public async Task InitializeConfigurationValues(IConfiguration configuration)
        {
            var ManagerUserNameTask = configuration.GetStringAsync("coscine/global/ecs/manager_user_name");
            var ManagerUserPasswordTask = configuration.GetStringAsync("coscine/global/ecs/manager_user_password");
            var ManagerApiEndpointTask = configuration.GetStringAsync("coscine/global/ecs/manager_api_endpoint");

            var NamespaceNameTask = configuration.GetStringAsync("coscine/global/ecs/namespace_name");
            var NamespaceAdminNameTask = configuration.GetStringAsync("coscine/global/ecs/namespace_admin_name");
            var NamespaceAdminPasswordTask = configuration.GetStringAsync("coscine/global/ecs/namespace_admin_password");

            var ObjectUserNameTask = configuration.GetStringAsync("coscine/global/ecs/object_user_name");

            var ReplicationGroupIdTask = configuration.GetStringAsync("coscine/global/ecs/replication_group_id");

            ManagerUserName = await ManagerUserNameTask;
            ManagerUserPassword = await ManagerUserPasswordTask;
            ManagerApiEndpoint = await ManagerApiEndpointTask;

            NamespaceName = await NamespaceNameTask;
            NamespaceAdminName = await NamespaceAdminNameTask;
            NamespaceAdminPassword = await NamespaceAdminPasswordTask;

            ObjectUserName = await ObjectUserNameTask;

            ReplicationGroupId = await ReplicationGroupIdTask;
        }
    }
}