﻿using ECSManagementSDK.Common;
using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;

namespace Coscine.ECSManager;

/// <inheritdoc />
public class CoscineECSManagementClient : IDisposable
{
    /// <summary>Supports calling dispose multiple times.</summary>
    private bool disposed;

    /// <inheritdoc />
    public string Username { get; }

    /// <inheritdoc />
    public string Password { get; }

    /// <inheritdoc />
    public string EndPoint { get; }

    /// <inheritdoc />
    public string AccessToken { get; private set; }

    private readonly HttpClientHandler _handler;

    /// <summary>
    /// Creates a new instance of the ECSManagementClient class.
    /// </summary>
    /// <param name="userName">The user name used to authenticate with management API.</param>
    /// <param name="password">The password used to authenticate with management API.</param>
    /// <param name="endpoint">The endpoint that represents the location of management API.</param>
    public CoscineECSManagementClient(string userName, string password, string endpoint)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to create an ECSManagementClient with a null or empty userName.");
        password.AssertIsNotNullOrEmpty(nameof(password), "Unable to create an ECSManagementClient with a null or empty password.");
        endpoint.AssertIsNotNullOrEmpty(nameof(endpoint), "Unable to create an ECSManagementClient with a null or empty endpoint.");

        Username = userName;
        Password = password;
        EndPoint = endpoint;

        _handler = new HttpClientHandler();

        ConfigurationManager.AppSettings.Set("TRUST_ALL_SSL_CERTIFICATES", "true");

        if (ConfigurationManager.AppSettings["SSL_SECURITY_PROTOCOL"] != string.Empty && ConfigurationManager.AppSettings["SSL_SECURITY_PROTOCOL"] != null)
            _handler.SslProtocols = (SslProtocols)short.Parse(ConfigurationManager.AppSettings["SSL_SECURITY_PROTOCOL"]);
        if (Convert.ToBoolean(ConfigurationManager.AppSettings["TRUST_ALL_SSL_CERTIFICATES"]))
            _handler.ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;
    }

    /// <inheritdoc />
    public async Task<string> Authenticate()
    {
        string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}", Username, Password)));
        HttpRequestMessage requestMessage = new()
        {
            Method = HttpMethod.Get,
            RequestUri = new Uri(string.Format("{0}/login", EndPoint))
        };
        requestMessage.Headers.Clear();
        requestMessage.Headers.Add("Authorization", "Basic " + credentials);
        HttpClient client = new(_handler)
        {
            Timeout = new TimeSpan(0, 5, 0)
        };
        HttpResponseMessage resp = await client.SendAsync(requestMessage, HttpCompletionOption.ResponseHeadersRead);
        AccessToken = resp.StatusCode == HttpStatusCode.OK || resp.StatusCode == HttpStatusCode.NonAuthoritativeInformation ? resp.Headers.GetValues("X-SDS-AUTH-TOKEN").First() : throw new InvalidOperationException(string.Format("Failed to login. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        string accessToken = AccessToken;
        return accessToken;
    }

    /// <inheritdoc />
    public async Task<bool> LogOut()
    {
        if (AccessToken == null)
            return true;
        HttpRequestMessage requestMessage = new()
        {
            Method = HttpMethod.Get,
            RequestUri = new Uri(string.Format("{0}/logout", EndPoint))
        };
        requestMessage.Headers.Clear();
        requestMessage.Headers.Add("X-SDS-AUTH-TOKEN", AccessToken);
        HttpClient client = new(_handler)
        {
            Timeout = new TimeSpan(0, 5, 0)
        };
        HttpResponseMessage resp = await client.SendAsync(requestMessage, HttpCompletionOption.ResponseHeadersRead);
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to logout. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        const bool result = true;
        AccessToken = null;
        return result;
    }

    /// <inheritdoc />
    public CoscineECSManagementService CreateServiceClient() => new(this, _handler);

    /// <summary>Create a new management service instance.</summary>
    /// <returns>No return value.</returns>
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Implements all resource cleanup logic to be shared between the Dispose method and the optional finalize.
    /// </summary>
    /// <param name="disposing">Indicates whether the method was invoked from the IDisposable.Dispose implementation or from the finalizer.</param>
    protected virtual void Dispose(bool disposing)
    {
        if (disposed)
            return;
        if (disposing)
            LogOut().Wait();
        disposed = true;
    }
}