﻿using DataSchemas;
using ECSManagementSDK;
using ECSManagementSDK.Common;
using EsuApiLib;
using EsuApiLib.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;

namespace Coscine.ECSManager;

/// <summary>Service class for managing API operations.</summary>
public class CoscineECSManagementService
{
    /// <summary>The http client used for making management API calls.</summary>
    private readonly HttpClient _httpClient;

    /// <summary>
    /// The management client used for making management API calls.
    /// </summary>
    public CoscineECSManagementClient Client { get; set; }

    /// <summary>
    /// Creates a new instance of the ECSManagementService class.
    /// </summary>
    /// <param name="client">The ECSManagementClient to use when performing API calls.</param>
    public CoscineECSManagementService(CoscineECSManagementClient client, HttpClientHandler handler = null)
    {
        if (handler == null)
        {
            handler = new HttpClientHandler();
        }

        this.Client = client;
        this._httpClient = new HttpClient(handler);
        this._httpClient.Timeout = new TimeSpan(0, 5, 0);
        this._httpClient.DefaultRequestHeaders.Add("X-SDS-AUTH-TOKEN", this.Client.AccessToken);
        this._httpClient.DefaultRequestHeaders.Add("Accept", "application/xml");
    }

    /// <summary>
    /// Create a Atmos subtenant for the specified object user and secret key.
    /// </summary>
    /// <param name="userName">The username to connect as into the Atmos API.</param>
    /// <param name="host">The hostname or IP of the ECS node.</param>
    /// <param name="port">The port to use when accessing the node (9022/9023).</param>
    /// <returns>A string representing the new Atmos subtenant id.</returns>
    public async Task<string> CreateAtmosSubtenant(string userName, string host, int port)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to create Atmos sub tenant with null or empty userName.");
        host.AssertIsNotNullOrEmpty(nameof(host), "Unable to create Atmos sub tenant with null or empty host.");
        if (port <= 0)
            throw new ArgumentException("Unable to create an Atmos subtenant with the supplied port parameter", nameof(port));
        secretKeyRestRep resp = await this.GetKeysForUser(userName);
        string secretKey = resp.secret_key_1;
        EsuApi esu = (EsuApi)new EsuRestApi(host, port, userName, secretKey);
        if (port == 9023)
            ((EsuRestApi)esu).Protocol = "https";
        string subtenant = esu.CreateSubtenant();
        resp = (secretKeyRestRep)null;
        secretKey = (string)null;
        esu = (EsuApi)null;
        return subtenant;
    }

    /// <summary>
    /// Create a Atmos subtenant for the specified object user and secret key.
    /// </summary>
    /// <param name="userName">The username to connect as into the Atmos API.</param>
    /// <param name="host">The hostname or IP of the ECS node.</param>
    /// <param name="port">The port to use when accessing the node (9022/9023).</param>
    /// <param name="subtenantId">The subtenant to be delted.</param>
    /// <returns>A bool indicating if the subtenant was successfully deleted.</returns>
    public async Task<bool> DeleteAtmosSubtenant(
      string userName,
      string host,
      int port,
      string subtenantId)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to delete Atmos sub tenant with null or empty userName.");
        host.AssertIsNotNullOrEmpty(nameof(host), "Unable to delete Atmos sub tenant with null or empty host.");
        subtenantId.AssertIsNotNullOrEmpty(nameof(subtenantId), "Unable to delete Atmos sub tenant with null or empty subtenantId.");
        if (port <= 0)
            throw new ArgumentException("Unable to delete an Atmos subtenant with the supplied port parameter", nameof(port));
        secretKeyRestRep resp = await this.GetKeysForUser(userName);
        string secretKey = resp.secret_key_1;
        EsuApi esu = (EsuApi)new EsuRestApi(host, port, userName, secretKey);
        if (port == 9023)
            ((EsuRestApi)esu).Protocol = "https";
        esu.DeleteSubtenant(subtenantId);
        bool flag = true;
        resp = (secretKeyRestRep)null;
        secretKey = (string)null;
        esu = (EsuApi)null;
        return flag;
    }

    /// <summary>
    /// Creates and sends an alert event with error logs attached as an aid to troubleshooting customer issues.
    /// </summary>
    /// <param name="userWrittenString">The user written string.</param>
    /// <param name="contactDetails">The contact details.</param>
    /// <returns>A alertRestRep object containing the newly created id.</returns>
    public async Task<alertRestRep> SendAlert(
      string userWrittenString,
      string contactDetails)
    {
        userWrittenString.AssertIsNotNull<string>(nameof(userWrittenString), "Unable to send alert with null or empty userWrittenString argument.");
        contactDetails.AssertIsNotNull<string>(nameof(contactDetails), "Unable to send alert with null or empty contactDetails argument.");
        eventParameters data = new eventParameters()
        {
            user_str = userWrittenString,
            contact = contactDetails
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<eventParameters>(new Uri(string.Format("{0}/vdc/callhome/alert", (object)this.Client.EndPoint)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to send alert. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        alertRestRep payload = await resp.Content.ReadAsAsync<alertRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        alertRestRep alertRestRep = payload;
        data = (eventParameters)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (alertRestRep)null;
        return alertRestRep;
    }

    /// <summary>Configures ESRS Servers.</summary>
    /// <param name="userId">ESRS user id.</param>
    /// <param name="password">ESRS password.</param>
    /// <param name="fqdn">ESRS fully qualified domain name.</param>
    /// <param name="port">ESRS port number.</param>
    /// <returns>A boolean indicating the ESRS servers were configured successfully.</returns>
    public async Task<bool> ConfigureEsrsServers(
      string userId,
      string password,
      string fqdn,
      int port)
    {
        userId.AssertIsNotNull<string>(nameof(userId), "Unable to configure ESRS servers with null or empty userId argument.");
        password.AssertIsNotNull<string>(nameof(password), "Unable to configure ESRS servers with null or empty password argument.");
        fqdn.AssertIsNotNull<string>(nameof(fqdn), "Unable to configure ESRS servers with null or empty fqdn argument.");
        port.AssertIsNotNull<int>(nameof(port), "Unable to configure ESRS servers with null or empty port argument.");
        configEsrsParameters data = new configEsrsParameters()
        {
            userid = userId,
            password = password,
            fqdn = fqdn,
            port = port
        };
        HttpClient httpClient = this._httpClient;
        Uri requestUri = new Uri(string.Format("{0}/vdc/callhome/esrs", (object)this.Client.EndPoint));
        configEsrsParameters configEsrsParameters = data;
        XmlMediaTypeFormatterECS formatter = new XmlMediaTypeFormatterECS();
        formatter.UseXmlSerializer = true;
        HttpResponseMessage resp = await httpClient.PostAsync<configEsrsParameters>(requestUri, configEsrsParameters, (MediaTypeFormatter)formatter, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to configure ESRS servers. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        data = (configEsrsParameters)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>Get list of ESRS server info.</summary>
    /// <returns>A listEsrsInfoRestRep object that represents the list of ESRS servers and corresponding details.</returns>
    public async Task<listEsrsInfoRestRep> GetEsrsInfo()
    {
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/vdc/callhome/esrs", (object)this.Client.EndPoint)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get list of ESRS server info. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        listEsrsInfoRestRep payload = await resp.Content.ReadAsAsync<listEsrsInfoRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        listEsrsInfoRestRep esrsInfo = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (listEsrsInfoRestRep)null;
        return esrsInfo;
    }

    /// <summary>Get ESRS server info.</summary>
    /// <param name="esrsId">The ESRS server id.</param>
    /// <returns>A esrsInfoRestRep object that represents the requested ESRS server.</returns>
    public async Task<esrsInfoRestRep> GetEsrsInfo(string esrsId)
    {
        esrsId.AssertIsNotNull<string>(nameof(esrsId), "Unable to get ESRS server info with null or empty esrsId argument.");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/vdc/callhome/esrs/{1}", (object)this.Client.EndPoint, (object)esrsId)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get ESRS server info. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        esrsInfoRestRep payload = await resp.Content.ReadAsAsync<esrsInfoRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        esrsInfoRestRep esrsInfo = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (esrsInfoRestRep)null;
        return esrsInfo;
    }

    /// <summary>Delete specified ESRS server.</summary>
    /// <param name="esrsId">The ESRS server id.</param>
    /// <returns>A boolean indicating if the ESRS server was successfully deleted.</returns>
    public async Task<esrsInfoRestRep> DeleteEsrsInfo(string esrsId)
    {
        esrsId.AssertIsNotNull<string>(nameof(esrsId), "Unable to delete ESRS server with null or empty esrsId argument.");
        HttpResponseMessage resp = await this._httpClient.DeleteAsync(new Uri(string.Format("{0}/vdc/callhome/esrs/{1}", (object)this.Client.EndPoint, (object)esrsId)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to delete ESRS server. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        esrsInfoRestRep payload = await resp.Content.ReadAsAsync<esrsInfoRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        esrsInfoRestRep esrsInfoRestRep = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (esrsInfoRestRep)null;
        return esrsInfoRestRep;
    }

    /// <summary>
    /// Gets the configuration properties for the specified category.
    /// </summary>
    /// <returns>A propertyInfoRestRep object representing the configuration properties for the specified category or "ALL".</returns>
    public async Task<propertyInfoRestRep> GetProperties()
    {
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/config/object/properties", (object)this.Client.EndPoint)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get certificate chain. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        propertyInfoRestRep payload = await resp.Content.ReadAsAsync<propertyInfoRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        propertyInfoRestRep properties = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (propertyInfoRestRep)null;
        return properties;
    }

    /// <summary>Sets the configuration properties for the system.</summary>
    /// <param name="properties">The properties to be set in the system.</param>
    /// <returns>A boolean indicating if the properties were successfully set.</returns>
    public async Task<bool> SetProperties(IDictionary<string, string> properties)
    {
        properties.AssertIsNotNull<IDictionary<string, string>>(nameof(properties), "Unable to set properties null properties argument.");
        propertyInfoUpdate data = new propertyInfoUpdate();
        propertyInfoEntry[] propertyList = properties.Select<KeyValuePair<string, string>, propertyInfoEntry>((Func<KeyValuePair<string, string>, propertyInfoEntry>)(p => new propertyInfoEntry()
        {
            key = p.Key,
            value = p.Value
        })).ToArray<propertyInfoEntry>();
        data.properties = propertyList;
        HttpResponseMessage resp = await this._httpClient.PutAsync<propertyInfoUpdate>(new Uri(string.Format("{0}/config/object/properties", (object)this.Client.EndPoint)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get certificate chain. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        data = (propertyInfoUpdate)null;
        propertyList = (propertyInfoEntry[])null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>
    /// Gets the meta data for each of the configuration properties.
    /// </summary>
    /// <returns>A propertiesMetadata object representing the configured metadata for the system.</returns>
    public async Task<propertiesMetadata> GetPropertyMetadata()
    {
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/config/object/properties/metadata", (object)this.Client.EndPoint)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get property metadata. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        propertiesMetadata payload = await resp.Content.ReadAsAsync<propertiesMetadata>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        propertiesMetadata propertyMetadata = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (propertiesMetadata)null;
        return propertyMetadata;
    }

    /// <summary>Gets the currently configured licenses.</summary>
    /// <returns>A license object representing the license information currently configured.</returns>
    public async Task<license> GetLicense()
    {
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/license", (object)this.Client.EndPoint)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get license information. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        license payload = await resp.Content.ReadAsAsync<license>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        license license = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (license)null;
        return license;
    }

    /// <summary>Adds the specified license.</summary>
    /// <returns>A boolean indicating if the license was posted successfully.</returns>
    public async Task<bool> PostLicense(
      string serial,
      string version,
      string issueDate,
      string expirationDate,
      string model,
      string product,
      string siteId,
      string issuer,
      string notice,
      bool licensedInd,
      bool expiredInd,
      string licenseIdIndicator,
      string errorMessage,
      string storageCapacityUnit,
      string storageCapacity,
      bool trialLicenseInd,
      string licenseText)
    {
        serial.AssertIsNotNullOrEmpty(nameof(serial), "Unable to post license with null or empty serial argument.");
        model.AssertIsNotNullOrEmpty(nameof(model), "Unable to post license with null or empty model argument.");
        product.AssertIsNotNullOrEmpty(nameof(product), "Unable to post license with null or empty product argument.");
        siteId.AssertIsNotNullOrEmpty(nameof(siteId), "Unable to post license with null or empty siteId argument.");
        issuer.AssertIsNotNullOrEmpty(nameof(issuer), "Unable to post license with null or empty issuer argument.");
        notice.AssertIsNotNullOrEmpty(nameof(notice), "Unable to post license with null or empty notice argument.");
        licensedInd.AssertIsNotNull<bool>(nameof(licensedInd), "Unable to post license with null licensedInd argument.");
        expiredInd.AssertIsNotNull<bool>(nameof(expiredInd), "Unable to post license with null expiredInd argument.");
        licenseIdIndicator.AssertIsNotNullOrEmpty(nameof(licenseIdIndicator), "Unable to post license with null or empty licenseIdIndicator argument.");
        errorMessage.AssertIsNotNullOrEmpty(nameof(errorMessage), "Unable to post license with null or empty errorMessage argument.");
        storageCapacityUnit.AssertIsNotNullOrEmpty(nameof(storageCapacityUnit), "Unable to post license with null or empty storageCapacityUnit argument.");
        storageCapacity.AssertIsNotNullOrEmpty(nameof(storageCapacity), "Unable to post license with null or empty storageCapacity argument.");
        trialLicenseInd.AssertIsNotNull<bool>(nameof(trialLicenseInd), "Unable to post license with null or empty trialLicenseInd argument.");
        licenseText.AssertIsNotNullOrEmpty(nameof(licenseText), "Unable to post license with null or empty licenseText argument.");
        license data = new license();
        licenseFeature[] features = new licenseFeature[1]
        {
        new licenseFeature()
        {
          serial = serial,
          model = model,
          product = product,
          site_id = siteId,
          issuer = issuer,
          notice = notice,
          licensed_ind = licensedInd,
          expired_ind = expiredInd,
          license_id_indicator = licenseIdIndicator,
          error_message = errorMessage,
          storage_capacity_unit = storageCapacityUnit,
          storage_capacity = storageCapacity,
          trial_license_ind = trialLicenseInd
        }
        };
        data.license_feature = features;
        data.license_text = licenseText;
        HttpResponseMessage resp = await this._httpClient.PostAsync<license>(new Uri(string.Format("{0}/license", (object)this.Client.EndPoint)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to post license information. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        data = (license)null;
        features = (licenseFeature[])null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>Gets CAS secret for specified user.</summary>
    /// <param name="userName">A valid user identifier to get the key from.</param>
    /// <param name="nameSpace">The namespace for which to get the CAS secret.</param>
    /// <returns>A casSecretRestRep object containing the CAS secret key.</returns>
    public async Task<casSecretRestRep> GetCasSecretForUser(
      string userName,
      string nameSpace = null)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to get CAS secret for user with null or empty userName argument.");
        string uriNamespace = nameSpace == null ? string.Empty : nameSpace + "/";
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/user-cas/secret/{1}{2}", (object)this.Client.EndPoint, (object)uriNamespace, (object)userName)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get default CAS secret for the user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        casSecretRestRep payload = await resp.Content.ReadAsAsync<casSecretRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        casSecretRestRep casSecretForUser = payload;
        uriNamespace = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (casSecretRestRep)null;
        return casSecretForUser;
    }

    /// <summary>Creates or updates CAS secret for a specified user.</summary>
    /// <param name="userName">A valid user identifier to get the key from.</param>
    /// <param name="nameSpace">The namespace for which to get the CAS secret.</param>
    /// <param name="casSecret">The secret to be set for the specified user and namespace.</param>
    /// <returns>A boolean indicating if the secret key was created or updated successfully.</returns>
    public async Task<bool> SetCasSecretForUser(
      string userName,
      string nameSpace,
      string casSecret)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to set CAS secret for user with null or empty userName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to set CAS secret for user with null or empty nameSpace argument.");
        casSecret.AssertIsNotNullOrEmpty(nameof(casSecret), "Unable to set CAS secret for user with null or empty casSecret argument.");
        casSecretParam data = new casSecretParam()
        {
            @namespace = nameSpace,
            secret = casSecret
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<casSecretParam>(new Uri(string.Format("{0}/object/user-cas/secret/{1}", (object)this.Client.EndPoint, (object)userName)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to set CAS secret for the user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        bool flag = true;
        data = (casSecretParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>
    /// Generates Pool Entry Authorization (PEA) file for specified user.
    /// </summary>
    /// <param name="nameSpace">The namespace to use when generating the PEA file.</param>
    /// <param name="userName">The object user to use when generating the PEA file.</param>
    /// <returns>A string representing the newly generated CAS PEA file in XML format.</returns>
    public async Task<string> GetProfilePea(string nameSpace, string userName)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to generate CAS PEA file with null or empty nameSpace argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(userName), "Unable to generate CAS PEA file with null or empty userName argument.");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/user-cas/secret/{1}/{2}/pea", (object)this.Client.EndPoint, (object)nameSpace, (object)userName)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to generate the CAS PEA file. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        string payload = resp.Content.ReadAsStringAsync().Result;
        resp.Dispose();
        string profilePea = payload;
        resp = (HttpResponseMessage)null;
        payload = (string)null;
        return profilePea;
    }

    /// <summary>Deletes CAS secret for a specified user.</summary>
    /// <param name="userName">A valid user identifier to delete the key from.</param>
    /// <param name="nameSpace">The namespace for which to delete the CAS secret.</param>
    /// <param name="casSecret">The secret to be deleted for the specified user and namespace.</param>
    /// <returns>A boolean indicating if the secret key was deleted successfully.</returns>
    public async Task<bool> DeleteCasSecretForUser(
      string userName,
      string nameSpace,
      string casSecret)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to delete CAS secret for user with null or empty userName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to delete CAS secret for user with null or empty nameSpace argument.");
        casSecret.AssertIsNotNullOrEmpty(nameof(casSecret), "Unable to delete CAS secret for user with null or empty casSecret argument.");
        casSecretParam data = new casSecretParam()
        {
            @namespace = nameSpace,
            secret = casSecret
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<casSecretParam>(new Uri(string.Format("{0}/object/user-cas/secret/{1}/deactivate", (object)this.Client.EndPoint, (object)userName)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get delete CAS secret for the user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        bool flag = true;
        data = (casSecretParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>
    /// Gets default bucket for the specified namespace and user identifier.
    /// </summary>
    /// <param name="nameSpace">The namespace from which to get default bucket.</param>
    /// <param name="userName">The user name from which to get detault bucket.</param>
    /// <returns>A userCasBucketParam object containing the name of the default CAS bucket.</returns>
    public async Task<userCasBucketParam> GetDefaultCasBucket(
      string userName,
      string nameSpace = null)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to get default CAS bucket with null or empty userName argument.");
        string uriNamespace = nameSpace == null ? string.Empty : nameSpace + "/";
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/user-cas/bucket/{1}{2}", (object)this.Client.EndPoint, (object)uriNamespace, (object)userName)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get default CAS bucket. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        userCasBucketParam payload = await resp.Content.ReadAsAsync<userCasBucketParam>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        userCasBucketParam defaultCasBucket = payload;
        uriNamespace = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (userCasBucketParam)null;
        return defaultCasBucket;
    }

    /// <summary>
    /// Sets the CAS default bucket for the specified object user and namespace.
    /// </summary>
    /// <param name="userName">The object user name to be assigned the CAS default bucket.</param>
    /// <param name="nameSpace">The namespace to which the object user belongs.</param>
    /// <param name="bucketName">The name of bucket used to set the CAS default bucket.</param>
    /// <returns>A bool indicating if the CAS default bucket was updated successfully.</returns>
    public async Task<bool> SetCasDefaultBucket(
      string userName,
      string nameSpace,
      string bucketName)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to set CAS default bucket with null or empty userName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to set CAS default bucket with null or empty nameSpace argument.");
        bucketName.AssertIsNotNullOrEmpty(nameof(bucketName), "Unable to set CAS default bucket with null or empty bucketName argument.");
        userCasBucketParam data = new userCasBucketParam()
        {
            name = bucketName
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<userCasBucketParam>(new Uri(string.Format("{0}/object/user-cas/bucket/{1}/{2}", (object)this.Client.EndPoint, (object)nameSpace, (object)userName)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to set CAS default bucket. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        data = (userCasBucketParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>
    /// Gets the CAS registered applications for a specified namespace.
    /// </summary>
    /// <param name="nameSpace">The namespace for which to get CAS registered applications.</param>
    /// <returns>A casRegisteredApplicationListRep object containing the list of CAS registered application for the namespace.</returns>
    public async Task<casRegisteredApplicationListRep> GetRegisteredApplications(
      string nameSpace)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to get CAS registered applications with null or empty nameSpace argument.");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/user-cas/applications/{1}", (object)this.Client.EndPoint, (object)nameSpace)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get default CAS registered applications. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        casRegisteredApplicationListRep payload = await resp.Content.ReadAsAsync<casRegisteredApplicationListRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        casRegisteredApplicationListRep registeredApplications = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (casRegisteredApplicationListRep)null;
        return registeredApplications;
    }

    /// <summary>
    /// Gets the capacity of the cluster. The details includes the provisioned capacity in GB and available capacity in GB.
    /// </summary>
    /// <param name="storagePoolId">Storage pool identifier for which to retrieve capacity.</param>
    /// <returns>A clusterCapacityRep object that represents the cluster capacity details.</returns>
    public async Task<clusterCapacityRep> GetClusterCapacity(
      string storagePoolId = null)
    {
        string uriStoragePoolId = storagePoolId == null ? string.Empty : "/" + storagePoolId;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/capacity{1}", (object)this.Client.EndPoint, (object)uriStoragePoolId)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get cluster capacity. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        clusterCapacityRep payload = await resp.Content.ReadAsAsync<clusterCapacityRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        clusterCapacityRep clusterCapacity = payload;
        uriStoragePoolId = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (clusterCapacityRep)null;
        return clusterCapacity;
    }

    /// <summary>
    /// Gets audit events for the specified namespace identifier and interval.
    /// </summary>
    /// <param name="nameSpace">Namespace identifier for which audit events needs to be retrieved.</param>
    /// <param name="startTime">Start time for the interval to retrieve audit events.</param>
    /// <param name="endTime">End time for the interval to retrieve audit events.</param>
    /// <param name="limit">Number of audit events requested in current fetch.</param>
    /// <param name="marker">Used to continue a truncated response. Omit this parameter on the first request.</param>
    /// <returns>A auditList object that lists audit events for the given namespace identifier and interval.</returns>
    public async Task<auditList> GetAuditEvents(
      string nameSpace,
      DateTime startTime,
      DateTime endTime,
      string marker = null,
      int limit = 0)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to get audit events with a null or emtpy namespace argument.");
        startTime.AssertIsNotNull<DateTime>(nameof(startTime), "Unable to get audit events with a null or emtpy startTime argument.");
        endTime.AssertIsNotNull<DateTime>(nameof(endTime), "Unable to get audit events with a null or emtpy endTime argument.");
        string uriStartTime = startTime.ToString("yyyy-MM-ddThh:mm");
        string uriEndTime = endTime.ToString("yyyy-MM-ddThh:mm");
        Dictionary<string, string> uriParams = new Dictionary<string, string>();
        uriParams.Add("namespace", nameSpace);
        uriParams.Add("start_time", uriStartTime);
        uriParams.Add("end_time", uriEndTime);
        if (limit > 0)
            uriParams.Add(nameof(limit), Convert.ToString(limit));
        if (marker != null)
            uriParams.Add(nameof(marker), marker);
        string[] arrayUriParams = uriParams.Select<KeyValuePair<string, string>, string>((Func<KeyValuePair<string, string>, string>)(d => string.Format("{0}={1}", (object)d.Key, (object)d.Value))).ToArray<string>();
        string uriParamsResult = arrayUriParams.Length != 0 ? "?" + string.Join("&", arrayUriParams) : string.Empty;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/vdc/events{1}", (object)this.Client.EndPoint, (object)uriParamsResult)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation && resp.StatusCode != HttpStatusCode.NotFound)
            throw new InvalidOperationException(string.Format("Failed to get audit events. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        auditList payload = await resp.Content.ReadAsAsync<auditList>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        auditList auditEvents = payload;
        uriStartTime = (string)null;
        uriEndTime = (string)null;
        uriParams = (Dictionary<string, string>)null;
        arrayUriParams = (string[])null;
        uriParamsResult = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (auditList)null;
        return auditEvents;
    }

    /// <summary>Gets the local VDC details.</summary>
    /// <param name="categories">The category(s) of data to be returned.  If not specified, all category data is returned.</param>
    /// <param name="dataTypes">Indicate if historical, current or both data values should be returned.  If using historical,
    /// secifiy additional startTime, endTime and interval parameters.  If not specified, current and historical data is
    /// returned for last five days in 1 hour intervals.</param>
    /// <param name="startTime">For historical data, specify start date in GMT Epoch time in seconds.</param>
    /// <param name="endTime">For historical data, specify end date in GMT Epoch time in seconds.</param>
    /// <param name="interval">For historical data, specify interval at which to return data values.</param>
    /// <returns>A string representing the result in JSON format.</returns>
    public async Task<string> GetLocalZone(
      IEnumerable<Category> categories = null,
      IEnumerable<DataType> dataTypes = null,
      int startTime = 0,
      int endTime = 0,
      int interval = 0)
    {
        string queryString = HelperExtentions.BuildDashboardQueryString(categories, dataTypes, (long)startTime, (long)endTime, interval);
        this._httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/dashboard/zones/localzone{1}", (object)this.Client.EndPoint, (object)queryString)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get local zone. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        string payload = await resp.Content.ReadAsStringAsync();
        resp.Dispose();
        string localZone = payload;
        queryString = (string)null;
        resp = (HttpResponseMessage)null;
        payload = (string)null;
        return localZone;
    }

    /// <summary>Gets the local VDC replication group details.</summary>
    /// <param name="categories">The category(s) of data to be returned.  If not specified, all category data is returned.</param>
    /// <param name="dataTypes">Indicate if historical, current or both data values should be returned.  If using historical,
    /// secifiy additional startTime, endTime and interval parameters.  If not specified, current and historical data is
    /// returned for last five days in 1 hour intervals.</param>
    /// <param name="startTime">For historical data, specify start date in GMT Epoch time in seconds.</param>
    /// <param name="endTime">For historical data, specify end date in GMT Epoch time in seconds.</param>
    /// <param name="interval">For historical data, specify interval at which to return data values.</param>
    /// <returns>A string representing the result in JSON format.</returns>
    public async Task<string> GetLocalZoneReplicationGroups(
      IEnumerable<Category> categories = null,
      IEnumerable<DataType> dataTypes = null,
      int startTime = 0,
      int endTime = 0,
      int interval = 0)
    {
        string queryString = HelperExtentions.BuildDashboardQueryString(categories, dataTypes, (long)startTime, (long)endTime, interval);
        this._httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/dashboard/zones/localzone/replicationgroups{1}", (object)this.Client.EndPoint, (object)queryString)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get local VDC replication groups. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        string payload = await resp.Content.ReadAsStringAsync();
        resp.Dispose();
        string replicationGroups = payload;
        queryString = (string)null;
        resp = (HttpResponseMessage)null;
        payload = (string)null;
        return replicationGroups;
    }

    /// <summary>
    /// Gets the local VDC replication group failed links details.
    /// </summary>
    /// <param name="categories">The category(s) of data to be returned.  If not specified, all category data is returned.</param>
    /// <param name="dataTypes">Indicate if historical, current or both data values should be returned.  If using historical,
    /// secifiy additional startTime, endTime and interval parameters.  If not specified, current and historical data is
    /// returned for last five days in 1 hour intervals.</param>
    /// <param name="startTime">For historical data, specify start date in GMT Epoch time in seconds.</param>
    /// <param name="endTime">For historical data, specify end date in GMT Epoch time in seconds.</param>
    /// <param name="interval">For historical data, specify interval at which to return data values.</param>
    /// <returns>A string representing the result in JSON format.</returns>
    public async Task<string> GetLocalZoneReplicationGroupFailedLinks(
      IEnumerable<Category> categories = null,
      IEnumerable<DataType> dataTypes = null,
      int startTime = 0,
      int endTime = 0,
      int interval = 0)
    {
        string queryString = HelperExtentions.BuildDashboardQueryString(categories, dataTypes, (long)startTime, (long)endTime, interval);
        this._httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/dashboard/zones/localzone/rglinksFailed{1}", (object)this.Client.EndPoint, (object)queryString)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get local VDC replication group failed links. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        string payload = await resp.Content.ReadAsStringAsync();
        resp.Dispose();
        string groupFailedLinks = payload;
        queryString = (string)null;
        resp = (HttpResponseMessage)null;
        payload = (string)null;
        return groupFailedLinks;
    }

    /// <summary>Gets the local VDC storage pool details.</summary>
    /// <param name="categories">The category(s) of data to be returned.  If not specified, all category data is returned.</param>
    /// <param name="dataTypes">Indicate if historical, current or both data values should be returned.  If using historical,
    /// secifiy additional startTime, endTime and interval parameters.  If not specified, current and historical data is
    /// returned for last five days in 1 hour intervals.</param>
    /// <param name="startTime">For historical data, specify start date in GMT Epoch time in seconds.</param>
    /// <param name="endTime">For historical data, specify end date in GMT Epoch time in seconds.</param>
    /// <param name="interval">For historical data, specify interval at which to return data values.</param>
    /// <returns>A string representing the result in JSON format.</returns>
    public async Task<string> GetLocalZoneStoragePools(
      IEnumerable<Category> categories = null,
      IEnumerable<DataType> dataTypes = null,
      int startTime = 0,
      int endTime = 0,
      int interval = 0)
    {
        string queryString = HelperExtentions.BuildDashboardQueryString(categories, dataTypes, (long)startTime, (long)endTime, interval);
        this._httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/dashboard/zones/localzone/storagepools{1}", (object)this.Client.EndPoint, (object)queryString)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get local VDC storage pools. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        string payload = await resp.Content.ReadAsStringAsync();
        resp.Dispose();
        string zoneStoragePools = payload;
        queryString = (string)null;
        resp = (HttpResponseMessage)null;
        payload = (string)null;
        return zoneStoragePools;
    }

    /// <summary>Gets the local VDC node details.</summary>
    /// <param name="categories">The category(s) of data to be returned.  If not specified, all category data is returned.</param>
    /// <param name="dataTypes">Indicate if historical, current or both data values should be returned.  If using historical,
    /// secifiy additional startTime, endTime and interval parameters.  If not specified, current and historical data is
    /// returned for last five days in 1 hour intervals.</param>
    /// <param name="startTime">For historical data, specify start date in GMT Epoch time in seconds.</param>
    /// <param name="endTime">For historical data, specify end date in GMT Epoch time in seconds.</param>
    /// <param name="interval">For historical data, specify interval at which to return data values.</param>
    /// <returns>A string representing the result in JSON format.</returns>
    public async Task<string> GetLocalZoneNodes(
      IEnumerable<Category> categories = null,
      IEnumerable<DataType> dataTypes = null,
      int startTime = 0,
      int endTime = 0,
      int interval = 0)
    {
        string queryString = HelperExtentions.BuildDashboardQueryString(categories, dataTypes, (long)startTime, (long)endTime, interval);
        this._httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/dashboard/zones/localzone/nodes{1}", (object)this.Client.EndPoint, (object)queryString)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get local VDC nodes. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        string payload = await resp.Content.ReadAsStringAsync();
        resp.Dispose();
        string localZoneNodes = payload;
        queryString = (string)null;
        resp = (HttpResponseMessage)null;
        payload = (string)null;
        return localZoneNodes;
    }

    /// <summary>Gets the storage pool details.</summary>
    /// <param name="storagePoolId">The storage pool identifier for which to get details.</param>
    /// <param name="categories">The category(s) of data to be returned.  If not specified, all category data is returned.</param>
    /// <param name="dataTypes">Indicate if historical, current or both data values should be returned.  If using historical,
    /// secifiy additional startTime, endTime and interval parameters.  If not specified, current and historical data is
    /// returned for last five days in 1 hour intervals.</param>
    /// <param name="startTime">For historical data, specify start date in GMT Epoch time in seconds.</param>
    /// <param name="endTime">For historical data, specify end date in GMT Epoch time in seconds.</param>
    /// <param name="interval">For historical data, specify interval at which to return data values.</param>
    /// <returns>A string representing the result in JSON format.</returns>
    public async Task<string> GetStoragePool(
      string storagePoolId,
      IEnumerable<Category> categories = null,
      IEnumerable<DataType> dataTypes = null,
      int startTime = 0,
      int endTime = 0,
      int interval = 0)
    {
        storagePoolId.AssertIsNotNullOrEmpty(nameof(storagePoolId), "Unable to get storage pool details with a null or emtpy storagePoolId argument.");
        string queryString = HelperExtentions.BuildDashboardQueryString(categories, dataTypes, (long)startTime, (long)endTime, interval);
        this._httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/dashboard/storagepools/{1}{2}", (object)this.Client.EndPoint, (object)storagePoolId, (object)queryString)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get storage pool. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        string payload = await resp.Content.ReadAsStringAsync();
        resp.Dispose();
        string storagePool = payload;
        queryString = (string)null;
        resp = (HttpResponseMessage)null;
        payload = (string)null;
        return storagePool;
    }

    /// <summary>Gets the storage pool details.</summary>
    /// <param name="nodeId">The node identifier for which to get details.</param>
    /// <param name="categories">The category(s) of data to be returned.  If not specified, all category data is returned.</param>
    /// <param name="dataTypes">Indicate if historical, current or both data values should be returned.  If using historical,
    /// secifiy additional startTime, endTime and interval parameters.  If not specified, current and historical data is
    /// returned for last five days in 1 hour intervals.</param>
    /// <param name="startTime">For historical data, specify start date in GMT Epoch time in seconds.</param>
    /// <param name="endTime">For historical data, specify end date in GMT Epoch time in seconds.</param>
    /// <param name="interval">For historical data, specify interval at which to return data values.</param>
    /// <returns>A string representing the result in JSON format.</returns>
    public async Task<string> GetNode(
      string nodeId,
      IEnumerable<Category> categories = null,
      IEnumerable<DataType> dataTypes = null,
      int startTime = 0,
      int endTime = 0,
      int interval = 0)
    {
        nodeId.AssertIsNotNullOrEmpty(nameof(nodeId), "Unable to get node details with a null or emtpy nodeId argument.");
        string queryString = HelperExtentions.BuildDashboardQueryString(categories, dataTypes, (long)startTime, (long)endTime, interval);
        this._httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/dashboard/nodes/{1}{2}", (object)this.Client.EndPoint, (object)nodeId, (object)queryString)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get node. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        string payload = await resp.Content.ReadAsStringAsync();
        resp.Dispose();
        string node = payload;
        queryString = (string)null;
        resp = (HttpResponseMessage)null;
        payload = (string)null;
        return node;
    }

    /// <summary>Gets the disk instance details.</summary>
    /// <param name="diskId">The disk identifier for which to get details.</param>
    /// <param name="categories">The category(s) of data to be returned.  If not specified, all category data is returned.</param>
    /// <param name="dataTypes">Indicate if historical, current or both data values should be returned.  If using historical,
    /// secifiy additional startTime, endTime and interval parameters.  If not specified, current and historical data is
    /// returned for last five days in 1 hour intervals.</param>
    /// <param name="startTime">For historical data, specify start date in GMT Epoch time in seconds.</param>
    /// <param name="endTime">For historical data, specify end date in GMT Epoch time in seconds.</param>
    /// <param name="interval">For historical data, specify interval at which to return data values.</param>
    /// <returns>A string representing the result in JSON format.</returns>
    public async Task<string> GetDisk(
      string diskId,
      IEnumerable<Category> categories = null,
      IEnumerable<DataType> dataTypes = null,
      int startTime = 0,
      int endTime = 0,
      int interval = 0)
    {
        diskId.AssertIsNotNullOrEmpty(nameof(diskId), "Unable to get disk details with a null or emtpy diskId argument.");
        string queryString = HelperExtentions.BuildDashboardQueryString(categories, dataTypes, (long)startTime, (long)endTime, interval);
        this._httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/dashboard/disks/{1}{2}", (object)this.Client.EndPoint, (object)diskId, (object)queryString)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get disk. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        string payload = await resp.Content.ReadAsStringAsync();
        resp.Dispose();
        string disk = payload;
        queryString = (string)null;
        resp = (HttpResponseMessage)null;
        payload = (string)null;
        return disk;
    }

    /// <summary>Gets the process instance details.</summary>
    /// <param name="processId">The process identifier for which to get details.</param>
    /// <param name="categories">The category(s) of data to be returned.  If not specified, all category data is returned.</param>
    /// <param name="dataTypes">Indicate if historical, current or both data values should be returned.  If using historical,
    /// secifiy additional startTime, endTime and interval parameters.  If not specified, current and historical data is
    /// returned for last five days in 1 hour intervals.</param>
    /// <param name="startTime">For historical data, specify start date in GMT Epoch time in seconds.</param>
    /// <param name="endTime">For historical data, specify end date in GMT Epoch time in seconds.</param>
    /// <param name="interval">For historical data, specify interval at which to return data values.</param>
    /// <returns>A string representing the result in JSON format.</returns>
    public async Task<string> GetProcess(
      string processId,
      IEnumerable<Category> categories = null,
      IEnumerable<DataType> dataTypes = null,
      int startTime = 0,
      int endTime = 0,
      int interval = 0)
    {
        processId.AssertIsNotNullOrEmpty(nameof(processId), "Unable to get process details with a null or emtpy processId argument.");
        string queryString = HelperExtentions.BuildDashboardQueryString(categories, dataTypes, (long)startTime, (long)endTime, interval);
        this._httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/dashboard/processes/{1}{2}", (object)this.Client.EndPoint, (object)processId, (object)queryString)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get process. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        string payload = await resp.Content.ReadAsStringAsync();
        resp.Dispose();
        string process = payload;
        queryString = (string)null;
        resp = (HttpResponseMessage)null;
        payload = (string)null;
        return process;
    }

    /// <summary>Gets the node instance process details.</summary>
    /// <param name="nodeId">The node for which to get process details.</param>
    /// <param name="categories">The category(s) of data to be returned.  If not specified, all category data is returned.</param>
    /// <param name="dataTypes">Indicate if historical, current or both data values should be returned.  If using historical,
    /// secifiy additional startTime, endTime and interval parameters.  If not specified, current and historical data is
    /// returned for last five days in 1 hour intervals.</param>
    /// <param name="startTime">For historical data, specify start date in GMT Epoch time in seconds.</param>
    /// <param name="endTime">For historical data, specify end date in GMT Epoch time in seconds.</param>
    /// <param name="interval">For historical data, specify interval at which to return data values.</param>
    /// <returns>A string representing the result in JSON format.</returns>
    public async Task<string> GetNodeProcesses(
      string nodeId,
      IEnumerable<Category> categories = null,
      IEnumerable<DataType> dataTypes = null,
      int startTime = 0,
      int endTime = 0,
      int interval = 0)
    {
        nodeId.AssertIsNotNullOrEmpty(nameof(nodeId), "Unable to get node process details with a null or emtpy nodeId argument.");
        string queryString = HelperExtentions.BuildDashboardQueryString(categories, dataTypes, (long)startTime, (long)endTime, interval);
        this._httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/dashboard/nodes/{1}/processes{2}", (object)this.Client.EndPoint, (object)nodeId, (object)queryString)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get node processes. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        string payload = await resp.Content.ReadAsStringAsync();
        resp.Dispose();
        string nodeProcesses = payload;
        queryString = (string)null;
        resp = (HttpResponseMessage)null;
        payload = (string)null;
        return nodeProcesses;
    }

    /// <summary>Gets the node instance disk details.</summary>
    /// <param name="nodeId">The node for which to get disk details.</param>
    /// <param name="categories">The category(s) of data to be returned.  If not specified, all category data is returned.</param>
    /// <param name="dataTypes">Indicate if historical, current or both data values should be returned.  If using historical,
    /// secifiy additional startTime, endTime and interval parameters.  If not specified, current and historical data is
    /// returned for last five days in 1 hour intervals.</param>
    /// <param name="startTime">For historical data, specify start date in GMT Epoch time in seconds.</param>
    /// <param name="endTime">For historical data, specify end date in GMT Epoch time in seconds.</param>
    /// <param name="interval">For historical data, specify interval at which to return data values.</param>
    /// <returns>A string representing the result in JSON format.</returns>
    public async Task<string> GetNodeDisks(
      string nodeId,
      IEnumerable<Category> categories = null,
      IEnumerable<DataType> dataTypes = null,
      int startTime = 0,
      int endTime = 0,
      int interval = 0)
    {
        nodeId.AssertIsNotNullOrEmpty(nameof(nodeId), "Unable to get node disk details with a null or emtpy nodeId argument.");
        string queryString = HelperExtentions.BuildDashboardQueryString(categories, dataTypes, (long)startTime, (long)endTime, interval);
        this._httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/dashboard/nodes/{1}/disks{2}", (object)this.Client.EndPoint, (object)nodeId, (object)queryString)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get node disks. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        string payload = await resp.Content.ReadAsStringAsync();
        resp.Dispose();
        string nodeDisks = payload;
        queryString = (string)null;
        resp = (HttpResponseMessage)null;
        payload = (string)null;
        return nodeDisks;
    }

    /// <summary>Gets the storage pool node details.</summary>
    /// <param name="storagePoolId">The storage pool for which to get node details.</param>
    /// <param name="categories">The category(s) of data to be returned.  If not specified, all category data is returned.</param>
    /// <param name="dataTypes">Indicate if historical, current or both data values should be returned.  If using historical,
    /// secifiy additional startTime, endTime and interval parameters.  If not specified, current and historical data is
    /// returned for last five days in 1 hour intervals.</param>
    /// <param name="startTime">For historical data, specify start date in GMT Epoch time in seconds.</param>
    /// <param name="endTime">For historical data, specify end date in GMT Epoch time in seconds.</param>
    /// <param name="interval">For historical data, specify interval at which to return data values.</param>
    /// <returns>A string representing the result in JSON format.</returns>
    public async Task<string> GetStoragePoolNodes(
      string storagePoolId,
      IEnumerable<Category> categories = null,
      IEnumerable<DataType> dataTypes = null,
      int startTime = 0,
      int endTime = 0,
      int interval = 0)
    {
        storagePoolId.AssertIsNotNullOrEmpty(nameof(storagePoolId), "Unable to get storage pool node details with a null or emtpy storagePoolId argument.");
        string queryString = HelperExtentions.BuildDashboardQueryString(categories, dataTypes, (long)startTime, (long)endTime, interval);
        this._httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/dashboard/storagepools/{1}/nodes{2}", (object)this.Client.EndPoint, (object)storagePoolId, (object)queryString)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get storage pool nodes. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        string payload = await resp.Content.ReadAsStringAsync();
        resp.Dispose();
        string storagePoolNodes = payload;
        queryString = (string)null;
        resp = (HttpResponseMessage)null;
        payload = (string)null;
        return storagePoolNodes;
    }

    /// <summary>
    /// Gets the local VDC replication group bootstrap links details.
    /// </summary>
    /// <param name="categories">The category(s) of data to be returned.  If not specified, all category data is returned.</param>
    /// <param name="dataTypes">Indicate if historical, current or both data values should be returned.  If using historical,
    /// secifiy additional startTime, endTime and interval parameters.  If not specified, current and historical data is
    /// returned for last five days in 1 hour intervals.</param>
    /// <param name="startTime">For historical data, specify start date in GMT Epoch time in seconds.</param>
    /// <param name="endTime">For historical data, specify end date in GMT Epoch time in seconds.</param>
    /// <param name="interval">For historical data, specify interval at which to return data values.</param>
    /// <returns>A string representing the result in JSON format.</returns>
    public async Task<string> GetLocalZoneReplicationGroupBootstrapLinks(
      IEnumerable<Category> categories = null,
      IEnumerable<DataType> dataTypes = null,
      int startTime = 0,
      int endTime = 0,
      int interval = 0)
    {
        string queryString = HelperExtentions.BuildDashboardQueryString(categories, dataTypes, (long)startTime, (long)endTime, interval);
        this._httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/dashboard/zones/localzone/rglinksBootstrap{1}", (object)this.Client.EndPoint, (object)queryString)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get local zone replication group bootstrap links. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        string payload = await resp.Content.ReadAsStringAsync();
        resp.Dispose();
        string groupBootstrapLinks = payload;
        queryString = (string)null;
        resp = (HttpResponseMessage)null;
        payload = (string)null;
        return groupBootstrapLinks;
    }

    /// <summary>Gets the replication group instance details.</summary>
    /// <param name="replicationGroupId">The replication group id for which to get details.</param>
    /// <param name="categories">The category(s) of data to be returned.  If not specified, all category data is returned.</param>
    /// <param name="dataTypes">Indicate if historical, current or both data values should be returned.  If using historical,
    /// secifiy additional startTime, endTime and interval parameters.  If not specified, current and historical data is
    /// returned for last five days in 1 hour intervals.</param>
    /// <param name="startTime">For historical data, specify start date in GMT Epoch time in seconds.</param>
    /// <param name="endTime">For historical data, specify end date in GMT Epoch time in seconds.</param>
    /// <param name="interval">For historical data, specify interval at which to return data values.</param>
    /// <returns>A string representing the result in JSON format.</returns>
    public async Task<string> GetReplicationGroupDashboard(
      string replicationGroupId,
      IEnumerable<Category> categories = null,
      IEnumerable<DataType> dataTypes = null,
      int startTime = 0,
      int endTime = 0,
      int interval = 0)
    {
        replicationGroupId.AssertIsNotNullOrEmpty(nameof(replicationGroupId), "Unable to get replication group details with a null or emtpy replicationGroupId argument.");
        string queryString = HelperExtentions.BuildDashboardQueryString(categories, dataTypes, (long)startTime, (long)endTime, interval);
        this._httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/dashboard/replicationgroups/{1}{2}", (object)this.Client.EndPoint, (object)replicationGroupId, (object)queryString)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get replication group. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        string payload = await resp.Content.ReadAsStringAsync();
        resp.Dispose();
        string replicationGroupDashboard = payload;
        queryString = (string)null;
        resp = (HttpResponseMessage)null;
        payload = (string)null;
        return replicationGroupDashboard;
    }

    /// <summary>
    /// Gets the replication group instance associated link details.
    /// </summary>
    /// <param name="replicationGroupId">The replication group id for which to get group link details.</param>
    /// <param name="categories">The category(s) of data to be returned.  If not specified, all category data is returned.</param>
    /// <param name="dataTypes">Indicate if historical, current or both data values should be returned.  If using historical,
    /// secifiy additional startTime, endTime and interval parameters.  If not specified, current and historical data is
    /// returned for last five days in 1 hour intervals.</param>
    /// <param name="startTime">For historical data, specify start date in GMT Epoch time in seconds.</param>
    /// <param name="endTime">For historical data, specify end date in GMT Epoch time in seconds.</param>
    /// <param name="interval">For historical data, specify interval at which to return data values.</param>
    /// <returns>A string representing the result in JSON format.</returns>
    public async Task<string> GetReplicationGroupLinks(
      string replicationGroupId,
      IEnumerable<Category> categories = null,
      IEnumerable<DataType> dataTypes = null,
      int startTime = 0,
      int endTime = 0,
      int interval = 0)
    {
        replicationGroupId.AssertIsNotNullOrEmpty(nameof(replicationGroupId), "Unable to get replication group links with a null or emtpy replicationGroupId argument.");
        string queryString = HelperExtentions.BuildDashboardQueryString(categories, dataTypes, (long)startTime, (long)endTime, interval);
        this._httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/dashboard/replicationgroups/{1}/rglinks{2}", (object)this.Client.EndPoint, (object)replicationGroupId, (object)queryString)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get replication group links instance. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        string payload = await resp.Content.ReadAsStringAsync();
        resp.Dispose();
        string replicationGroupLinks = payload;
        queryString = (string)null;
        resp = (HttpResponseMessage)null;
        payload = (string)null;
        return replicationGroupLinks;
    }

    /// <summary>Gets the replication group link instance details.</summary>
    /// <param name="replicationGroupId">The replication group id for which to get link details.</param>
    /// <param name="categories">The category(s) of data to be returned.  If not specified, all category data is returned.</param>
    /// <param name="dataTypes">Indicate if historical, current or both data values should be returned.  If using historical,
    /// secifiy additional startTime, endTime and interval parameters.  If not specified, current and historical data is
    /// returned for last five days in 1 hour intervals.</param>
    /// <param name="startTime">For historical data, specify start date in GMT Epoch time in seconds.</param>
    /// <param name="endTime">For historical data, specify end date in GMT Epoch time in seconds.</param>
    /// <param name="interval">For historical data, specify interval at which to return data values.</param>
    /// <returns>A string representing the result in JSON format.</returns>
    public async Task<string> GetRgLink(
      string replicationGroupId,
      IEnumerable<Category> categories = null,
      IEnumerable<DataType> dataTypes = null,
      int startTime = 0,
      int endTime = 0,
      int interval = 0)
    {
        replicationGroupId.AssertIsNotNullOrEmpty(nameof(replicationGroupId), "Unable to get replication group link details with a null or emtpy replicationGroupId argument.");
        string queryString = HelperExtentions.BuildDashboardQueryString(categories, dataTypes, (long)startTime, (long)endTime, interval);
        this._httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/dashboard/rglinks/{1}{2}", (object)this.Client.EndPoint, (object)replicationGroupId, (object)queryString)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get replication group link instance. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        string payload = await resp.Content.ReadAsStringAsync();
        resp.Dispose();
        string rgLink = payload;
        queryString = (string)null;
        resp = (HttpResponseMessage)null;
        payload = (string)null;
        return rgLink;
    }

    /// <summary>Gets a list of all configured replication groups.</summary>
    /// <returns>A dataServiceVpoolListResponse object listing all replication groups in the system.</returns>
    public async Task<dataServiceVpoolListResponse> GetReplicationGroups()
    {
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/vdc/data-service/vpools", (object)this.Client.EndPoint)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get list of replication groups. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        dataServiceVpoolListResponse payload = await resp.Content.ReadAsAsync<dataServiceVpoolListResponse>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        dataServiceVpoolListResponse replicationGroups = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (dataServiceVpoolListResponse)null;
        return replicationGroups;
    }

    /// <summary>
    /// Creates a replication group that includes the specified storage pools(VDC:storage pool tuple).
    /// </summary>
    /// <param name="name">A unique name identifying this classification of replication group.</param>
    /// <param name="description">The description of the replication group.</param>
    /// <param name="zoneMappings">The zone mappings of virtual data center, storage pool resources including target replication flag.</param>
    /// <param name="allowAccessAllNamespaces">A flag indicating if the vpool allows access to all namespace for all data services.</param>
    /// <param name="enableRebalancing">A flag indicating if rebalancing should be enabled.</param>
    /// <param name="isFullReplication">In a three (or more) site configuration, indicates data should be replicated to all sites.</param>
    /// <param name="useReplicationTarget">A flag indicating if replication target should be used.</param>
    /// <returns>A dataServiceVpoolRestRep object that represents the newly created replication group.</returns>
    public async Task<dataServiceVpoolRestRep> CreateReplicationGroup(
      string name,
      string description,
      IEnumerable<ReplicationMapping> zoneMappings,
      bool allowAccessAllNamespaces,
      bool enableRebalancing,
      bool isFullReplication,
      bool useReplicationTarget)
    {
        name.AssertIsNotNullOrEmpty(nameof(name), "Unable to create replication group with null or empty name argument.");
        description.AssertIsNotNullOrEmpty(nameof(description), "Unable to create replication group with null or empty description argument.");
        allowAccessAllNamespaces.AssertIsNotNull<bool>(nameof(allowAccessAllNamespaces), "Unable to create replication group with null or empty allowAccessAllNamespaces argument.");
        enableRebalancing.AssertIsNotNull<bool>(nameof(enableRebalancing), "Unable to create replication group with null or empty enableRebalancing argument.");
        isFullReplication.AssertIsNotNull<bool>(nameof(isFullReplication), "Unable to create replication group with null or empty isFullReplication argument.");
        useReplicationTarget.AssertIsNotNull<bool>(nameof(useReplicationTarget), "Unable to create replication group with null or empty useReplicationTarget argument.");
        zoneCosTuple[] hash_map = new zoneCosTuple[zoneMappings.Count<ReplicationMapping>()];
        int index = 0;
        foreach (ReplicationMapping item in zoneMappings)
        {
            string varray_name = item.StoragePoolName;
            string vdc_name = item.VdcName;
            bool is_rep_target = item.IsReplicationGroupTarget;
            string varray_id = await this.QueryVirtualArray(varray_name);
            string vdc_id = await this.QueryVirtualDataCenter(vdc_name);
            hash_map[index] = new zoneCosTuple()
            {
                name = vdc_id,
                value = varray_id,
                is_replication_target = is_rep_target,
                is_replication_targetSpecified = is_rep_target
            };
            ++index;
            varray_name = (string)null;
            vdc_name = (string)null;
            varray_id = (string)null;
            vdc_id = (string)null;
        }
        dataServiceVpoolCreateParam data = new dataServiceVpoolCreateParam()
        {
            name = name,
            description = description,
            zone_mappings = hash_map,
            isAllowAllNamespaces = allowAccessAllNamespaces,
            enable_rebalancing = enableRebalancing,
            isFullRep = isFullReplication,
            use_replication_target = useReplicationTarget
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<dataServiceVpoolCreateParam>(new Uri(string.Format("{0}/vdc/data-service/vpools", (object)this.Client.EndPoint)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to create replication group. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        dataServiceVpoolRestRep payload = await resp.Content.ReadAsAsync<dataServiceVpoolRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        dataServiceVpoolRestRep replicationGroup = payload;
        hash_map = (zoneCosTuple[])null;
        data = (dataServiceVpoolCreateParam)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (dataServiceVpoolRestRep)null;
        return replicationGroup;
    }

    /// <summary>Deletes the specified replication group.</summary>
    /// <param name="id">The unique id of the replication group to be deleted.</param>
    /// <returns>A boolean indicating if the request completed successfully.</returns>
    public async Task<bool> DeleteReplicationGroup(string id)
    {
        id.AssertIsNotNullOrEmpty(nameof(id), "Unable to delete replication group with null or empty id argument.");
        HttpResponseMessage resp = await this._httpClient.PostAsync(new Uri(string.Format("{0}/vdc/data-service/vpools/{1}/deactivate", (object)this.Client.EndPoint, (object)id)), (HttpContent)null);
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to delete replication group. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>Updates the specified replication group.</summary>
    /// <param name="replicationGroupId">The id of the replication group to be updated.</param>
    /// <param name="name">A unique name identifying this classification of replication group.</param>
    /// <param name="description">The description of the replication group.</param>
    /// <param name="allowAccessAllNamespaces">A flag indicating if the vpool allows access to all namespace for all data services.</param>
    /// <param name="enableRebalancing">A flag indicating if rebalancing should be enabled.</param>
    /// <returns>A boolean indicating if the replication group was updated successfully.</returns>
    public async Task<bool> UpdateReplicationGroup(
      string replicationGroupId,
      string name,
      string description,
      bool allowAccessAllNamespaces,
      bool enableRebalancing)
    {
        replicationGroupId.AssertIsNotNullOrEmpty(nameof(replicationGroupId), "Unable to update replication group with null or empty replicationGroupId argument.");
        name.AssertIsNotNullOrEmpty(nameof(name), "Unable to update replication group with null or empty name argument.");
        description.AssertIsNotNullOrEmpty(nameof(description), "Unable to update replication group with null or empty description argument.");
        allowAccessAllNamespaces.AssertIsNotNull<bool>(nameof(allowAccessAllNamespaces), "Unable to update replication group with null or empty allowAccessAllNamespaces argument.");
        enableRebalancing.AssertIsNotNull<bool>(nameof(enableRebalancing), "Unable to update replication group with null or empty enableRebalancing argument.");
        dataServiceVpoolUpdateParam data = new dataServiceVpoolUpdateParam()
        {
            name = name,
            description = description,
            allowAllNamespaces = allowAccessAllNamespaces,
            allowAllNamespacesSpecified = true,
            enable_rebalancing = enableRebalancing,
            enable_rebalancingSpecified = enableRebalancing
        };
        HttpResponseMessage resp = await this._httpClient.PutAsync<dataServiceVpoolUpdateParam>(new Uri(string.Format("{0}/vdc/data-service/vpools/{1}", (object)this.Client.EndPoint, (object)replicationGroupId)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to update replication group. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        data = (dataServiceVpoolUpdateParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>Gets the details for the specified replication group.</summary>
    /// <param name="replicationGroupId">The id of the replication for which to get details.</param>
    /// <returns>A dataServiceVpoolRestRep object showing details about the replication group.</returns>
    public async Task<dataServiceVpoolRestRep> GetReplicationGroup(
      string replicationGroupId)
    {
        replicationGroupId.AssertIsNotNullOrEmpty(nameof(replicationGroupId), "Unable to get replication group with null or empty replicationGroupId argument.");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/vdc/data-service/vpools/{1}", (object)this.Client.EndPoint, (object)replicationGroupId)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get replication group. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        dataServiceVpoolRestRep payload = await resp.Content.ReadAsAsync<dataServiceVpoolRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        dataServiceVpoolRestRep replicationGroup = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (dataServiceVpoolRestRep)null;
        return replicationGroup;
    }

    /// <summary>
    /// Deletes a storage pool (VDC:storage pool tuple) from a specified replication group.
    /// </summary>
    /// <param name="replicationGroupId">The replication group id for which storage pools will be removed.</param>
    /// <param name="zoneMappingsToRemove">The zone mappings of virtual data center, storage pool resources including target replication flag.</param>
    /// <returns>A boolean indicating if the storage pool(s) were removed from the replication group successfully.</returns>
    public async Task<bool> DeleteStoragePoolFromReplicationGroup(
      string replicationGroupId,
      IEnumerable<ReplicationMapping> zoneMappingsToRemove)
    {
        replicationGroupId.AssertIsNotNullOrEmpty(nameof(replicationGroupId), "Unable to remove storage pool(s) from replication group with null or empty replicationGroupId argument.");
        zoneMappingsToRemove.AssertIsNotNull<IEnumerable<ReplicationMapping>>(nameof(zoneMappingsToRemove), "Unable to remove storage pool(s) from replication group with null or empty zoneMappingsToRemove argument.");
        zoneCosTuple[] hash_map = new zoneCosTuple[zoneMappingsToRemove.Count<ReplicationMapping>()];
        int index = 0;
        foreach (ReplicationMapping item in zoneMappingsToRemove)
        {
            string varray_name = item.StoragePoolName;
            string vdc_name = item.VdcName;
            bool is_replication_target = item.IsReplicationGroupTarget;
            string varray_id = await this.QueryVirtualArray(varray_name);
            string vdc_id = await this.QueryVirtualDataCenter(vdc_name);
            hash_map[index] = new zoneCosTuple()
            {
                name = vdc_id,
                value = varray_id,
                is_replication_target = is_replication_target,
                is_replication_targetSpecified = is_replication_target
            };
            ++index;
            varray_name = (string)null;
            vdc_name = (string)null;
            varray_id = (string)null;
            vdc_id = (string)null;
        }
        dataServiceVpoolVarrayInfo data = new dataServiceVpoolVarrayInfo()
        {
            mappings = hash_map
        };
        HttpResponseMessage resp = await this._httpClient.PutAsync<dataServiceVpoolVarrayInfo>(new Uri(string.Format("{0}/vdc/data-service/vpools/{1}/removevarrays", (object)this.Client.EndPoint, (object)replicationGroupId)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to remove storage pool(s) from replication group. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        hash_map = (zoneCosTuple[])null;
        data = (dataServiceVpoolVarrayInfo)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>
    /// Adds a storage pool (VDC:storage pool tuple) to the specified replication group.
    /// </summary>
    /// <param name="replicationGroupId">The replication group id for which storage pools will be added.</param>
    /// <param name="zoneMappingsToAdd">The zone mappings of virtual data center and storage pool resources and replication target flag to be added.</param>
    /// <returns>A boolean indicating if the storage pool(s) were added to the replication group successfully.</returns>
    public async Task<bool> AddStoragePoolToReplicationGroup(
      string replicationGroupId,
      IEnumerable<ReplicationMapping> zoneMappingsToAdd)
    {
        replicationGroupId.AssertIsNotNullOrEmpty(nameof(replicationGroupId), "Unable to add storage pool(s) to replication group with null or empty replicationGroupId argument.");
        zoneMappingsToAdd.AssertIsNotNull<IEnumerable<ReplicationMapping>>(nameof(zoneMappingsToAdd), "Unable to add storage pool(s) to replication group with null or empty zoneMappingsToAdd argument.");
        zoneCosTuple[] hash_map = new zoneCosTuple[zoneMappingsToAdd.Count<ReplicationMapping>()];
        int index = 0;
        foreach (ReplicationMapping item in zoneMappingsToAdd)
        {
            string varray_name = item.StoragePoolName;
            string vdc_name = item.VdcName;
            bool is_replication_target = item.IsReplicationGroupTarget;
            string varray_id = await this.QueryVirtualArray(varray_name);
            string vdc_id = await this.QueryVirtualDataCenter(vdc_name);
            hash_map[index] = new zoneCosTuple()
            {
                name = vdc_id,
                value = varray_id,
                is_replication_target = is_replication_target,
                is_replication_targetSpecified = is_replication_target
            };
            ++index;
            varray_name = (string)null;
            vdc_name = (string)null;
            varray_id = (string)null;
            vdc_id = (string)null;
        }
        dataServiceVpoolVarrayInfo data = new dataServiceVpoolVarrayInfo()
        {
            mappings = hash_map
        };
        HttpResponseMessage resp = await this._httpClient.PutAsync<dataServiceVpoolVarrayInfo>(new Uri(string.Format("{0}/vdc/data-service/vpools/{1}/addvarrays", (object)this.Client.EndPoint)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to add storage pool(s) to replication group. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool replicationGroup = true;
        hash_map = (zoneCosTuple[])null;
        data = (dataServiceVpoolVarrayInfo)null;
        resp = (HttpResponseMessage)null;
        return replicationGroup;
    }

    /// <summary>
    /// Gets all the temp failed zone details for the specified replication group identifier.
    /// </summary>
    /// <param name="replicationGroupId">The id of the replication group for which to retrieve details.</param>
    /// <returns>A tempFailedZoneList object that represents all temp failed zone details for the specified replication group.</returns>
    public async Task<tempFailedZoneRestRep> GetTempFailedZone(
      string replicationGroupId)
    {
        replicationGroupId.AssertIsNotNullOrEmpty(nameof(replicationGroupId), "Unable to get temp failed zone details for replication group with null or empty replicationGroupId argument.");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/tempfailedzone/rgid/{1}", (object)this.Client.EndPoint, (object)replicationGroupId)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get temp failed zone details for replication group. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        tempFailedZoneRestRep payload = await resp.Content.ReadAsAsync<tempFailedZoneRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        tempFailedZoneRestRep tempFailedZone = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (tempFailedZoneRestRep)null;
        return tempFailedZone;
    }

    /// <summary>Gets all the configured temp failed zones.</summary>
    /// <returns>A tempFailedZoneList object that represents all temp failed zones.</returns>
    public async Task<tempFailedZoneList> GetAllTempFailedZones()
    {
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/tempfailedzone/allfailedzones", (object)this.Client.EndPoint)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get all temp failed zones. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        tempFailedZoneList payload = await resp.Content.ReadAsAsync<tempFailedZoneList>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        tempFailedZoneList allTempFailedZones = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (tempFailedZoneList)null;
        return allTempFailedZones;
    }

    /// <summary>Creates a Base URL with the given details.</summary>
    /// <param name="urlName">The name for the base url.</param>
    /// <param name="baseUrl">The base url to be used.</param>
    /// <param name="isNamespaceInHost">Set true if namespace is in host, false otherwise.</param>
    /// <returns>A objectBaseUrlRestRep object that represents the newly created base url details.</returns>
    public async Task<objectBaseUrlRestRep> CreateBaseUrl(
      string urlName,
      string baseUrl,
      bool isNamespaceInHost)
    {
        urlName.AssertIsNotNullOrEmpty(nameof(urlName), "Unable to create base url with null or empty urlName argument.");
        baseUrl.AssertIsNotNullOrEmpty("name", "Unable to create base url with null or empty name argument.");
        isNamespaceInHost.AssertIsNotNull<bool>("virtualArray", "Unable to create base url with null or empty virtualArray argument.");
        objectBaseUrlCreate data = new objectBaseUrlCreate()
        {
            name = urlName,
            base_url = baseUrl,
            is_namespace_in_host = isNamespaceInHost
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<objectBaseUrlCreate>(new Uri(string.Format("{0}/object/baseurl", (object)this.Client.EndPoint)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to create base url. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        objectBaseUrlRestRep payload = await resp.Content.ReadAsAsync<objectBaseUrlRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        objectBaseUrlRestRep baseUrl1 = payload;
        data = (objectBaseUrlCreate)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (objectBaseUrlRestRep)null;
        return baseUrl1;
    }

    /// <summary>Deletes the specified Base URL.</summary>
    /// <param name="baseUrlId">The name for the base url.</param>
    /// <returns>A boolean indicating if the base url was deleted successfully.</returns>
    public async Task<bool> DeactivateBaseUrl(string baseUrlId)
    {
        baseUrlId.AssertIsNotNullOrEmpty(nameof(baseUrlId), "Unable to delete base url with null or empty baseUrlId argument.");
        HttpResponseMessage resp = await this._httpClient.PostAsync(new Uri(string.Format("{0}/object/baseurl/{1}/deactivate", (object)this.Client.EndPoint, (object)baseUrlId)), (HttpContent)null);
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to deactivate base url. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>Updates the specified base url details..</summary>
    /// <param name="baseUrlId">The id of the base url to be udated.</param>
    /// <param name="urlName">The name for the base url.</param>
    /// <param name="baseUrl">The base url to be used.</param>
    /// <param name="isNamespaceInHost">Set true if namespace is in host, false otherwise.</param>
    /// <returns>A boolean indicating if the base url was updated successfully.</returns>
    public async Task<bool> UpdateBaseUrl(
      string baseUrlId,
      string urlName,
      string baseUrl,
      bool isNamespaceInHost)
    {
        baseUrlId.AssertIsNotNullOrEmpty(nameof(baseUrlId), "Unable to update base url with null or empty baseUrlId argument.");
        urlName.AssertIsNotNullOrEmpty(nameof(urlName), "Unable to update base url with null or empty urlName argument.");
        baseUrl.AssertIsNotNullOrEmpty("name", "Unable to update base url with null or empty name argument.");
        isNamespaceInHost.AssertIsNotNull<bool>("virtualArray", "Unable to update base url with null or empty virtualArray argument.");
        objectBaseUrlUpdate data = new objectBaseUrlUpdate()
        {
            name = urlName,
            base_url = baseUrl,
            is_namespace_in_host = isNamespaceInHost
        };
        HttpResponseMessage resp = await this._httpClient.PutAsync<objectBaseUrlUpdate>(new Uri(string.Format("{0}/object/baseurl/{1}", (object)this.Client.EndPoint, (object)baseUrlId)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to update base url. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        data = (objectBaseUrlUpdate)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>Gets details for the specified Base URL.</summary>
    /// <param name="baseUrlId">The id of the base url for which to get details.</param>
    /// <returns>A objectBaseUrlRestRep object that represents the requested base url details.</returns>
    public async Task<objectBaseUrlRestRep> GetBaseUrl(string baseUrlId)
    {
        baseUrlId.AssertIsNotNullOrEmpty(nameof(baseUrlId), "Unable to get base url with null or empty baseUrlId argument.");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/baseurl/{1}", (object)this.Client.EndPoint, (object)baseUrlId)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get base url. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        objectBaseUrlRestRep payload = await resp.Content.ReadAsAsync<objectBaseUrlRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        objectBaseUrlRestRep baseUrl = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (objectBaseUrlRestRep)null;
        return baseUrl;
    }

    /// <summary>Lists all configured Base URLs.</summary>
    /// <returns>A baseUrlList object that represents all base urls configured in ECS.</returns>
    public async Task<baseUrlList> GetAllBaseUrls()
    {
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/baseurl", (object)this.Client.EndPoint)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get all base urls. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        baseUrlList payload = await resp.Content.ReadAsAsync<baseUrlList>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        baseUrlList allBaseUrls = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (baseUrlList)null;
        return allBaseUrls;
    }

    /// <summary>
    /// Gets list of configured commodity or filesystem data stores.
    /// </summary>
    /// <returns>A dataStoreList object listing data stores configured in the system.</returns>
    public async Task<dataStoreList> GetDataStoreList()
    {
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/vdc/data-stores", (object)this.Client.EndPoint)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get list of data stores. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        dataStoreList payload = await resp.Content.ReadAsAsync<dataStoreList>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        dataStoreList dataStoreList = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (dataStoreList)null;
        return dataStoreList;
    }

    /// <summary>
    /// Creates one or more new commodity data stores. A data store represents a commodity node providing backing storage for the object store.
    /// </summary>
    /// <param name="nodeId">The IP address for the commodity node.</param>
    /// <param name="name">The name for the data store (not verified or unique).</param>
    /// <param name="virtualArray">Desired storage pool id for creating the data store.</param>
    /// <param name="description">The description for the data store (not verified or unique).</param>
    /// <returns>A taskList object that represent the data store creation task list.</returns>
    public async Task<taskList> CreateDataStoreOnCommodity(
      string nodeId,
      string name,
      string virtualArray,
      string description)
    {
        nodeId.AssertIsNotNullOrEmpty(nameof(nodeId), "Unable to create data store on commodity with null or empty nodeId argument.");
        name.AssertIsNotNullOrEmpty(nameof(name), "Unable to create data store on commodity with null or empty name argument.");
        virtualArray.AssertIsNotNullOrEmpty(nameof(virtualArray), "Unable to create data store on commodity with null or empty virtualArray argument.");
        description.AssertIsNotNullOrEmpty(nameof(description), "Unable to create data store on commodity with null or empty description argument.");
        string storagePoolId = await this.QueryVirtualArray(virtualArray);
        commodityDataStoreParam data = new commodityDataStoreParam();
        commodityParam[] commodityParamArray = new commodityParam[1];
        commodityParam commodityParam = new commodityParam();
        commodityParam.nodeId = nodeId;
        commodityParam.name = name;
        commodityParam.virtual_array = storagePoolId;
        commodityParam.description = description;
        commodityParamArray[0] = commodityParam;
        commodityParam[] temp = commodityParamArray;
        data.nodes = temp;
        HttpResponseMessage resp = await this._httpClient.PostAsync<commodityDataStoreParam>(new Uri(string.Format("{0}/vdc/data-stores/commodity", (object)this.Client.EndPoint)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to create data store on commodity. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        taskList payload = await resp.Content.ReadAsAsync<taskList>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        taskList storeOnCommodity = payload;
        storagePoolId = (string)null;
        data = (commodityDataStoreParam)null;
        temp = (commodityParam[])null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (taskList)null;
        return storeOnCommodity;
    }

    /// <summary>Gets the details of a commodity data store.</summary>
    /// <param name="dataStoreId">The id of the data store for which to get details.</param>
    /// <returns>A commodityDataStoreRestRep object that represents the commodity data store hosting device information.</returns>
    public async Task<commodityDataStoreRestRep> GetCommodityDataStoreAssociatedWithStoragePool(
      string dataStoreId)
    {
        dataStoreId.AssertIsNotNullOrEmpty(nameof(dataStoreId), "Unable to get commodity data store associated with storage pool with null or empty dataStoreId argument.");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/vdc/data-stores/commodity/{1}", (object)this.Client.EndPoint, (object)dataStoreId)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get commodity data store associated with storage pool. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        commodityDataStoreRestRep payload = await resp.Content.ReadAsAsync<commodityDataStoreRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        commodityDataStoreRestRep associatedWithStoragePool = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (commodityDataStoreRestRep)null;
        return associatedWithStoragePool;
    }

    /// <summary>
    /// Gets the list of details of commodity data stores associated with a storage pool.
    /// </summary>
    /// <param name="virtualArrayId">The id of the virtual array for which to get data store details.</param>
    /// <returns>A commodityDataStoreList object that represents the commodity data stores associated with specified storage pool.</returns>
    public async Task<commodityDataStoreList> GetCommodityDataStoreAssociatedWithVarray(
      string virtualArrayId)
    {
        virtualArrayId.AssertIsNotNullOrEmpty(nameof(virtualArrayId), "Unable to get commodity data store associated with specified storage pool with null or empty virtualArrayId argument.");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/vdc/data-stores/commodity/search/varray/{1}", (object)this.Client.EndPoint, (object)virtualArrayId)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get commodity data store associated with specified storage pool. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        commodityDataStoreList payload = await resp.Content.ReadAsAsync<commodityDataStoreList>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        commodityDataStoreList associatedWithVarray = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (commodityDataStoreList)null;
        return associatedWithVarray;
    }

    /// <summary>Deactivates the commodity node and data store.</summary>
    /// <param name="dataStoreId">The id of the data store to delete.</param>
    /// <returns>A taskList object that represent the data store deletion task list.</returns>
    public async Task<taskList> DeactivateDataStore(string dataStoreId)
    {
        dataStoreId.AssertIsNotNullOrEmpty(nameof(dataStoreId), "Unable to deactivate data store with null or empty dataStoreId argument.");
        HttpResponseMessage resp = await this._httpClient.PostAsync(new Uri(string.Format("{0}/vdc/data-stores/{1}/deactivate", (object)this.Client.EndPoint, (object)dataStoreId)), (HttpContent)null);
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to deactivate the data store. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        taskList payload = await resp.Content.ReadAsAsync<taskList>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        taskList taskList = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (taskList)null;
        return taskList;
    }

    /// <summary>Get all recent tasks for a specific data store.</summary>
    /// <param name="dataStoreId">The id of the data store to query.</param>
    /// <param name="operationId">The id of the task operation of the data store to query.</param>
    /// <returns>A taskResourceRep object that represents all tasks for the data store.</returns>
    public async Task<taskResourceRep> GetDataStoreTask(
      string dataStoreId,
      string operationId)
    {
        dataStoreId.AssertIsNotNullOrEmpty(nameof(dataStoreId), "Unable to get data store tasks with null or empty dataStoreId argument.");
        operationId.AssertIsNotNullOrEmpty(nameof(operationId), "Unable to get data store tasks with null or empty operationId argument.");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/vdc/data-stores/{1}/tasks/{2}", (object)this.Client.EndPoint, (object)dataStoreId, (object)operationId)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get tasks for the data store. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        taskResourceRep payload = await resp.Content.ReadAsAsync<taskResourceRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        taskResourceRep dataStoreTask = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (taskResourceRep)null;
        return dataStoreTask;
    }

    /// <summary>
    /// Retrieves list of resource (data store) representations based on input ids.
    /// </summary>
    /// <param name="dataStoreIds">The ids of the data stores to query.</param>
    /// <returns>A dataStoreBulkRep object that represents the requested list.</returns>
    public async Task<dataStoreBulkRep> GetBulkResources(
      IEnumerable<string> dataStoreIds)
    {
        dataStoreIds.AssertIsNotNull<IEnumerable<string>>(nameof(dataStoreIds), "Unable to get bulk resources with null dataStoreIds argument.");
        bulkIdParam data = new bulkIdParam()
        {
            id = dataStoreIds.Select<string, string>((Func<string, string>)(d => d.ToString())).ToArray<string>()
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<bulkIdParam>(new Uri(string.Format("{0}/vdc/data-stores/bulk", (object)this.Client.EndPoint)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get bulk resources. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        dataStoreBulkRep payload = await resp.Content.ReadAsAsync<dataStoreBulkRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        dataStoreBulkRep bulkResources = payload;
        data = (bulkIdParam)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (dataStoreBulkRep)null;
        return bulkResources;
    }

    /// <summary>
    /// Gets the data nodes that are currently configured in the cluster.
    /// </summary>
    /// <returns>A dataNodeList object listing data nodes in the cluster.</returns>
    public async Task<dataNodeList> GetNodes()
    {
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/vdc/nodes", (object)this.Client.EndPoint)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get list of nodes. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        dataNodeList payload = await resp.Content.ReadAsAsync<dataNodeList>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        dataNodeList nodes = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (dataNodeList)null;
        return nodes;
    }

    /// <summary>Gets a list of storage pools from the local VDC.</summary>
    /// <param name="vdcId">VDC identifier from which storage pool information should be returned.</param>
    /// <returns>A virtualArrayList object listing storage pools from the local VDC.</returns>
    public async Task<virtualArrayList> GetVirtualArrays(string vdcId = null)
    {
        string uriVdcId = vdcId == null ? string.Empty : "?vdc-id=" + vdcId;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/vdc/data-services/varrays{1}", (object)this.Client.EndPoint, (object)uriVdcId)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get list of storage pools. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        virtualArrayList payload = await resp.Content.ReadAsAsync<virtualArrayList>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        virtualArrayList virtualArrays = payload;
        uriVdcId = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (virtualArrayList)null;
        return virtualArrays;
    }

    /// <summary>Gets the details for the specified storage pool.</summary>
    /// <param name="storagePoolId">The storage pool id for which to get details.</param>
    /// <returns>A virtualArrayRestRep object representing details for the specified storage pool.</returns>
    public async Task<virtualArrayRestRep> GetVirtualArray(
      string storagePoolId)
    {
        storagePoolId.AssertIsNotNullOrEmpty(nameof(storagePoolId), "Unable to get virtual array with null or empty storagePoolId argument.");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/vdc/data-services/varrays/{1}", (object)this.Client.EndPoint, (object)storagePoolId)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get virtual array. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        virtualArrayRestRep payload = await resp.Content.ReadAsAsync<virtualArrayRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        virtualArrayRestRep virtualArray = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (virtualArrayRestRep)null;
        return virtualArray;
    }

    /// <summary>Create a storage pool with the specified details.</summary>
    /// <param name="name">The name for the virtual array.</param>
    /// <param name="isProtected">Flag to indicate if virtual array is protected.</param>
    /// <param name="description">The description for the virtual array.</param>
    /// <returns>A virtualArrayRestRep object representing the newly created virtual array details.</returns>
    public async Task<virtualArrayRestRep> CreateVirtualArray(
      string name,
      bool isProtected,
      string description)
    {
        name.AssertIsNotNullOrEmpty(nameof(name), "Unable to create virtual array with null or empty name argument.");
        isProtected.AssertIsNotNull<bool>(nameof(isProtected), "Unable to create virtual array with null isProtected argument.");
        description.AssertIsNotNullOrEmpty(nameof(description), "Unable to create virtual array with null or empty description argument.");
        virtualArrayCreateParam data = new virtualArrayCreateParam()
        {
            name = name,
            isProtected = isProtected,
            description = description
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<virtualArrayCreateParam>(new Uri(string.Format("{0}/vdc/data-services/varrays", (object)this.Client.EndPoint)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to create virtual array. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        virtualArrayRestRep payload = await resp.Content.ReadAsAsync<virtualArrayRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        virtualArrayRestRep virtualArray = payload;
        data = (virtualArrayCreateParam)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (virtualArrayRestRep)null;
        return virtualArray;
    }

    /// <summary>
    /// Update the storage pool with the specified identifier.
    /// </summary>
    /// <param name="storagePoolId">The storage pool id to be updated.</param>
    /// <param name="name">The name for the virtual array.</param>
    /// <param name="isProtected">Flag to indicate if virtual array is protected.</param>
    /// <param name="description">The description for the virtual array.</param>
    /// <returns>A virtualArrayRestRep object representing the updated virtual array details.</returns>
    public async Task<virtualArrayRestRep> UpdateVirtualArray(
      string storagePoolId,
      string name,
      bool isProtected,
      string description)
    {
        storagePoolId.AssertIsNotNullOrEmpty(nameof(storagePoolId), "Unable to update virtual array with null or empty storagePoolId argument.");
        name.AssertIsNotNullOrEmpty(nameof(name), "Unable to update virtual array with null or empty name argument.");
        isProtected.AssertIsNotNull<bool>(nameof(isProtected), "Unable to update virtual array with null isProtected argument.");
        description.AssertIsNotNullOrEmpty(nameof(description), "Unable to update virtual array with null or empty description argument.");
        virtualArrayUpdateParam data = new virtualArrayUpdateParam()
        {
            name = name,
            isProtected = isProtected,
            description = description
        };
        HttpResponseMessage resp = await this._httpClient.PutAsync<virtualArrayUpdateParam>(new Uri(string.Format("{0}/vdc/data-services/varrays/{1}", (object)this.Client.EndPoint, (object)storagePoolId)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to update virtual array. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        virtualArrayRestRep payload = await resp.Content.ReadAsAsync<virtualArrayRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        virtualArrayRestRep virtualArrayRestRep = payload;
        data = (virtualArrayUpdateParam)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (virtualArrayRestRep)null;
        return virtualArrayRestRep;
    }

    /// <summary>
    /// Deletes the storage pool for the specified identifier.
    /// </summary>
    /// <param name="storagePoolId">The storage pool identifier to be deleted.</param>
    /// <returns>A boolean indicating if the virtual array was deleted successfully.</returns>
    public async Task<bool> DeleteVirtualArray(string storagePoolId)
    {
        storagePoolId.AssertIsNotNullOrEmpty(nameof(storagePoolId), "Unable to delete virtual array with null or empty storagePoolId argument.");
        HttpResponseMessage resp = await this._httpClient.DeleteAsync(new Uri(string.Format("{0}/vdc/data-services/varrays/{1}", (object)this.Client.EndPoint, (object)storagePoolId)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to delete virtual array. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>
    /// Using provided virtual array name, lookup the corresponding id.
    /// </summary>
    /// <param name="virtualArrayName">The name of the virtual array for which to lookup the id.</param>
    /// <returns>A string that represents the virtual array id.</returns>
    private async Task<string> QueryVirtualArray(string virtualArrayName)
    {
        virtualArrayName.AssertIsNotNullOrEmpty(nameof(virtualArrayName), "Unable to lookup the virtual array id with null or empty virtualArrayName argument.");
        virtualArrayList result = await this.GetVirtualArrays();
        virtualArrayRestRep[] virtualArrayRestRepArray = result.varray;
        for (int index = 0; index < virtualArrayRestRepArray.Length; ++index)
        {
            virtualArrayRestRep item = virtualArrayRestRepArray[index];
            if (item.name == virtualArrayName)
            {
                string id = item.id;
                result = (virtualArrayList)null;
                return id;
            }
            item = (virtualArrayRestRep)null;
        }
        virtualArrayRestRepArray = (virtualArrayRestRep[])null;
        throw new ArgumentException(string.Format("Unable to lookup virutal array id using specified name: {0}.", (object)virtualArrayName), nameof(virtualArrayName));
    }

    /// <summary>
    /// Insert the attributes for the current VDC or a VDC which you want the current VDC to connect.
    /// </summary>
    /// <param name="vdcName">The name of the VDC to be inserted.</param>
    /// <param name="interVdcEndPoints">End points for the VDC.</param>
    /// <param name="interVdcCmdEndPoints">Control plane and end points for the VDC.</param>
    /// <param name="secretKeys">Secret key to encrypt communication between VDC.</param>
    /// <returns>A boolean indicating if the operation completed successfully.</returns>
    public async Task<bool> InsertVdcAttributes(
      string vdcName,
      string interVdcEndPoints,
      string secretKeys,
      string interVdcCmdEndPoints = null)
    {
        vdcName.AssertIsNotNullOrEmpty(nameof(vdcName), "Unable to insert VDC attributes with null or empty vdcName argument.");
        interVdcEndPoints.AssertIsNotNullOrEmpty(nameof(interVdcEndPoints), "Unable to insert VDC attributes with null or empty interVdcEndPoints argument.");
        secretKeys.AssertIsNotNullOrEmpty(nameof(secretKeys), "Unable to insert VDC attributes with null or empty secretKeys argument.");
        vdcInsertParam data = new vdcInsertParam()
        {
            vdcName = vdcName,
            interVdcEndPoints = interVdcEndPoints,
            interVdcCmdEndPoints = interVdcCmdEndPoints,
            secretKeys = secretKeys
        };
        HttpResponseMessage resp = await this._httpClient.PutAsync<vdcInsertParam>(new Uri(string.Format("{0}/object/vdcs/vdc/{1}", (object)this.Client.EndPoint, (object)vdcName)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to insert VDC attributes. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        bool flag = true;
        data = (vdcInsertParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>Deactivates and deletes a VDC.</summary>
    /// <param name="vdcId">VDC identifier for which VDC information needs to be deleted.</param>
    /// <returns>A boolean indicating if the operation completed successfully.</returns>
    public async Task<bool> DeactivateVdc(string vdcId)
    {
        vdcId.AssertIsNotNullOrEmpty(nameof(vdcId), "Unable to deactivate VDC with null or empty vdcId argument.");
        HttpResponseMessage resp = await this._httpClient.PostAsync(new Uri(string.Format("{0}/object/vdcs/vdc/{1}/deactivate", (object)this.Client.EndPoint, (object)vdcId)), (HttpContent)null);
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to deactivate VDC. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        bool flag = true;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>Gets the details for a specific VDC based on name.</summary>
    /// <param name="vdcName">VDC name for which VDC Information is to be retrieved.</param>
    /// <returns>A vdcRestRep that represents the details of the requested VDC.</returns>
    public async Task<vdcRestRep> GetVdcByName(string vdcName)
    {
        vdcName.AssertIsNotNullOrEmpty(nameof(vdcName), "Unable to get VDC by name with null or empty vdcName argument.");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/vdcs/vdc/{1}", (object)this.Client.EndPoint, (object)vdcName)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get VDC by name. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        vdcRestRep payload = await resp.Content.ReadAsAsync<vdcRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        vdcRestRep vdcByName = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (vdcRestRep)null;
        return vdcByName;
    }

    /// <summary>Gets the details for a specific VDC based on id.</summary>
    /// <param name="vdcId">VDC id for which VDC Information is to be retrieved.</param>
    /// <returns>A vdcRestRep that represents the details of the requested VDC.</returns>
    public async Task<vdcRestRep> GetVdcById(string vdcId)
    {
        vdcId.AssertIsNotNullOrEmpty(nameof(vdcId), "Unable to get VDC by id null or empty vdcId argument.");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/vdcs/vdcid/{1}", (object)this.Client.EndPoint, (object)vdcId)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get VDC by id. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        vdcRestRep payload = await resp.Content.ReadAsAsync<vdcRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        vdcRestRep vdcById = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (vdcRestRep)null;
        return vdcById;
    }

    /// <summary>Gets all details of all configured VDCs.</summary>
    /// <returns>A vdcList object listing VDC information in the system.</returns>
    public async Task<vdcList> GetAllVdcDetails()
    {
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/vdcs/vdc/list", (object)this.Client.EndPoint)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get list of VDCs. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        vdcList payload = await resp.Content.ReadAsAsync<vdcList>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        vdcList allVdcDetails = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (vdcList)null;
        return allVdcDetails;
    }

    /// <summary>Gets the details for the local VDC.</summary>
    /// <returns>A vdcRestRep object that represents the detail of the local VDC in the system.</returns>
    public async Task<vdcRestRep> GetLocalVdc()
    {
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/vdcs/vdc/local", (object)this.Client.EndPoint)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get local VDC. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        vdcRestRep payload = await resp.Content.ReadAsAsync<vdcRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        vdcRestRep localVdc = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (vdcRestRep)null;
        return localVdc;
    }

    /// <summary>Gets the secret key details for the local VDC.</summary>
    /// <returns>A vdcSecretKeyRestRep object containing the secret key for the local VDC.</returns>
    public async Task<vdcSecretKeyRestRep> GetLocalVdcSecretKey()
    {
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/vdcs/vdc/local/secretkey", (object)this.Client.EndPoint)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get local VDC secret key. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        vdcSecretKeyRestRep payload = await resp.Content.ReadAsAsync<vdcSecretKeyRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        vdcSecretKeyRestRep localVdcSecretKey = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (vdcSecretKeyRestRep)null;
        return localVdcSecretKey;
    }

    /// <summary>
    /// Using provided virtual data center name, lookup the corresponding id.
    /// </summary>
    /// <param name="virtualDataCenterName">The name of the virtual data center for which to lookup the id.</param>
    /// <returns>A string that represents the virtual data center id.</returns>
    private async Task<string> QueryVirtualDataCenter(string virtualDataCenterName)
    {
        virtualDataCenterName.AssertIsNotNullOrEmpty(nameof(virtualDataCenterName), "Unable to lookup the virtual data center id with null or empty virtualDataCenterName argument.");
        vdcList result = await this.GetAllVdcDetails();
        vdcRestRep[] vdcRestRepArray = result.vdc;
        for (int index = 0; index < vdcRestRepArray.Length; ++index)
        {
            vdcRestRep item = vdcRestRepArray[index];
            if (item.name == virtualDataCenterName)
            {
                string id = item.id;
                result = (vdcList)null;
                return id;
            }
            item = (vdcRestRep)null;
        }
        vdcRestRepArray = (vdcRestRep[])null;
        throw new ArgumentException(string.Format("Unable to lookup virutal data center id using specified name: {0}.", (object)virtualDataCenterName), nameof(virtualDataCenterName));
    }

    /// <summary>
    /// Gets billing details for the specified namespace and bucket details.
    /// </summary>
    /// <param name="nameSpace">The namespace to get billing information about.</param>
    /// <param name="sizeUnit">Unit to be used for calculating the size on disk (KB,MB and GB. GB is default value).</param>
    /// <param name="includeBucketDetail">If true, include information about all the buckets owned by this namespace. When bucket details are included, the total size on disk can be different to the total size without bucket details. This is due to bucket size being rounded and summed to give the total size.</param>
    /// <param name="marker">Used to continue a truncated response. Omit this parameter on the first request.</param>
    /// <returns>A namespaceBillingRestRep object representing the billing details for the namespace.</returns>
    public async Task<namespaceBillingRestRep> GetNamespaceBillingInfo(
      string nameSpace,
      string sizeUnit = null,
      bool includeBucketDetail = false,
      string marker = null)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to get namespace billing details with a null or emtpy namespace argument.");
        Dictionary<string, string> uriParams = new Dictionary<string, string>();
        if (sizeUnit != null)
        {
            if (!(sizeUnit == "KB") && !(sizeUnit == "MB") && !(sizeUnit == "GB"))
                throw new ArgumentException("Unable to get namespace billing details with the provided sizeUnit pameter.", nameof(sizeUnit));
            uriParams.Add("sizeunit", sizeUnit);
        }
        if (includeBucketDetail)
            uriParams.Add("include_bucket_detail", "true");
        if (marker != null)
            uriParams.Add(nameof(marker), marker);
        string[] arrayUriParams = uriParams.Select<KeyValuePair<string, string>, string>((Func<KeyValuePair<string, string>, string>)(d => string.Format("{0}={1}", (object)d.Key, (object)d.Value))).ToArray<string>();
        string uriParamsResult = arrayUriParams.Length != 0 ? "?" + string.Join("&", arrayUriParams) : string.Empty;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/billing/namespace/{1}/info{2}", (object)this.Client.EndPoint, (object)nameSpace, (object)uriParamsResult)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation && resp.StatusCode != HttpStatusCode.NotFound)
            throw new InvalidOperationException(string.Format("Failed to get namespace billing info. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        namespaceBillingRestRep payload = await resp.Content.ReadAsAsync<namespaceBillingRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        namespaceBillingRestRep namespaceBillingInfo = payload;
        uriParams = (Dictionary<string, string>)null;
        arrayUriParams = (string[])null;
        uriParamsResult = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (namespaceBillingRestRep)null;
        return namespaceBillingInfo;
    }

    /// <summary>
    /// Gets billing details for the specified namespace and bucket name.
    /// </summary>
    /// <param name="nameSpace">The namespace to get billing information about.</param>
    /// <param name="bucketName">The bucket name to get billing information about.</param>
    /// <param name="sizeUnit">Unit to be used for calculating the size on disk (KB,MB and GB. GB is default value).</param>
    /// <returns>A bucketBillingInfoRestRep object representing the billing details for the bucket and namespace.</returns>
    public async Task<bucketBillingInfoRestRep> GetBucketBillingInfo(
      string nameSpace,
      string bucketName,
      string sizeUnit = null)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to get bucket billing details with a null or emtpy namespace argument.");
        bucketName.AssertIsNotNullOrEmpty(nameof(bucketName), "Unable to get bucket billing details with a null or emtpy bucketName argument.");
        string uriSizeUnit = string.Empty;
        if (sizeUnit != null)
        {
            if (!(sizeUnit == "KB") && !(sizeUnit == "MB") && !(sizeUnit == "GB"))
                throw new ArgumentException("Unable to get namespace billing details with the provided sizeUnit pameter.", nameof(sizeUnit));
            uriSizeUnit = "?sizeunit=" + sizeUnit;
        }
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/billing/buckets/{1}/{2}/info{3}", (object)this.Client.EndPoint, (object)nameSpace, (object)bucketName, (object)uriSizeUnit)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation && resp.StatusCode != HttpStatusCode.NotFound)
            throw new InvalidOperationException(string.Format("Failed to get bucket billing info. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        bucketBillingInfoRestRep payload = await resp.Content.ReadAsAsync<bucketBillingInfoRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        bucketBillingInfoRestRep bucketBillingInfo = payload;
        uriSizeUnit = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (bucketBillingInfoRestRep)null;
        return bucketBillingInfo;
    }

    /// <summary>
    /// Gets billing details for the specified namespace, interval and, optionally, buckets.
    /// </summary>
    /// <param name="nameSpace">The namespace to get billing information about.</param>
    /// <param name="sizeUnit">Unit to be used for calculating the size on disk (KB,MB and GB. GB is default value).</param>
    /// <param name="startTime">Starting time (inclusive) for the sample(s) in ISO-8601 minute format. Must be a multiple of 5 minutes.</param>
    /// <param name="endTime">Ending time (inclusive) for the sample(s) in ISO-8601 minute format. Must be a multiple of 5 minutes.</param>
    /// <param name="includeBucketDetail">If true, include information about all the buckets owned by this namespace. When bucket details are included, the total size on disk can be different to the total size without bucket details. This is due to bucket size being rounded and summed to give the total size.</param>
    /// <param name="marker">Used to continue a truncated response. Omit this parameter on the first request.</param>
    /// <returns>A namespaceBillingSampleRestRep object that contains the billing information for the given time period for the namespace and its buckets.</returns>
    public async Task<namespaceBillingSampleRestRep> GetNamespaceBillingSample(
      string nameSpace,
      DateTime startTime,
      DateTime endTime,
      string sizeUnit = null,
      bool includeBucketDetail = false,
      string marker = null)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to get namespace billing sample with a null or emtpy namespace argument.");
        startTime.AssertIsNotNull<DateTime>(nameof(startTime), "Unable to get namespace billing sample with a null or emtpy startTime argument.");
        endTime.AssertIsNotNull<DateTime>(nameof(endTime), "Unable to get namespace billing sample with a null or emtpy endTime argument.");
        string uriStartTime = startTime.ToString("yyyy-MM-ddThh:mm");
        string uriEndTime = endTime.ToString("yyyy-MM-ddThh:mm");
        Dictionary<string, string> uriParams = new Dictionary<string, string>();
        uriParams.Add("start_time", uriStartTime);
        uriParams.Add("end_time", uriEndTime);
        if (sizeUnit != null)
        {
            if (!(sizeUnit == "KB") && !(sizeUnit == "MB") && !(sizeUnit == "GB"))
                throw new ArgumentException("Unable to get namespace billing sample with the provided sizeUnit pameter.", nameof(sizeUnit));
            uriParams.Add("sizeunit", sizeUnit);
        }
        if (includeBucketDetail)
            uriParams.Add("include_bucket_detail", "true");
        if (marker != null)
            uriParams.Add(nameof(marker), marker);
        string[] arrayUriParams = uriParams.Select<KeyValuePair<string, string>, string>((Func<KeyValuePair<string, string>, string>)(d => string.Format("{0}={1}", (object)d.Key, (object)d.Value))).ToArray<string>();
        string uriParamsResult = arrayUriParams.Length != 0 ? "?" + string.Join("&", arrayUriParams) : string.Empty;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/billing/namespace/{1}/sample{2}", (object)this.Client.EndPoint, (object)nameSpace, (object)uriParamsResult)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation && resp.StatusCode != HttpStatusCode.NotFound)
            throw new InvalidOperationException(string.Format("Failed to get namespace billing info. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        namespaceBillingSampleRestRep payload = await resp.Content.ReadAsAsync<namespaceBillingSampleRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        namespaceBillingSampleRestRep namespaceBillingSample = payload;
        uriStartTime = (string)null;
        uriEndTime = (string)null;
        uriParams = (Dictionary<string, string>)null;
        arrayUriParams = (string[])null;
        uriParamsResult = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (namespaceBillingSampleRestRep)null;
        return namespaceBillingSample;
    }

    /// <summary>
    /// Gets billing details for the specified namespace, interval and bucket details.
    /// </summary>
    /// <param name="nameSpace">The namespace to get billing information about.</param>
    /// <param name="bucketName">The bucket to get billing information about.</param>
    /// <param name="sizeUnit">Unit to be used for calculating the size on disk (KB,MB and GB. GB is default value).</param>
    /// <param name="startTime">Starting time (inclusive) for the sample(s) in ISO-8601 minute format. Must be a multiple of 5 minutes.</param>
    /// <param name="endTime">Ending time (inclusive) for the sample(s) in ISO-8601 minute format. Must be a multiple of 5 minutes.</param>
    /// <returns>A bucketBillingSampleRestRep object that contains the billing information for the given time period for the bucket.</returns>
    public async Task<bucketBillingSampleRestRep> GetBucketBillingSample(
      string nameSpace,
      string bucketName,
      DateTime startTime,
      DateTime endTime,
      string sizeUnit = null)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to get bucket billing sample with a null or emtpy namespace argument.");
        bucketName.AssertIsNotNullOrEmpty(nameof(bucketName), "Unable to get bucket billing sample with a null or emtpy bucketName argument.");
        startTime.AssertIsNotNull<DateTime>(nameof(startTime), "Unable to get bucket billing sample with a null or emtpy startTime argument.");
        endTime.AssertIsNotNull<DateTime>(nameof(endTime), "Unable to get bucket billing sample with a null or emtpy endTime argument.");
        string uriStartTime = startTime.ToString("yyyy-MM-ddThh:mm");
        string uriEndTime = endTime.ToString("yyyy-MM-ddThh:mm");
        Dictionary<string, string> uriParams = new Dictionary<string, string>();
        uriParams.Add("start_time", uriStartTime);
        uriParams.Add("end_time", uriEndTime);
        if (sizeUnit != null)
        {
            if (!(sizeUnit == "KB") && !(sizeUnit == "MB") && !(sizeUnit == "GB"))
                throw new ArgumentException("Unable to get bucket billing sample with the provided sizeUnit pameter.", nameof(sizeUnit));
            uriParams.Add("sizeunit", sizeUnit);
        }
        string[] arrayUriParams = uriParams.Select<KeyValuePair<string, string>, string>((Func<KeyValuePair<string, string>, string>)(d => string.Format("{0}={1}", (object)d.Key, (object)d.Value))).ToArray<string>();
        string uriParamsResult = arrayUriParams.Length != 0 ? "?" + string.Join("&", arrayUriParams) : string.Empty;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/billing/buckets/{1}/{2}/sample{3}", (object)this.Client.EndPoint, (object)nameSpace, (object)bucketName, (object)uriParamsResult)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation && resp.StatusCode != HttpStatusCode.NotFound)
            throw new InvalidOperationException(string.Format("Failed to get bucket billing info. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        bucketBillingSampleRestRep payload = await resp.Content.ReadAsAsync<bucketBillingSampleRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        bucketBillingSampleRestRep bucketBillingSample = payload;
        uriStartTime = (string)null;
        uriEndTime = (string)null;
        uriParams = (Dictionary<string, string>)null;
        arrayUriParams = (string[])null;
        uriParamsResult = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (bucketBillingSampleRestRep)null;
        return bucketBillingSample;
    }

    /// <summary>Creates a new object user in ECS.</summary>
    /// <param name="userName">The user name for the new object user.</param>
    /// <param name="nameSpace">The namespace under which to create the new object user.</param>
    /// <param name="tags">A list of arbitrary tags to assign to the new user. These can be used to track additional information about the user and will also appear on bucket billing responses for buckets owned by the user.</param>
    /// <returns>A secretKeyInfoRep object showing details about the object user.</returns>
    public async Task<secretKeyInfoRep> CreateObjectUser(
      string userName,
      string nameSpace,
      IDictionary<string, string> tags = null)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to create object user with null or empty userName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to create object user with null or empty nameSpace argument.");
        IDictionary<string, string> source = tags;
        userTagParam[] tagsArray = source != null ? source.Select<KeyValuePair<string, string>, userTagParam>((Func<KeyValuePair<string, string>, userTagParam>)(a => new userTagParam()
        {
            name = a.Key,
            value = a.Value
        })).ToArray<userTagParam>() : (userTagParam[])null;
        userCreateParam data = new userCreateParam()
        {
            user = userName,
            @namespace = nameSpace,
            tags = tagsArray
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<userCreateParam>(new Uri(string.Format("{0}/object/users", (object)this.Client.EndPoint)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to create object user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        secretKeyInfoRep payload = await resp.Content.ReadAsAsync<secretKeyInfoRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        secretKeyInfoRep objectUser = payload;
        tagsArray = (userTagParam[])null;
        data = (userCreateParam)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (secretKeyInfoRep)null;
        return objectUser;
    }

    /// <summary>Deletes a new object user in ECS.</summary>
    /// <param name="userName">The user name for the object user to delete.</param>
    /// <param name="nameSpace">The namespace under which the object user exists.</param>
    /// <returns>A bool indicating if object user was deleted successfully.</returns>
    public async Task<bool> DeleteObjectUser(string userName, string nameSpace)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to delete object user with null or empty userName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to delete object user with null or empty nameSpace argument.");
        userDeleteParam data = new userDeleteParam()
        {
            user = userName,
            @namespace = nameSpace
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<userDeleteParam>(new Uri(string.Format("{0}/object/users/deactivate", (object)this.Client.EndPoint)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to delete object user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        data = (userDeleteParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>
    /// Gets user details for the specified user belong to the specified namespace.
    /// </summary>
    /// <param name="userName">The user identifier used to retrieve details.</param>
    /// <param name="nameSpace">The namespace to which the specified user belongs.</param>
    /// <returns>A userInfoRestRep object showing details about the object user.</returns>
    public async Task<userInfoRestRep> GetObjectUser(
      string userName,
      string nameSpace = null)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to get object user with null or empty userName argument.");
        string uriNamespace = nameSpace == null ? string.Empty : "?namespace=" + nameSpace;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/users/{1}/info{2}", (object)this.Client.EndPoint, (object)userName, (object)uriNamespace)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get the object user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        userInfoRestRep payload = await resp.Content.ReadAsAsync<userInfoRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        userInfoRestRep objectUser = payload;
        uriNamespace = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (userInfoRestRep)null;
        return objectUser;
    }

    /// <summary>
    /// Gets the user tags for the specified user belonging to the specified namespace.
    /// </summary>
    /// <param name="userName">The user identifier used to retrieve details.</param>
    /// <param name="nameSpace">The namespace to which the specified user belongs.</param>
    /// <returns>A userTagRestRep object showing the tags associated with the specified object user.</returns>
    public async Task<userTagRestRep> GetObjectUserTags(
      string userName,
      string nameSpace = null)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to get object user tags with null or empty userName argument.");
        string uriNamespace = nameSpace == null ? string.Empty : "?namespace=" + nameSpace;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/users/{1}/tags{2}", (object)this.Client.EndPoint, (object)userName, (object)uriNamespace)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get the object user tags. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        userTagRestRep payload = await resp.Content.ReadAsAsync<userTagRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        userTagRestRep objectUserTags = payload;
        uriNamespace = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (userTagRestRep)null;
        return objectUserTags;
    }

    /// <summary>
    /// Updates values for existing tags on the user.  Be sure to pass a value for all existing tags.
    /// </summary>
    /// <param name="userName">The user identifier used to update corresponding tags.</param>
    /// <param name="nameSpace">The namespace to which the specified user belongs.</param>
    /// <param name="tags">A list of key/value pairs representing new values to replace existing user tags.</param>
    /// <returns>A bool indicating if the operation completed successfully.</returns>
    public async Task<bool> UpdateObjectUserTags(
      string userName,
      IDictionary<string, string> tags,
      string nameSpace = null)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to update object user tags with null or empty userName argument.");
        tags.AssertIsNotNull<IDictionary<string, string>>(nameof(tags), "Unable to update object user tags object user tags with null or empty tags argument.");
        IDictionary<string, string> source = tags;
        userTagParam[] tagsArray = source != null ? source.Select<KeyValuePair<string, string>, userTagParam>((Func<KeyValuePair<string, string>, userTagParam>)(a => new userTagParam()
        {
            name = a.Key,
            value = a.Value
        })).ToArray<userTagParam>() : (userTagParam[])null;
        string uriNamespace = nameSpace == null ? string.Empty : "?namespace=" + nameSpace;
        userUpdateParam data = new userUpdateParam()
        {
            tags = tagsArray
        };
        HttpResponseMessage resp = await this._httpClient.PutAsync<userUpdateParam>(new Uri(string.Format("{0}/object/users/{1}/tags{2}", (object)this.Client.EndPoint, (object)userName, (object)uriNamespace)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to update the object user tags. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        tagsArray = (userTagParam[])null;
        uriNamespace = (string)null;
        data = (userUpdateParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>Appends tags on the user.</summary>
    /// <param name="userName">The user identifier used to update corresponding tags.</param>
    /// <param name="nameSpace">The namespace to which the specified user belongs.</param>
    /// <param name="tags">A list of key/value pairs representing new values to be appended existing user tags.</param>
    /// <returns>A bool indicating if the operation completed successfully.</returns>
    public async Task<bool> AppendObjectUserTags(
      string userName,
      IDictionary<string, string> tags,
      string nameSpace = null)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to append object user tags with null or empty userName argument.");
        tags.AssertIsNotNull<IDictionary<string, string>>(nameof(tags), "Unable to append object user tags object user tags with null or empty tags argument.");
        IDictionary<string, string> source = tags;
        userTagParam[] tagsArray = source != null ? source.Select<KeyValuePair<string, string>, userTagParam>((Func<KeyValuePair<string, string>, userTagParam>)(a => new userTagParam()
        {
            name = a.Key,
            value = a.Value
        })).ToArray<userTagParam>() : (userTagParam[])null;
        string uriNamespace = nameSpace == null ? string.Empty : "?namespace=" + nameSpace;
        userUpdateParam data = new userUpdateParam()
        {
            tags = tagsArray
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<userUpdateParam>(new Uri(string.Format("{0}/object/users/{1}/tags{2}", (object)this.Client.EndPoint, (object)userName, (object)uriNamespace)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to append the object user tags. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        tagsArray = (userTagParam[])null;
        uriNamespace = (string)null;
        data = (userUpdateParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>
    /// Queries for object users using the provided tag name and tag value.
    /// </summary>
    /// <param name="tagName">The name of the tag to use in the query for matching object users.</param>
    /// <param name="tagValue">The value of the tag to use in the query for matching object users.</param>
    /// <param name="nameSpace">The namespace under which the object user exists.</param>
    /// <param name="marker">A reference (obtained from previous request's response) indicating where to start next page results.</param>
    /// <param name="limit">The number of object users to be returned in the current request.</param>
    /// <returns>A usersList object representing a list of objects users that have matching values for tagName and tagValue.</returns>
    public async Task<usersList> QueryObjectUsers(
      string tagName,
      string tagValue,
      string nameSpace = null,
      string marker = null,
      int limit = 0)
    {
        tagName.AssertIsNotNullOrEmpty(nameof(tagName), "Unable to query object users with null or empty tagName argument.");
        tagValue.AssertIsNotNullOrEmpty(nameof(tagValue), "Unable to query object users with null or empty tagValue argument.");
        Dictionary<string, string> uriParams = new Dictionary<string, string>();
        if (limit > 0)
            uriParams.Add(nameof(limit), limit.ToString());
        if (marker != null)
            uriParams.Add(nameof(marker), marker);
        if (nameSpace != null)
            uriParams.Add("namespace", nameSpace);
        uriParams.Add("tag", tagName);
        uriParams.Add("value", tagValue);
        string[] arrayUriParams = uriParams.Select<KeyValuePair<string, string>, string>((Func<KeyValuePair<string, string>, string>)(d => string.Format("{0}={1}", (object)d.Key, (object)d.Value))).ToArray<string>();
        string uriParamsResult = arrayUriParams.Length != 0 ? "?" + string.Join("&", arrayUriParams) : string.Empty;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/users/query{1}", (object)this.Client.EndPoint, (object)uriParamsResult)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to query the object users. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        usersList payload = await resp.Content.ReadAsAsync<usersList>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        usersList usersList = payload;
        uriParams = (Dictionary<string, string>)null;
        arrayUriParams = (string[])null;
        uriParamsResult = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (usersList)null;
        return usersList;
    }

    /// <summary>Gets all users for the specified namespace.</summary>
    /// <param name="nameSpace">The namespace under which to create the new object user.</param>
    /// <returns>A usersList object listing user information for the specific Namespace.</returns>
    public async Task<usersList> GetObjectUsers(string nameSpace = null)
    {
        string uriNamespace = nameSpace == null ? string.Empty : "/" + nameSpace;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/users{1}", (object)this.Client.EndPoint, (object)uriNamespace)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get the object users. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        usersList payload = await resp.Content.ReadAsAsync<usersList>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        usersList objectUsers = payload;
        uriNamespace = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (usersList)null;
        return objectUsers;
    }

    /// <summary>
    /// Locks or unlocks the specified user. If the user belongs to a namespace, the namespace must be supplied
    /// </summary>
    /// <param name="userName">User name to be locked/unlocked.</param>
    /// <param name="nameSpace">The namespace for the specified user.</param>
    /// <param name="lockedFlag">Flag indicating if user is to be locked/unlocked.</param>
    /// <returns>A bool indicating of the locked flag was set successfully.</returns>
    public async Task<bool> SetUserLock(string userName, string nameSpace, bool lockedFlag)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to set object user lock with null or empty userName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to set object user lock with null or empty nameSpace argument.");
        lockedFlag.AssertIsNotNull<bool>(nameof(lockedFlag), "Unable to set object user lock with null or empty lockedFlag argument.");
        userLockParam data = new userLockParam()
        {
            user = userName,
            @namespace = nameSpace,
            isLocked = lockedFlag
        };
        HttpResponseMessage resp = await this._httpClient.PutAsync<userLockParam>(new Uri(string.Format("{0}/object/users/lock", (object)this.Client.EndPoint)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to set lock on object user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        data = (userLockParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>
    /// Gets the user lock state for the specified user belonging to the specified namespace.
    /// </summary>
    /// <param name="userName">User name for which user lock status should be returned.</param>
    /// <param name="nameSpace">The namespace to which the object user belongs.</param>
    /// <returns>A JSON object indicating the object user's locked state.</returns>
    public async Task<userLockRestRep> GetUserLock(
      string userName,
      string nameSpace = null)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to get object user lock with null or empty userName argument.");
        string uriNamespace = nameSpace == null ? string.Empty : "/" + nameSpace;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/users/lock/{1}{2}", (object)this.Client.EndPoint, (object)userName, (object)uriNamespace)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get the object user lock state. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        userLockRestRep payload = await resp.Content.ReadAsAsync<userLockRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        userLockRestRep userLock = payload;
        uriNamespace = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (userLockRestRep)null;
        return userLock;
    }

    /// <summary>Creates a new management object user in ECS.</summary>
    /// <param name="userName">The user name for the new management user.</param>
    /// <param name="password">The password for the new management user.</param>
    /// <param name="isSystemAdmin">Flag indicating whether management user is System Admin.</param>
    /// <param name="isSystemMonitor">Flag indicating whether management user is System Monitor.</param>
    /// <returns>A JSON object representing the newly created management user.</returns>
    public async Task<mgmtUserInfoRestRep> CreateManagementUser(
      string userName,
      string password,
      bool isSystemAdmin,
      bool isSystemMonitor)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to create management user with null or empty userName argument.");
        password.AssertIsNotNullOrEmpty(nameof(password), "Unable to create management user with null or empty password argument.");
        isSystemAdmin.AssertIsNotNull<bool>(nameof(isSystemAdmin), "Unable to create management user with null isSystemAdmin argument.");
        isSystemMonitor.AssertIsNotNull<bool>(nameof(isSystemMonitor), "Unable to create management user with null isSystemMonitor argument.");
        mgmtUserInfoCreate data = new mgmtUserInfoCreate()
        {
            userId = userName,
            password = password,
            isSystemAdmin = isSystemAdmin,
            isSystemMonitor = isSystemMonitor
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<mgmtUserInfoCreate>(new Uri(string.Format("{0}/vdc/users", (object)this.Client.EndPoint)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to create object user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        mgmtUserInfoRestRep payload = await resp.Content.ReadAsAsync<mgmtUserInfoRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        mgmtUserInfoRestRep managementUser = payload;
        data = (mgmtUserInfoCreate)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (mgmtUserInfoRestRep)null;
        return managementUser;
    }

    /// <summary>Deletes a new management object user in ECS.</summary>
    /// <param name="userName">The user name of the new management user to be deleted.</param>
    /// <returns>A bool indicating if the management user was deleted successfully.</returns>
    public async Task<bool> DeleteManagementUser(string userName)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to delete management user with null or empty userName argument.");
        HttpResponseMessage resp = await this._httpClient.PostAsync(new Uri(string.Format("{0}/vdc/users/{1}/deactivate", (object)this.Client.EndPoint, (object)userName)), (HttpContent)null);
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to delete object user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>
    /// Updates the user details for the specified management user.
    /// </summary>
    /// <param name="userName">The user name for the management user.</param>
    /// <param name="password">The password for the management user.</param>
    /// <param name="isSystemAdmin">Flag indicating whether management user is System Admin.</param>
    /// <param name="isSystemMonitor">Flag indicating whether management user is System Monitor.</param>
    /// <returns>A bool indicating if the management user was updated successfully.</returns>
    public async Task<bool> UpdateManagementUser(
      string userName,
      string password,
      bool isSystemAdmin,
      bool isSystemMonitor)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to update management user with null or empty userName argument.");
        password.AssertIsNotNullOrEmpty(nameof(password), "Unable to update management user with null or empty password argument.");
        isSystemAdmin.AssertIsNotNull<bool>(nameof(isSystemAdmin), "Unable to update management user with null isSystemAdmin argument.");
        isSystemMonitor.AssertIsNotNull<bool>(nameof(isSystemMonitor), "Unable to update management user with null isSystemMonitor argument.");
        mgmtUserInfoUpdate data = new mgmtUserInfoUpdate()
        {
            password = password,
            isSystemAdmin = isSystemAdmin,
            isSystemMonitor = isSystemMonitor
        };
        HttpResponseMessage resp = await this._httpClient.PutAsync<mgmtUserInfoUpdate>(new Uri(string.Format("{0}/vdc/users/{1}", (object)this.Client.EndPoint, (object)userName)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to update object user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        data = (mgmtUserInfoUpdate)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>Gets details for the specified local management user.</summary>
    /// <param name="userName">User identifier for which local user information needs to be retrieved.</param>
    /// <returns>A JSON object representing the retrieved management user.</returns>
    public async Task<mgmtUserInfoRestRep> GetManagementUser(string userName)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to get management user with null or empty userName argument.");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/vdc/users/{1}", (object)this.Client.EndPoint, (object)userName)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get management user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        mgmtUserInfoRestRep payload = await resp.Content.ReadAsAsync<mgmtUserInfoRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        mgmtUserInfoRestRep managementUser = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (mgmtUserInfoRestRep)null;
        return managementUser;
    }

    /// <summary>Gets all configured local management users.</summary>
    /// <returns>A JSON object representing the retrieved management users.</returns>
    public async Task<mgmtUserInfoList> GetManagementUsers()
    {
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/vdc/users", (object)this.Client.EndPoint)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get management users. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        mgmtUserInfoList payload = await resp.Content.ReadAsAsync<mgmtUserInfoList>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        mgmtUserInfoList managementUsers = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (mgmtUserInfoList)null;
        return managementUsers;
    }

    /// <summary>Creates a namespace with the given details.</summary>
    /// <param name="nameSpace">The name to use for the new namepsace.</param>
    /// <param name="namespaceAdmins">The admin name(s) to use for the new namepsace.</param>
    /// <param name="defaultDataServicesVpool">The ID of the replication group to be used for the namespace.</param>
    /// <param name="defaultObjectProject">Default project id for this tenant when creating buckets.</param>
    /// <param name="allowedVpoolList">List of replication groups that are allowed to create buckets on the corresponding namespace.</param>
    /// <param name="disallowedVpoolList">List of replication groups that are not allowed to create buckets on the corresponding namespace.</param>
    /// <returns>A namespaceRestRep object representing newly created namespace.</returns>
    public async Task<namespaceRestRep> CreateNamespace(
      string nameSpace,
      IEnumerable<string> namespaceAdmins,
      string defaultDataServicesVpool,
      string defaultObjectProject,
      IEnumerable<string> allowedVpoolList,
      IEnumerable<string> disallowedVpoolList)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to create namesapce with null or empty nameSpace argument.");
        namespaceAdmins.AssertIsNotNull<IEnumerable<string>>(nameof(namespaceAdmins), "Unable to create namesapce with null namespaceAdmins argument.");
        defaultDataServicesVpool.AssertIsNotNullOrEmpty(nameof(defaultDataServicesVpool), "Unable to create namesapce with null or empty defaultDataServicesVpool argument.");
        defaultObjectProject.AssertIsNotNull<string>(nameof(defaultObjectProject), "Unable to create namesapce with null or empty defaultObjectProject argument.");
        allowedVpoolList.AssertIsNotNull<IEnumerable<string>>(nameof(allowedVpoolList), "Unable to create namesapce with null allowedVpoolList argument.");
        disallowedVpoolList.AssertIsNotNull<IEnumerable<string>>(nameof(disallowedVpoolList), "Unable to create namesapce with null disallowedVpoolList argument.");
        namespaceCreateParam data = new namespaceCreateParam()
        {
            @namespace = nameSpace,
            namespace_admins = string.Join(",", namespaceAdmins.Select<string, string>((Func<string, string>)(a => a.ToString()))),
            default_data_services_vpool = defaultDataServicesVpool,
            default_object_project = defaultObjectProject,
            allowed_vpools_list = allowedVpoolList.ToArray<string>(),
            disallowed_vpools_list = disallowedVpoolList.ToArray<string>()
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<namespaceCreateParam>(new Uri(string.Format("{0}/object/namespaces/namespace", (object)this.Client.EndPoint)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to create the namespace. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        namespaceRestRep payload = await resp.Content.ReadAsAsync<namespaceRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        namespaceRestRep namespaceRestRep = payload;
        data = (namespaceCreateParam)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (namespaceRestRep)null;
        return namespaceRestRep;
    }

    /// <summary>Creates a namespace with the given details.</summary>
    /// <param name="nameSpace">The name to use for the new namepsace.</param>
    /// <param name="namespaceAdmins">The admin name(s) to use for the new namepsace.</param>
    /// <param name="defaultDataServicesVpool">The ID of the replication group to be used for the namespace.</param>
    /// <param name="defaultObjectProject">Default project id for this tenant when creating buckets.</param>
    /// <param name="allowedVpoolList">List of replication groups that are allowed to create buckets on the corresponding namespace.</param>
    /// <param name="disallowedVpoolList">List of replication groups that are not allowed to create buckets on the corresponding namespace.</param>
    /// <param name="defaultBucketBlockSize">Default bucket quota size for buckets created in this namespace.</param>
    /// <param name="defaultIsStaleAllowed">Default access during outage (ADO) for buckets created in this namespace in the event of temporary site outage (TSO).</param>
    /// <param name="defaultEncryption">Default buckets to use data at rest encryption (D@re) that are created in this namespace.</param>
    /// <param name="enableCompliance">Enable compliance mode on namespace.</param>
    /// <returns>A namespaceRestRep object representing newly created namespace.</returns>
    public async Task<namespaceRestRep> CreateNamespace(
      string nameSpace,
      IEnumerable<string> namespaceAdmins,
      string defaultDataServicesVpool,
      string defaultObjectProject = null,
      IEnumerable<string> allowedVpoolList = null,
      IEnumerable<string> disallowedVpoolList = null,
      long defaultBucketBlockSize = 0,
      bool defaultIsStaleAllowed = false,
      bool defaultEncryption = false,
      bool enableCompliance = false)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to create namesapce with null or empty nameSpace argument.");
        namespaceAdmins.AssertIsNotNull<IEnumerable<string>>(nameof(namespaceAdmins), "Unable to create namesapce with null namespaceAdmins argument.");
        defaultDataServicesVpool.AssertIsNotNullOrEmpty(nameof(defaultDataServicesVpool), "Unable to create namesapce with null or empty defaultDataServicesVpool argument.");
        namespaceCreateParam namespaceCreateParam = new namespaceCreateParam();
        namespaceCreateParam.@namespace = nameSpace;
        namespaceCreateParam.namespace_admins = string.Join(",", namespaceAdmins.Select<string, string>((Func<string, string>)(a => a.ToString())));
        namespaceCreateParam.default_data_services_vpool = defaultDataServicesVpool;
        namespaceCreateParam.default_object_project = defaultObjectProject;
        IEnumerable<string> source1 = allowedVpoolList;
        namespaceCreateParam.allowed_vpools_list = source1 != null ? source1.ToArray<string>() : (string[])null;
        IEnumerable<string> source2 = disallowedVpoolList;
        namespaceCreateParam.disallowed_vpools_list = source2 != null ? source2.ToArray<string>() : (string[])null;
        namespaceCreateParam.default_bucket_block_size = defaultBucketBlockSize;
        namespaceCreateParam.default_bucket_block_sizeSpecified = defaultBucketBlockSize > 0L;
        namespaceCreateParam.is_stale_allowed = defaultIsStaleAllowed;
        namespaceCreateParam.is_stale_allowedSpecified = defaultIsStaleAllowed;
        namespaceCreateParam.is_encryption_enabled = defaultEncryption;
        namespaceCreateParam.is_encryption_enabledSpecified = defaultEncryption;
        namespaceCreateParam.compliance_enabled = enableCompliance;
        namespaceCreateParam.compliance_enabledSpecified = enableCompliance;
        namespaceCreateParam data = namespaceCreateParam;
        HttpResponseMessage resp = await this._httpClient.PostAsync<namespaceCreateParam>(new Uri(string.Format("{0}/object/namespaces/namespace", (object)this.Client.EndPoint)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to create the namespace. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        namespaceRestRep payload = await resp.Content.ReadAsAsync<namespaceRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        namespaceRestRep namespaceRestRep = payload;
        data = (namespaceCreateParam)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (namespaceRestRep)null;
        return namespaceRestRep;
    }

    /// <summary>
    /// Updates namespace details like replication group list, namespace admins and user mappings.
    /// </summary>
    /// <param name="nameSpace">The name to use for the namepsace.</param>
    /// <param name="namespaceAdmins">The admin name(s) to assign to the new namepsace.</param>
    /// <param name="defaultDataServicesVpool">The ID of the replication group to be used for the namespace.</param>
    /// <param name="addAllowedVpoolList">List of replication group identifier which will be added in the allowed List for allowing namespace access.</param>
    /// <param name="addDisallowedVpoolList">List of replication group identifier which will be added in the disAllowed list for prohibiting namespace access</param>
    /// <param name="removeAllowedVpool">List of replication group identifier which will be removed from allowed list.</param>
    /// <param name="removeDisallowedVpool">List of replication group identifier which will be removed from disAllowed list for removing their prohibition namespace access.</param>
    /// <returns>A boolean indicating if the namespace was updated successfully.</returns>
    public async Task<bool> UpdateNamespace(
      string nameSpace,
      IEnumerable<string> namespaceAdmins,
      string defaultDataServicesVpool,
      IEnumerable<string> addAllowedVpoolList,
      IEnumerable<string> addDisallowedVpoolList,
      IEnumerable<string> removeAllowedVpool,
      IEnumerable<string> removeDisallowedVpool)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to update namesapce with null or empty nameSpace argument.");
        namespaceAdmins.AssertIsNotNull<IEnumerable<string>>(nameof(namespaceAdmins), "Unable to update namesapce with null namespaceAdmins argument.");
        defaultDataServicesVpool.AssertIsNotNullOrEmpty(nameof(defaultDataServicesVpool), "Unable to update namesapce with null or empty defaultDataServicesVpool argument.");
        addAllowedVpoolList.AssertIsNotNull<IEnumerable<string>>(nameof(addAllowedVpoolList), "Unable to update namesapce with null addAllowedVpoolList argument.");
        addDisallowedVpoolList.AssertIsNotNull<IEnumerable<string>>(nameof(addDisallowedVpoolList), "Unable to update namesapce with null addDisallowedVpoolList argument.");
        removeAllowedVpool.AssertIsNotNull<IEnumerable<string>>(nameof(removeAllowedVpool), "Unable to update namesapce with null removeAllowedVpool argument.");
        removeDisallowedVpool.AssertIsNotNull<IEnumerable<string>>(nameof(removeDisallowedVpool), "Unable to update namesapce with null removeDisallowedVpool argument.");
        namespaceUpdateParam data = new namespaceUpdateParam()
        {
            default_data_services_vpool = defaultDataServicesVpool,
            vpools_added_to_allowed_vpools_list = addAllowedVpoolList.ToArray<string>(),
            vpools_added_to_disallowed_vpools_list = addDisallowedVpoolList.ToArray<string>(),
            vpools_removed_from_allowed_vpools_list = removeAllowedVpool.ToArray<string>(),
            vpools_removed_from_disallowed_vpools_list = removeDisallowedVpool.ToArray<string>(),
            namespace_admins = string.Join(",", namespaceAdmins.Select<string, string>((Func<string, string>)(a => a.ToString())))
        };
        HttpResponseMessage resp = await this._httpClient.PutAsync<namespaceUpdateParam>(new Uri(string.Format("{0}/object/namespaces/namespace/{1}", (object)this.Client.EndPoint, (object)nameSpace)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to update the namespace. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        data = (namespaceUpdateParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>
    /// Updates namespace details like replication group list, namespace admins and user mappings.
    /// </summary>
    /// <param name="nameSpace">The name to use for the namepsace.</param>
    /// <param name="namespaceAdmins">The admin name(s) to assign to the new namepsace.</param>
    /// <param name="defaultDataServicesVpool">The ID of the replication group to be used for the namespace.</param>
    /// <param name="addAllowedVpoolList">List of replication group identifier which will be added in the allowed List for allowing namespace access.</param>
    /// <param name="addDisallowedVpoolList">List of replication group identifier which will be added in the disAllowed list for prohibiting namespace access</param>
    /// <param name="removeAllowedVpool">List of replication group identifier which will be removed from allowed list.</param>
    /// <param name="removeDisallowedVpool">List of replication group identifier which will be removed from disAllowed list for removing their prohibition namespace access.</param>
    /// <param name="defaultBucketBlockSize">Default bucket quota size for buckets created in this namespace.</param>
    /// <param name="defaultIsStaleAllowed">Default access during outage (ADO) for buckets created in this namespace in the event of temporary site outage (TSO).</param>
    /// <returns>A boolean indicating if the namespace was updated successfully.</returns>
    public async Task<bool> UpdateNamespace(
      string nameSpace,
      IEnumerable<string> namespaceAdmins,
      string defaultDataServicesVpool,
      IEnumerable<string> addAllowedVpoolList = null,
      IEnumerable<string> addDisallowedVpoolList = null,
      IEnumerable<string> removeAllowedVpool = null,
      IEnumerable<string> removeDisallowedVpool = null,
      long defaultBucketBlockSize = 0,
      bool defaultIsStaleAllowed = false)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to update namesapce with null or empty nameSpace argument.");
        namespaceAdmins.AssertIsNotNull<IEnumerable<string>>(nameof(namespaceAdmins), "Unable to update namesapce with null namespaceAdmins argument.");
        defaultDataServicesVpool.AssertIsNotNullOrEmpty(nameof(defaultDataServicesVpool), "Unable to update namesapce with null or empty defaultDataServicesVpool argument.");
        namespaceUpdateParam namespaceUpdateParam = new namespaceUpdateParam();
        namespaceUpdateParam.default_data_services_vpool = defaultDataServicesVpool;
        IEnumerable<string> source1 = addAllowedVpoolList;
        namespaceUpdateParam.vpools_added_to_allowed_vpools_list = source1 != null ? source1.ToArray<string>() : (string[])null;
        IEnumerable<string> source2 = addDisallowedVpoolList;
        namespaceUpdateParam.vpools_added_to_disallowed_vpools_list = source2 != null ? source2.ToArray<string>() : (string[])null;
        IEnumerable<string> source3 = removeAllowedVpool;
        namespaceUpdateParam.vpools_removed_from_allowed_vpools_list = source3 != null ? source3.ToArray<string>() : (string[])null;
        IEnumerable<string> source4 = removeDisallowedVpool;
        namespaceUpdateParam.vpools_removed_from_disallowed_vpools_list = source4 != null ? source4.ToArray<string>() : (string[])null;
        namespaceUpdateParam.namespace_admins = string.Join(",", namespaceAdmins.Select<string, string>((Func<string, string>)(a => a.ToString())));
        namespaceUpdateParam.default_bucket_block_size = defaultBucketBlockSize;
        namespaceUpdateParam.default_bucket_block_sizeSpecified = defaultBucketBlockSize > 0L;
        namespaceUpdateParam.is_stale_allowed = defaultIsStaleAllowed;
        namespaceUpdateParam.is_stale_allowedSpecified = defaultIsStaleAllowed;
        namespaceUpdateParam data = namespaceUpdateParam;
        HttpResponseMessage resp = await this._httpClient.PutAsync<namespaceUpdateParam>(new Uri(string.Format("{0}/object/namespaces/namespace/{1}", (object)this.Client.EndPoint, (object)nameSpace)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to update the namespace. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        data = (namespaceUpdateParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>
    /// Deactivates and deletes the given namespace and all associated user mappings.
    /// </summary>
    /// <param name="nameSpace">The name of the namespace to delete.</param>
    /// <returns>A bool indicating if namesapce was deleted successfully.</returns>
    public async Task<bool> DeleteNamespace(string nameSpace)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to delete namesapce with null or empty nameSpace argument.");
        HttpResponseMessage resp = await this._httpClient.PostAsync(new Uri(string.Format("{0}/object/namespaces/namespace/{1}/deactivate", (object)this.Client.EndPoint, (object)nameSpace)), (HttpContent)null);
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to delete the namespace. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>Gets the details for the given namespace.</summary>
    /// <param name="nameSpace">The name of the namesapce to be retrieved.</param>
    /// <returns>A JSON object representing the namespace.</returns>
    public async Task<namespaceRestRep> GetNamespace(string nameSpace)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to get namesapce with null or empty nameSpace argument.");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/namespaces/namespace/{1}", (object)this.Client.EndPoint, (object)nameSpace)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get the namespace. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        namespaceRestRep payload = await resp.Content.ReadAsAsync<namespaceRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        namespaceRestRep namespaceRestRep = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (namespaceRestRep)null;
        return namespaceRestRep;
    }

    /// <summary>Gets the identifiers for all configured namespaces.</summary>
    /// <param name="marker">Reference to last object returned.</param>
    /// <param name="limit">Number of object requested in current fetch.</param>
    /// <param name="prefix">Case sensitive prefix of the Namespace name with a wild card(*) Ex : any_prefix_string*</param>
    /// <returns>A JSON object representing a list of all configured namespaces.</returns>
    public async Task<namespaceList> GetNamespaces(
      string marker = null,
      int limit = 0,
      string prefix = null)
    {
        Dictionary<string, string> uriParams = new Dictionary<string, string>();
        if (limit > 0)
            uriParams.Add(nameof(limit), limit.ToString());
        if (marker != null)
            uriParams.Add(nameof(marker), marker);
        if (prefix != null)
            uriParams.Add(nameof(prefix), prefix);
        string[] arrayUriParams = uriParams.Select<KeyValuePair<string, string>, string>((Func<KeyValuePair<string, string>, string>)(d => string.Format("{0}={1}", (object)d.Key, (object)d.Value))).ToArray<string>();
        string uriParamsResult = arrayUriParams.Length != 0 ? "?" + string.Join("&", arrayUriParams) : string.Empty;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/namespaces{1}", (object)this.Client.EndPoint, (object)uriParamsResult)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get namespaces. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        namespaceList payload = await resp.Content.ReadAsAsync<namespaceList>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        namespaceList namespaces = payload;
        uriParams = (Dictionary<string, string>)null;
        arrayUriParams = (string[])null;
        uriParamsResult = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (namespaceList)null;
        return namespaces;
    }

    /// <summary>
    /// Updates the namespace quota for a specified namespace.
    /// </summary>
    /// <param name="nameSpace">Namespace identifier for which namespace quota details need to be updated.</param>
    /// <param name="blockSize">Block size in GB.</param>
    /// <param name="notificationSize">Notification size in GB.</param>
    /// <returns>A bool indicating if the namespace quota was updated successfully.</returns>
    public async Task<bool> UpdateNamespaceQuota(
      string nameSpace,
      long blockSize,
      long notificationSize)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to update namespace quota with a null or empty nameSpace argument.");
        blockSize.AssertIsNotNull<long>(nameof(blockSize), "Unable to update namespace quota with a null blockSize argument.");
        notificationSize.AssertIsNotNull<long>(nameof(notificationSize), "Unable to update namespace quota with null notificationSize argument.");
        namespaceQuotaRestRep data = new namespaceQuotaRestRep()
        {
            blockSize = blockSize,
            notificationSize = notificationSize
        };
        HttpResponseMessage resp = await this._httpClient.PutAsync<namespaceQuotaRestRep>(new Uri(string.Format("{0}/object/namespaces/namespace/{1}/quota", (object)this.Client.EndPoint, (object)nameSpace)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to update namespace quota. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        data = (namespaceQuotaRestRep)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>Get the namespace quota for a specified namespace.</summary>
    /// <param name="nameSpace">Namespace identifier for which namespace quota details will be retrieved.</param>
    /// <returns>A namespaceQuotaRestRep object representing the namespace quota details for the given namespace.</returns>
    public async Task<namespaceQuotaRestRep> GetNamespaceQuota(
      string nameSpace)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to get namespace quota with a null or empty nameSpace argument.");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/namespaces/namespace/{1}/quota", (object)this.Client.EndPoint, (object)nameSpace)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get the namespace quota. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        namespaceQuotaRestRep payload = await resp.Content.ReadAsAsync<namespaceQuotaRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        namespaceQuotaRestRep namespaceQuota = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (namespaceQuotaRestRep)null;
        return namespaceQuota;
    }

    /// <summary>
    /// Deletes the namespace quota for a specified namespace.
    /// </summary>
    /// <param name="nameSpace">Namespace identifier for which the namespace quota will be deleted.</param>
    /// <returns>A bool indicating if the namespace quota was deleted successfully.</returns>
    public async Task<bool> DeleteNamespaceQuota(string nameSpace)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to delete the namespace quota with a null or empty nameSpace argument.");
        HttpResponseMessage resp = await this._httpClient.DeleteAsync(new Uri(string.Format("{0}/object/namespaces/namespace/{1}/quota", (object)this.Client.EndPoint, (object)nameSpace)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to delete the namespace quota. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>
    /// Creates a retention class for the specified namespace.
    /// </summary>
    /// <param name="nameSpace">Namespace identifier for which retention class needs to created.</param>
    /// <param name="className">Name of retention class.</param>
    /// <param name="periodOfRetention">Period of the retention class in seconds.</param>
    /// <returns>A bool indicating if the retention class was created successfully.</returns>
    public async Task<bool> CreateRetentionClass(
      string nameSpace,
      string className,
      long periodOfRetention)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to create retention class with a null or empty nameSpace argument.");
        className.AssertIsNotNullOrEmpty(nameof(className), "Unable to create retention class with a null or empty className argument.");
        periodOfRetention.AssertIsNotNull<long>(nameof(periodOfRetention), "Unable to create retention class with null periodOfRetention argument.");
        retentionClassCreateParam data = new retentionClassCreateParam()
        {
            name = className,
            period = periodOfRetention
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<retentionClassCreateParam>(new Uri(string.Format("{0}/object/namespaces/namespace/{1}/retention", (object)this.Client.EndPoint, (object)nameSpace)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to create retention class. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool retentionClass = true;
        data = (retentionClassCreateParam)null;
        resp = (HttpResponseMessage)null;
        return retentionClass;
    }

    /// <summary>
    /// Updates a retention class for the specified namespace.
    /// </summary>
    /// <param name="nameSpace">Namespace identifier for which retention class needs to created.</param>
    /// <param name="className">Name of retention class.</param>
    /// <param name="periodOfRetention">Period of the retention class in seconds.</param>
    /// <returns>A bool indicating if the retention class was updated successfully.</returns>
    public async Task<bool> UpdateRetentionClass(
      string nameSpace,
      string className,
      long periodOfRetention)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to create retention class with a null or empty nameSpace argument.");
        className.AssertIsNotNullOrEmpty(nameof(className), "Unable to create retention class with a null or empty className argument.");
        periodOfRetention.AssertIsNotNull<long>(nameof(periodOfRetention), "Unable to create retention class with null periodOfRetention argument.");
        retentionClassUpdateParam data = new retentionClassUpdateParam()
        {
            period = periodOfRetention
        };
        HttpResponseMessage resp = await this._httpClient.PutAsync<retentionClassUpdateParam>(new Uri(string.Format("{0}/object/namespaces/namespace/{1}/retention/{2}", (object)this.Client.EndPoint, (object)nameSpace, (object)className)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to update retention class. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        data = (retentionClassUpdateParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>
    /// Gets the retention period for the given namespace and retention class.
    /// </summary>
    /// <param name="nameSpace">Namespace identifier for which retention class needs be retrieved.</param>
    /// <param name="className">Class name for which retention period needs to be retrieved.</param>
    /// <returns>A retentionClassInfoRep object representing the retention period for given namespace and retention class.</returns>
    public async Task<retentionClassInfoRep> GetRetentionClass(
      string nameSpace,
      string className)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to get retention class with a null or empty nameSpace argument.");
        className.AssertIsNotNullOrEmpty(nameof(className), "Unable to get retention class with a null or empty className argument.");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/namespaces/namespace/{1}/retention/{2}", (object)this.Client.EndPoint, (object)nameSpace, (object)className)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get the retention class. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        retentionClassInfoRep payload = await resp.Content.ReadAsAsync<retentionClassInfoRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        retentionClassInfoRep retentionClass = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (retentionClassInfoRep)null;
        return retentionClass;
    }

    /// <summary>
    /// Gets a list of retention classes for the given namespace.
    /// </summary>
    /// <param name="nameSpace">Namespace identifier for which retention classes need be retrieved.</param>
    /// <returns>A retentionClassListRep object representing the retention classes for the given namespace.</returns>
    public async Task<retentionClassListRep> GetRetentionClasses(
      string nameSpace)
    {
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to get retention classes with a null or empty nameSpace argument.");
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/namespaces/namespace/{1}/retention", (object)this.Client.EndPoint, (object)nameSpace)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get the list of retention classes. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        retentionClassListRep payload = await resp.Content.ReadAsAsync<retentionClassListRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        retentionClassListRep retentionClasses = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (retentionClassListRep)null;
        return retentionClasses;
    }

    /// <summary>Creates a new bucket in ECS.</summary>
    /// <param name="bucketName">The name to use for the new bucket.</param>
    /// <param name="vPool">The bucket vPool ID.</param>
    /// <param name="fileSystemEnabled">Flag indicating if file system is enabled.</param>
    /// <param name="headType">The object head type allowd to access the bucket.</param>
    /// <param name="staleAllowed">Flag to allow stale data in the bucket.</param>
    /// <param name="tsoReadOnly">During TSO provide only read-only access.</param>
    /// <param name="nameSpace">The namespace associated to the bucket user/tenant.</param>
    /// <param name="bucketOwner">The object user to be set as the bucket owner.</param>
    /// <param name="retention">The retention period for objects written to this bucket.</param>
    /// <param name="encryptionEnabled">Enable D@RE encryption on the bucket.</param>
    /// <returns>A objectBucketRestRep object representing the newly created bucket.</returns>
    public async Task<objectBucketRestRep> CreateBucket(
      string bucketName,
      string vPool,
      bool fileSystemEnabled,
      string headType,
      string nameSpace,
      bool staleAllowed,
      string bucketOwner = null,
      long retention = 0,
      bool encryptionEnabled = false,
      bool tsoReadOnly = false)
    {
        bucketName.AssertIsNotNullOrEmpty(nameof(bucketName), "Unable to create bucket with null or empty bucketName argument.");
        vPool.AssertIsNotNullOrEmpty(nameof(vPool), "Unable to create bucket with null or empty vPool argument.");
        fileSystemEnabled.AssertIsNotNull<bool>(nameof(fileSystemEnabled), "Unable to create bucket with null fileSystemEnabled argument.");
        headType.AssertIsNotNullOrEmpty(nameof(headType), "Unable to create bucket with null or empty headType argument.");
        staleAllowed.AssertIsNotNull<bool>(nameof(staleAllowed), "Unable to create bucket with null staleAllowed argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to create bucket with null or empty nameSpace argument.");
        if (tsoReadOnly && !staleAllowed)
            throw new ArgumentException(nameof(staleAllowed), "Unble to set tsoReadOnly to true with staleAllowed false.");
        objectBucketParam data = new objectBucketParam()
        {
            name = bucketName,
            vpool = vPool,
            filesystem_enabled = fileSystemEnabled,
            filesystem_enabledSpecified = fileSystemEnabled,
            head_type = headType,
            @namespace = nameSpace,
            is_stale_allowed = staleAllowed,
            is_stale_allowedSpecified = staleAllowed,
            is_tso_read_only = tsoReadOnly,
            is_tso_read_onlySpecified = tsoReadOnly,
            owner = bucketOwner == null ? string.Empty : bucketOwner,
            retention = retention,
            retentionSpecified = retention > 0L,
            is_encryption_enabled = encryptionEnabled,
            is_encryption_enabledSpecified = encryptionEnabled
        };
        HttpClient httpClient = this._httpClient;
        Uri requestUri = new Uri(string.Format("{0}/object/bucket", (object)this.Client.EndPoint));
        objectBucketParam objectBucketParam = data;
        XmlMediaTypeFormatterECS formatter = new XmlMediaTypeFormatterECS();
        formatter.UseXmlSerializer = true;
        HttpResponseMessage resp = await httpClient.PostAsync<objectBucketParam>(requestUri, objectBucketParam, (MediaTypeFormatter)formatter, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to create bucket. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        objectBucketRestRep payload = await resp.Content.ReadAsAsync<objectBucketRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        objectBucketRestRep bucket = payload;
        data = (objectBucketParam)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (objectBucketRestRep)null;
        return bucket;
    }

    /// <summary>Deletes the specified bucket.</summary>
    /// <param name="bucketName">The name of the bucket to delete.</param>
    /// <param name="nameSpace">The namespace to which the bucket is associated.</param>
    /// <returns>A bool indicating if bucket was deleted successfully.</returns>
    public async Task<bool> DeleteBucket(string bucketName, string nameSpace = null)
    {
        bucketName.AssertIsNotNullOrEmpty(nameof(bucketName), "Unable to delete bucket with null or empty bucketName argument.");
        string uriNamespace = nameSpace == null ? string.Empty : "?namespace=" + nameSpace;
        HttpResponseMessage resp = await this._httpClient.PostAsync(new Uri(string.Format("{0}/object/bucket/{1}/deactivate{2}", (object)this.Client.EndPoint, (object)bucketName, (object)uriNamespace)), (HttpContent)null);
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to delete bucket. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        uriNamespace = (string)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>
    /// Gets the list of buckets for the specified namespace. If namespace to this bucket creation does not exist then user's namespace is used.
    /// </summary>
    /// <param name="nameSpace">Namespace for which the buckets should be listed.</param>
    /// <param name="marker">Reference to last object returned.</param>
    /// <param name="limit">Number of object requested in current fetch.</param>
    /// <returns>A bucketListRestRep object representing a list of buckets.</returns>
    public async Task<bucketListRestRep> GetBuckets(
      string nameSpace = null,
      string marker = null,
      int limit = 0)
    {
        Dictionary<string, string> uriParams = new Dictionary<string, string>();
        if (limit > 0)
            uriParams.Add(nameof(limit), limit.ToString());
        if (marker != null)
            uriParams.Add(nameof(marker), marker);
        if (nameSpace != null)
            uriParams.Add("namespace", nameSpace);
        string[] arrayUriParams = uriParams.Select<KeyValuePair<string, string>, string>((Func<KeyValuePair<string, string>, string>)(d => string.Format("{0}={1}", (object)d.Key, (object)d.Value))).ToArray<string>();
        string uriParamsResult = arrayUriParams.Length != 0 ? "?" + string.Join("&", arrayUriParams) : string.Empty;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/bucket{1}", (object)this.Client.EndPoint, (object)uriParamsResult)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get buckets. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        bucketListRestRep payload = await resp.Content.ReadAsAsync<bucketListRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        bucketListRestRep buckets = payload;
        uriParams = (Dictionary<string, string>)null;
        arrayUriParams = (string[])null;
        uriParamsResult = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (bucketListRestRep)null;
        return buckets;
    }

    /// <summary>Sets a bucket's default retention.</summary>
    /// <param name="bucketName">The name of bucket used to set default retention period.</param>
    /// <param name="nameSpace">The namespace to which the bucket belongs.</param>
    /// <param name="retentionPeriod">The default retention period in seconds.</param>
    /// <param name="enforceRetention">Force every object to have retention set; objects without retention are rejected.</param>
    /// <param name="minimumFixedRetention">The minumum fixed retention value that can be set.</param>
    /// <param name="maximumFixedRetention">The maximum fixed retention value that can be set.</param>
    /// <param name="minimumVariableRetention">The minumum variable retention value that can be set.</param>
    /// <param name="maximumVariableRetention">The maximum variable retention value that can be set.</param>
    /// <returns>A bool indicating if the bucket retention period was successfully updated.</returns>
    public async Task<bool> SetBucketRetention(
      string bucketName,
      string nameSpace,
      int retentionPeriod,
      bool enforceRetention = false,
      long minimumFixedRetention = 0,
      long maximumFixedRetention = 0,
      long minimumVariableRetention = 0,
      long maximumVariableRetention = 0)
    {
        bucketName.AssertIsNotNullOrEmpty(nameof(bucketName), "Unable to set bucket retention with null or empty bucketName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to set bucket retention with null or empty nameSpace argument.");
        retentionPeriod.AssertIsNotNull<int>(nameof(retentionPeriod), "Unable to set bucket retention with null retentionPeriod argument.");
        retentionMinMaxGovernor retGvnr = (retentionMinMaxGovernor)null;
        if (enforceRetention)
            retGvnr = new retentionMinMaxGovernor()
            {
                enforce_retention = enforceRetention,
                enforce_retentionSpecified = enforceRetention,
                minimum_fixed_retention = minimumFixedRetention,
                minimum_fixed_retentionSpecified = minimumFixedRetention > 0L,
                maximum_fixed_retention = maximumFixedRetention,
                maximum_fixed_retentionSpecified = maximumFixedRetention > 0L,
                minimum_variable_retention = minimumVariableRetention,
                minimum_variable_retentionSpecified = minimumVariableRetention > 0L,
                maximum_variable_retention = maximumVariableRetention,
                maximum_variable_retentionSpecified = maximumVariableRetention > 0L
            };
        bucketRetentionUpdateParam data = new bucketRetentionUpdateParam()
        {
            period = (long)retentionPeriod,
            periodSpecified = true,
            @namespace = nameSpace
        };
        if (retGvnr != null)
            data.min_max_governor = retGvnr;
        HttpClient httpClient = this._httpClient;
        Uri requestUri = new Uri(string.Format("{0}/object/bucket/{1}/retention", (object)this.Client.EndPoint, (object)bucketName));
        bucketRetentionUpdateParam retentionUpdateParam = data;
        XmlMediaTypeFormatterECS formatter = new XmlMediaTypeFormatterECS();
        formatter.UseXmlSerializer = true;
        HttpResponseMessage resp = await httpClient.PutAsync<bucketRetentionUpdateParam>(requestUri, retentionUpdateParam, (MediaTypeFormatter)formatter, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to set bucket retention. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        retGvnr = (retentionMinMaxGovernor)null;
        data = (bucketRetentionUpdateParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>
    /// Gets the retention period setting for the specified bucket.
    /// </summary>
    /// <param name="bucketName">Bucket name for which the retention period setting will be retrieved</param>
    /// <param name="nameSpace">Namespace name to which the specified bucket is associated.</param>
    /// <returns>A bucketRetentionInfoRep object representing retention details about the specified bucket.</returns>
    public async Task<bucketRetentionInfoRep> GetBucketRetention(
      string bucketName,
      string nameSpace = null)
    {
        bucketName.AssertIsNotNullOrEmpty(nameof(bucketName), "Unable to get bucket retention info with null or empty bucketName argument.");
        string uriNamespace = nameSpace == null ? string.Empty : "?namespace=" + nameSpace;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/bucket/{1}/retention{2}", (object)this.Client.EndPoint, (object)bucketName, (object)uriNamespace)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get bucket retention info. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        bucketRetentionInfoRep payload = await resp.Content.ReadAsAsync<bucketRetentionInfoRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        bucketRetentionInfoRep bucketRetention = payload;
        uriNamespace = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (bucketRetentionInfoRep)null;
        return bucketRetention;
    }

    /// <summary>Get information about the provided bucket.</summary>
    /// <param name="bucketName">The bucket name used to get info.</param>
    /// <param name="nameSpace">The namespace associated. If it's null, the current user's namespace is used.</param>
    /// <returns>A objectBucketInfoRestRep object representing details about the specified bucket.</returns>
    public async Task<objectBucketInfoRestRep> GetBucketInfo(
      string bucketName,
      string nameSpace = null)
    {
        bucketName.AssertIsNotNullOrEmpty(nameof(bucketName), "Unable to get bucket info with null or empty bucketName argument.");
        string uriNamespace = nameSpace == null ? string.Empty : "?namespace=" + nameSpace;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/bucket/{1}/info{2}", (object)this.Client.EndPoint, (object)bucketName, (object)uriNamespace)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get bucket info. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        objectBucketInfoRestRep payload = await resp.Content.ReadAsAsync<objectBucketInfoRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        objectBucketInfoRestRep bucketInfo = payload;
        uriNamespace = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (objectBucketInfoRestRep)null;
        return bucketInfo;
    }

    /// <summary>Updates the owner for the specified bucket.</summary>
    /// <param name="bucketName">The name of bucket used to set default retention period.</param>
    /// <param name="nameSpace">The namespace to which the bucket belongs.</param>
    /// <param name="newOwner">The new owner of the bucket</param>
    /// <returns>A bool indicating if the bucket owner successfully updated.</returns>
    public async Task<bool> UpdateBucketOwner(
      string bucketName,
      string nameSpace,
      string newOwner)
    {
        bucketName.AssertIsNotNullOrEmpty(nameof(bucketName), "Unable to update the bucket owner with null or empty bucketName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to update the bucket owner with null or empty userACL argument.");
        newOwner.AssertIsNotNullOrEmpty("period", "Unable to update the bucket owner with null or empty groupACL argument.");
        updateBucketOwnerParam data = new updateBucketOwnerParam()
        {
            @namespace = nameSpace,
            new_owner = newOwner
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<updateBucketOwnerParam>(new Uri(string.Format("{0}/object/bucket/{1}/owner", (object)this.Client.EndPoint)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to update bucket owner. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        data = (updateBucketOwnerParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>
    /// Updates isStaleAllowed details for the specified bucket in order to enable access to the bucket during a temporary site outage.
    /// </summary>
    /// <param name="bucketName">The name of bucket used to set the is stale allowed flag.</param>
    /// <param name="nameSpace">The namespace to which the bucket belongs.</param>
    /// <param name="isStaleAllowed">Flag indicating if stale is allowed on the bucket.</param>
    /// <returns>A bool indicating if the bucket's is stale allowed property was successfully updated.</returns>
    public async Task<bool> UpdateBucketIsStaleAllowed(
      string bucketName,
      string nameSpace,
      bool isStaleAllowed)
    {
        bucketName.AssertIsNotNullOrEmpty(nameof(bucketName), "Unable to update the bucket is stale allowed flag with null or empty bucketName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to update the bucket is stale allowed flag with null or empty nameSpace argument.");
        isStaleAllowed.AssertIsNotNull<bool>(nameof(isStaleAllowed), "Unable to update the bucket is stale allowed flag with null isStaleAllowed argument.");
        updateBucketIsStaleAllowedParam data = new updateBucketIsStaleAllowedParam()
        {
            @namespace = nameSpace,
            is_stale_allowed = Convert.ToString(isStaleAllowed)
        };
        HttpClient httpClient = this._httpClient;
        Uri requestUri = new Uri(string.Format("{0}/object/bucket/{1}/isstaleallowed", (object)this.Client.EndPoint, (object)bucketName));
        updateBucketIsStaleAllowedParam staleAllowedParam = data;
        XmlMediaTypeFormatterECS formatter = new XmlMediaTypeFormatterECS();
        formatter.UseXmlSerializer = true;
        HttpResponseMessage resp = await httpClient.PostAsync<updateBucketIsStaleAllowedParam>(requestUri, staleAllowedParam, (MediaTypeFormatter)formatter, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to update bucket is stale allowed flag. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        data = (updateBucketIsStaleAllowedParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>Get information about the provided bucket.</summary>
    /// <param name="bucketName">The bucket name used to get lock info.</param>
    /// <param name="nameSpace">The namespace associated. If it's null, the current user's namespace is used.</param>
    /// <returns>A bucketLockRestRep object representing lock details about the specified bucket.</returns>
    public async Task<bucketLockRestRep> GetBucketLock(
      string bucketName,
      string nameSpace = null)
    {
        bucketName.AssertIsNotNullOrEmpty(nameof(bucketName), "Unable to get bucket info with null or empty bucketName argument.");
        string uriNamespace = nameSpace == null ? string.Empty : "?namespace=" + nameSpace;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/bucket/{1}/lock{2}", (object)this.Client.EndPoint, (object)bucketName, (object)uriNamespace)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get bucket locked flag. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        bucketLockRestRep payload = await resp.Content.ReadAsAsync<bucketLockRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        bucketLockRestRep bucketLock = payload;
        uriNamespace = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (bucketLockRestRep)null;
        return bucketLock;
    }

    /// <summary>
    /// Locks or unlocks the specified bucket. Current user's namespace is used.
    /// </summary>
    /// <param name="bucketName">The name of bucket used to set the locked status.</param>
    /// <param name="nameSpace">The namespace to which the bucket belongs.</param>
    /// <param name="isLocked">Flag indicating what to set the bucket locked status.</param>
    /// <returns>A bucketLockRestRep object showing the namespace of the specified bucket.</returns>
    public async Task<bucketLockRestRep> SetBucketLock(
      string bucketName,
      string nameSpace,
      bool isLocked)
    {
        bucketName.AssertIsNotNullOrEmpty(nameof(bucketName), "Unable to set the bucket locked flag with null or empty bucketName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to set the bucket locked flag with null or empty nameSpace argument.");
        isLocked.AssertIsNotNull<bool>(nameof(isLocked), "Unable to set the bucket locked flag with null isLocked argument.");
        bucketLockParam data = new bucketLockParam()
        {
            @namespace = nameSpace
        };
        HttpClient httpClient = this._httpClient;
        Uri requestUri = new Uri(string.Format("{0}/object/bucket/{1}/lock/{2}", (object)this.Client.EndPoint, (object)bucketName, (object)Convert.ToString(isLocked)));
        bucketLockParam bucketLockParam = data;
        XmlMediaTypeFormatterECS formatter = new XmlMediaTypeFormatterECS();
        formatter.UseXmlSerializer = true;
        HttpResponseMessage resp = await httpClient.PutAsync<bucketLockParam>(requestUri, bucketLockParam, (MediaTypeFormatter)formatter, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to set bucket locked flag. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        bucketLockRestRep payload = await resp.Content.ReadAsAsync<bucketLockRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        bucketLockRestRep bucketLockRestRep = payload;
        data = (bucketLockParam)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (bucketLockRestRep)null;
        return bucketLockRestRep;
    }

    /// <summary>
    /// Updates the quota for the specified bucket. The payload specifies a limit at which a notification will be raised in the event log and a limit at which access will be blocked.
    /// </summary>
    /// <param name="bucketName">The name of the bucket for which the quota is to be updated.</param>
    /// <param name="nameSpace">The namespace to which the bucket belongs.</param>
    /// <param name="blockSize">Block size in GB. Cannot be less than 1GB or use decimal values. Can be set to -1 to indicate quota value not defined.</param>
    /// <param name="notificationSize">Notification size in GB. Cannot be less than 1GB or use decimal values. Can be set to -1 to indicate quota value not defined.</param>
    /// <returns>A boolean indicating if the bucket quota was successfully updated.</returns>
    public async Task<bool> UpdateBucketQuota(
      string bucketName,
      string nameSpace,
      long blockSize,
      long notificationSize)
    {
        bucketName.AssertIsNotNullOrEmpty(nameof(bucketName), "Unable to update the bucket quota with null or empty bucketName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to update the bucket quota with null or empty nameSpace argument.");
        blockSize.AssertIsNotNull<long>(nameof(blockSize), "Unable to update the bucket quota with null blockSize argument.");
        notificationSize.AssertIsNotNull<long>(nameof(notificationSize), "Unable to update the bucket quota with null notificationSize argument.");
        bucketSetQuotaParam data = new bucketSetQuotaParam()
        {
            blockSize = blockSize,
            notificationSize = notificationSize,
            notificationSizeSpecified = true,
            blockSizeSpecified = true,
            @namespace = nameSpace
        };
        HttpClient httpClient = this._httpClient;
        Uri requestUri = new Uri(string.Format("{0}/object/bucket/{1}/quota", (object)this.Client.EndPoint, (object)bucketName));
        bucketSetQuotaParam bucketSetQuotaParam = data;
        XmlMediaTypeFormatterECS formatter = new XmlMediaTypeFormatterECS();
        formatter.UseXmlSerializer = true;
        HttpResponseMessage resp = await httpClient.PutAsync<bucketSetQuotaParam>(requestUri, bucketSetQuotaParam, (MediaTypeFormatter)formatter, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to update the bucket quota. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        data = (bucketSetQuotaParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>Gets the quota for the given bucket and namespace.</summary>
    /// <param name="bucketName">The name of the bucket for which quota is to be retrieved.</param>
    /// <param name="nameSpace">The namespace associated. If it's null, the current user's namespace is used.</param>
    /// <returns>A bucketLockRestRep object representing lock details about the specified bucket.</returns>
    public async Task<bucketQuotaRestRep> GetBucketQuota(
      string bucketName,
      string nameSpace = null)
    {
        bucketName.AssertIsNotNullOrEmpty(nameof(bucketName), "Unable to get bucket info with null or empty bucketName argument.");
        string uriNamespace = nameSpace == null ? string.Empty : "?namespace=" + nameSpace;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/bucket/{1}/quota{2}", (object)this.Client.EndPoint, (object)bucketName, (object)uriNamespace)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get bucket locked flag. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        bucketQuotaRestRep payload = await resp.Content.ReadAsAsync<bucketQuotaRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        bucketQuotaRestRep bucketQuota = payload;
        uriNamespace = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (bucketQuotaRestRep)null;
        return bucketQuota;
    }

    /// <summary>
    /// Deletes the quota setting for the given bucket and namespace.
    /// </summary>
    /// <param name="bucketName">The name of the bucket for which quota is to be retrieved.</param>
    /// <param name="nameSpace">The namespace associated. If it's null, the current user's namespace is used.</param>
    /// <returns>A bool indicating of the bucket quota was removed successfully.</returns>
    public async Task<bool> RemoveBucketQuota(string bucketName, string nameSpace = null)
    {
        bucketName.AssertIsNotNullOrEmpty(nameof(bucketName), "Unable to remove bucket quota setting with null or empty bucketName argument.");
        string uriNamespace = nameSpace == null ? string.Empty : "?namespace=" + nameSpace;
        HttpResponseMessage resp = await this._httpClient.DeleteAsync(new Uri(string.Format("{0}/object/bucket/{1}/quota{2}", (object)this.Client.EndPoint, (object)bucketName, (object)uriNamespace)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to remove bucket quota setting. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        uriNamespace = (string)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>Sets a bucket's ACL configuration.</summary>
    /// <param name="bucketName">The name of bucket used to set ACL information.</param>
    /// <param name="nameSpace">The namespace to which the bucket belongs.</param>
    /// <param name="bucketOwner">The name of bucket owner.</param>
    /// <param name="userACL">A collection of users and their corresponding permissions.</param>
    /// <param name="groupACL">A collection of groups and their corresponding permissions.</param>
    /// <param name="customACL">A collection of custom names and their corresponding permissions.</param>
    /// <returns>A bool indicating if the bucket ACL was successfully updated.</returns>
    public async Task<bool> SetBucketACL(
      string bucketName,
      string nameSpace,
      string bucketOwner,
      IDictionary<string, IEnumerable<string>> userACL,
      IDictionary<string, IEnumerable<string>> groupACL,
      IDictionary<string, IEnumerable<string>> customACL)
    {
        bucketName.AssertIsNotNullOrEmpty(nameof(bucketName), "Unable to set bucket ACL with null or empty bucketName argument.");
        userACL.AssertIsNotNull<IDictionary<string, IEnumerable<string>>>(nameof(userACL), "Unable to set bucket ACL with null userACL argument.");
        groupACL.AssertIsNotNull<IDictionary<string, IEnumerable<string>>>(nameof(groupACL), "Unable to set bucket ACL with null groupACL argument.");
        customACL.AssertIsNotNull<IDictionary<string, IEnumerable<string>>>(nameof(customACL), "Unable to set bucket ACL with null customACL argument.");
        bucketACLRep data = new bucketACLRep()
        {
            bucket = bucketName,
            @namespace = nameSpace
        };
        userAclRep[] uacl = userACL.Select<KeyValuePair<string, IEnumerable<string>>, userAclRep>((Func<KeyValuePair<string, IEnumerable<string>>, userAclRep>)(a => new userAclRep()
        {
            user = a.Key,
            permission = a.Value.Select<string, string>((Func<string, string>)(p => p.ToString())).ToArray<string>()
        })).ToArray<userAclRep>();
        groupAclRep[] gacl = groupACL.Select<KeyValuePair<string, IEnumerable<string>>, groupAclRep>((Func<KeyValuePair<string, IEnumerable<string>>, groupAclRep>)(a => new groupAclRep()
        {
            group = a.Key,
            permission = a.Value.Select<string, string>((Func<string, string>)(p => p.ToString())).ToArray<string>()
        })).ToArray<groupAclRep>();
        customGroupAclRep[] cacl = customACL.Select<KeyValuePair<string, IEnumerable<string>>, customGroupAclRep>((Func<KeyValuePair<string, IEnumerable<string>>, customGroupAclRep>)(a => new customGroupAclRep()
        {
            customgroup = a.Key,
            permission = a.Value.Select<string, string>((Func<string, string>)(p => p.ToString())).ToArray<string>()
        })).ToArray<customGroupAclRep>();
        aclRep aclStruct = new aclRep()
        {
            user_acl = uacl,
            group_acl = gacl,
            customgroup_acl = cacl,
            owner = bucketOwner
        };
        data.acl = aclStruct;
        HttpClient httpClient = this._httpClient;
        Uri requestUri = new Uri(string.Format("{0}/object/bucket/{1}/acl", (object)this.Client.EndPoint, (object)bucketName));
        bucketACLRep bucketAclRep = data;
        XmlMediaTypeFormatterECS formatter = new XmlMediaTypeFormatterECS();
        formatter.UseXmlSerializer = true;
        HttpResponseMessage resp = await httpClient.PutAsync<bucketACLRep>(requestUri, bucketAclRep, (MediaTypeFormatter)formatter, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to set bucket ACL. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        data = (bucketACLRep)null;
        uacl = (userAclRep[])null;
        gacl = (groupAclRep[])null;
        cacl = (customGroupAclRep[])null;
        aclStruct = (aclRep)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>Gets a bucket's ACL configuration.</summary>
    /// <param name="bucketName">The name of bucket used to get ACL information.</param>
    /// <param name="nameSpace">The namespace associated. If it's null, the current user's namespace is used.</param>
    /// <returns>A bucketACLRep object representing the ACL for the specified bucket.</returns>
    public async Task<bucketACLRep> GetBucketACL(
      string bucketName,
      string nameSpace = null)
    {
        bucketName.AssertIsNotNullOrEmpty(nameof(bucketName), "Unable to get bucket ACL with null or empty bucketName argument.");
        string uriNamespace = nameSpace == null ? string.Empty : "?namespace=" + nameSpace;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/bucket/{1}/acl{2}", (object)this.Client.EndPoint, (object)bucketName, (object)uriNamespace)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get bucket ACL. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        bucketACLRep payload = await resp.Content.ReadAsAsync<bucketACLRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        bucketACLRep bucketAcl = payload;
        uriNamespace = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (bucketACLRep)null;
        return bucketAcl;
    }

    /// <summary>Gets all ACL permissions.</summary>
    /// <returns>A dirPermissionListRep object representing the ACL permissions.</returns>
    public async Task<dirPermissionListRep> GetPermissions()
    {
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/bucket/acl/permissions", (object)this.Client.EndPoint)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get all ACL permissions. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        dirPermissionListRep payload = await resp.Content.ReadAsAsync<dirPermissionListRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        dirPermissionListRep permissions = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (dirPermissionListRep)null;
        return permissions;
    }

    /// <summary>Gets all ACL groups.</summary>
    /// <returns>A dirGroupListRep object representing the ACL permissions.</returns>
    public async Task<dirGroupListRep> GetGroups()
    {
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/bucket/acl/groups", (object)this.Client.EndPoint)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get all ACL groups. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        dirGroupListRep payload = await resp.Content.ReadAsAsync<dirGroupListRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        dirGroupListRep groups = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (dirGroupListRep)null;
        return groups;
    }

    /// <summary>Lists the configured authentication providers.</summary>
    /// <returns>A baseUrlList object that represents all base urls configured in ECS.</returns>
    public async Task<baseUrlList> GetAuthenticationProviders()
    {
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/baseurl", (object)this.Client.EndPoint)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get all base urls. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        baseUrlList payload = await resp.Content.ReadAsAsync<baseUrlList>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        baseUrlList authenticationProviders = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (baseUrlList)null;
        return authenticationProviders;
    }

    /// <summary>
    /// Creates password and group information for a specific user identifier.
    /// </summary>
    /// <param name="userName">The user id to for which to create the password group information.</param>
    /// <param name="password">The password to be created for the specified user.</param>
    /// <param name="nameSpace">The namespace within which the user resides.</param>
    /// <param name="groupList">The list of groups to which to user is a member.</param>
    /// <returns>A bool indicating if the group and password was created successfully.</returns>
    public async Task<bool> CreatePasswordGroupForSwiftUser(
      string userName,
      string password,
      string nameSpace,
      IEnumerable<string> groupList = null)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to create password group for user with a null or emtpy userName argument.");
        password.AssertIsNotNullOrEmpty(nameof(password), "Unable to create password group for user with a null or emtpy password argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to create password group for user with a null or emtpy namespace argument.");
        string[] tempGroups = groupList == null ? (string[])null : groupList.Select<string, string>((Func<string, string>)(g => g.ToString())).ToArray<string>();
        userPasswordGroupCreateParam data = new userPasswordGroupCreateParam()
        {
            password = password,
            @namespace = nameSpace,
            groups_list = tempGroups
        };
        HttpResponseMessage resp = await this._httpClient.PutAsync<userPasswordGroupCreateParam>(new Uri(string.Format("{0}/object/user-password/{1}", (object)this.Client.EndPoint, (object)userName)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to create password group for user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        bool groupForSwiftUser = true;
        tempGroups = (string[])null;
        data = (userPasswordGroupCreateParam)null;
        resp = (HttpResponseMessage)null;
        return groupForSwiftUser;
    }

    /// <summary>
    /// Updates password and group information for a specific user identifier.
    /// </summary>
    /// <param name="userName">The user id to for which to update the password group information.</param>
    /// <param name="password">The password to be updated for the specified user.</param>
    /// <param name="nameSpace">The namespace within which the user resides.</param>
    /// <param name="groupList">The list of groups to which to user is a member.</param>
    /// <returns>A bool indicating if the group and password was updated successfully.</returns>
    public async Task<bool> UpdatePasswordGroupForSwiftUser(
      string userName,
      string password,
      string nameSpace,
      IEnumerable<string> groupList = null)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to update password group for user with a null or emtpy userName argument.");
        password.AssertIsNotNullOrEmpty(nameof(password), "Unable to update password group for user with a null or emtpy password argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to update password group for user with a null or emtpy namespace argument.");
        string[] tempGroups = groupList == null ? (string[])null : groupList.Select<string, string>((Func<string, string>)(g => g.ToString())).ToArray<string>();
        userPasswordGroupUpdateParam data = new userPasswordGroupUpdateParam()
        {
            password = password,
            @namespace = nameSpace,
            groups_list = tempGroups
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<userPasswordGroupUpdateParam>(new Uri(string.Format("{0}/object/user-password/{1}", (object)this.Client.EndPoint, (object)userName)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to update password group for user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        bool flag = true;
        tempGroups = (string[])null;
        data = (userPasswordGroupUpdateParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>Gets all user groups for a specified user identifier.</summary>
    /// <param name="userName">The user id to for which to get the password group information.</param>
    /// <param name="nameSpace">The namespace for which to get the password group information.</param>
    /// <returns>A userGroupRestRep object that represents the password group list for swift user.</returns>
    public async Task<userGroupRestRep> GetGroupsForSwiftUser(
      string userName,
      string nameSpace = null)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to get password group list for user with a null or emtpy userName argument.");
        string uriNamespace = nameSpace == null ? string.Empty : "/" + nameSpace;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/user-password/{1}{2}", (object)this.Client.EndPoint, (object)userName, (object)uriNamespace)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to update password group for user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        userGroupRestRep payload = await resp.Content.ReadAsAsync<userGroupRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        userGroupRestRep groupsForSwiftUser = payload;
        uriNamespace = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (userGroupRestRep)null;
        return groupsForSwiftUser;
    }

    /// <summary>Deletes password group for a specified user.</summary>
    /// <param name="userName">The user id to for which to delete the password group information.</param>
    /// <param name="nameSpace">The namespace within which the user resides.</param>
    /// <returns>A bool indicating if the password group was successfully deleted.</returns>
    public async Task<bool> DeactivatePasswordGroupForSwiftUser(
      string userName,
      string nameSpace)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to delete password group for user with a null or emtpy userName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to delete password group for user with a null or emtpy namespace argument.");
        userPasswordGroupParam data = new userPasswordGroupParam()
        {
            @namespace = nameSpace
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<userPasswordGroupParam>(new Uri(string.Format("{0}/object/user-password/{1}/deactivate", (object)this.Client.EndPoint, (object)userName)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.NoContent && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to delete password group for user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        bool flag = true;
        data = (userPasswordGroupParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>Gets all secret keys for the specified user.</summary>
    /// <param name="userName">The user name for which to get secret keys.</param>
    /// <param name="nameSpace">The namespace for which to get secret keys.</param>
    /// <returns>A secretKeyRestRep object representing secret keys for the user including the timestamps of their creation.</returns>
    public async Task<secretKeyRestRep> GetKeysForUser(
      string userName,
      string nameSpace = null)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to get secret keys for user with null or empty userName argument.");
        string uriNamespace = nameSpace == null ? string.Empty : "/" + nameSpace;
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/user-secret-keys/{1}{2}", (object)this.Client.EndPoint, (object)userName, (object)uriNamespace)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get secret keys for user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        secretKeyRestRep payload = await resp.Content.ReadAsAsync<secretKeyRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        secretKeyRestRep keysForUser = payload;
        uriNamespace = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (secretKeyRestRep)null;
        return keysForUser;
    }

    /// <summary>
    /// Deletes a specified secret key for a user. If the system user scope is NAMESPACE, the user's namespace must be supplied.
    /// </summary>
    /// <param name="userName">The user from which to delete the secret key.</param>
    /// <param name="nameSpace">The namespace under which the user resides.</param>
    /// <param name="secretKey">The secret key to be deleted.</param>
    /// <returns>A boolean indiciating if the secret was successfully deleted.</returns>
    public async Task<bool> DeactivateSecretKey(
      string userName,
      string nameSpace,
      string secretKey)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to delete secret key with null or empty userName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to delete secret key with null or empty nameSpace argument.");
        secretKey.AssertIsNotNullOrEmpty(nameof(secretKey), "Unable to delete secret key with null or empty secretKey argument.");
        userSecretKeyDeleteParam data = new userSecretKeyDeleteParam()
        {
            @namespace = nameSpace,
            secret_key = secretKey
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<userSecretKeyDeleteParam>(new Uri(string.Format("{0}/object/user-secret-keys/{1}/deactivate", (object)this.Client.EndPoint, (object)userName)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to delete secret key. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        data = (userSecretKeyDeleteParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>
    /// Create a secret key for the specified user and namespace.
    /// </summary>
    /// <param name="userName">The ECS object user to be assigned the secret key.</param>
    /// <param name="nameSpace">The namespace under which the object user resides.</param>
    /// <param name="secretKey">Secret key associated with this user. If not provided, system will generate one.</param>
    /// <param name="existingKeyExpirationTime">Expiry time in minutes for the secret key. Note that nodes may cache secret keys for up to two minutes so old keys may not expire immediately.</param>
    /// <returns>A secretKeyInfoRep object representating the secret keys created including the timestamps of creation.</returns>
    public async Task<secretKeyInfoRep> CreateNewKeyForUser(
      string userName,
      string nameSpace,
      string secretKey = "",
      int existingKeyExpirationTime = 0)
    {
        userName.AssertIsNotNullOrEmpty(nameof(userName), "Unable to create secret key with null or empty userName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to create secret key with null or empty nameSpace argument.");
        string expireMins = existingKeyExpirationTime == 0 ? (string)null : Convert.ToString(existingKeyExpirationTime);
        userSecretKeyCreateParam data = new userSecretKeyCreateParam()
        {
            @namespace = nameSpace,
            secretkey = secretKey,
            existing_key_expiry_time_mins = expireMins
        };
        HttpClient httpClient = this._httpClient;
        Uri requestUri = new Uri(string.Format("{0}/object/user-secret-keys/{1}", (object)this.Client.EndPoint, (object)userName));
        userSecretKeyCreateParam secretKeyCreateParam = data;
        XmlMediaTypeFormatterECS formatter = new XmlMediaTypeFormatterECS();
        formatter.UseXmlSerializer = true;
        HttpResponseMessage resp = await httpClient.PostAsync<userSecretKeyCreateParam>(requestUri, secretKeyCreateParam, (MediaTypeFormatter)formatter, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to generate secret key. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        secretKeyInfoRep payload = await resp.Content.ReadAsAsync<secretKeyInfoRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        secretKeyInfoRep newKeyForUser = payload;
        expireMins = (string)null;
        data = (userSecretKeyCreateParam)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (secretKeyInfoRep)null;
        return newKeyForUser;
    }

    /// <summary>
    /// Gets all configured secret keys for the user account that makes the request.
    /// </summary>
    /// <returns>A secretKeyRestRep object that represents all secret keys configured to the system for calling user.</returns>
    public async Task<secretKeyRestRep> GetKeys()
    {
        HttpResponseMessage resp = await this._httpClient.GetAsync(new Uri(string.Format("{0}/object/secret-keys", (object)this.Client.EndPoint)));
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to get all secret keys. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        secretKeyRestRep payload = await resp.Content.ReadAsAsync<secretKeyRestRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        secretKeyRestRep keys = payload;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (secretKeyRestRep)null;
        return keys;
    }

    /// <summary>
    /// Create a secret key for the authenticated user that makes the request.
    /// </summary>
    /// <param name="existingKeyExpirationTime">Expiry time/date for the secret key in minutes. Note that nodes may cache old keys for up to two minutes so the old key may not expire immediately.</param>
    /// <returns>A secretKeyInfoRep object that represents the new secret key for the current user.</returns>
    public async Task<secretKeyInfoRep> CreateNewKey(
      int existingKeyExpirationTime = 0)
    {
        secretKeyCreateParam data = new secretKeyCreateParam()
        {
            existing_key_expiry_time_mins = Convert.ToString(existingKeyExpirationTime)
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<secretKeyCreateParam>(new Uri(string.Format("{0}/object/secret-keys", (object)this.Client.EndPoint)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to create secret key. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        secretKeyInfoRep payload = await resp.Content.ReadAsAsync<secretKeyInfoRep>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        secretKeyInfoRep newKey = payload;
        data = (secretKeyCreateParam)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (secretKeyInfoRep)null;
        return newKey;
    }

    /// <summary>
    /// Deletes the specified secret key for the authenticated user that makes the request.
    /// </summary>
    /// <param name="secretKey">The secret key to be deleted.</param>
    /// <returns>A boolean indicating if the secret was deleted successfully.</returns>
    public async Task<bool> DeactivateKey(string secretKey)
    {
        secretKeyDeleteParam data = new secretKeyDeleteParam()
        {
            secret_key = secretKey
        };
        HttpResponseMessage resp = await this._httpClient.PostAsync<secretKeyDeleteParam>(new Uri(string.Format("{0}/object/secret-keys/deactivate", (object)this.Client.EndPoint)), data, (MediaTypeFormatter)new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        }, "application/xml");
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
            throw new InvalidOperationException(string.Format("Failed to delete secret key. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        resp.Dispose();
        bool flag = true;
        data = (secretKeyDeleteParam)null;
        resp = (HttpResponseMessage)null;
        return flag;
    }

    /// <summary>Creates a new IAM user in ECS.</summary>
    /// <param name="nameSpace">The namespace under which to create the new IAM user.</param>
    /// <param name="iamUserName">The user name for the new IAM user.</param>
    /// <param name="permissionBoundaryArn">The ARN of the permission policy for the new IAM user.</param>
    /// <param name="tags">A list of arbitrary tags to assign to the new user. These can be used to track additional information about the IAM user and will also appear on bucket billing responses for buckets owned by the user.</param>
    /// <returns>A createIamUserResp object showing details about the IAM user.</returns>
    public async Task<createIamUserResp> CreateIamUser(
      string nameSpace,
      string iamUserName,
      string permissionBoundaryArn,
      IDictionary<string, string> tags = null)
    {
        iamUserName.AssertIsNotNullOrEmpty(nameof(iamUserName), "Unable to create IAM user with null or empty iamUserName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to create IAM user with null or empty nameSpace argument.");
        IDictionary<string, string> source = tags;
        userTagParam[] tagsArray = source != null ? source.Select<KeyValuePair<string, string>, userTagParam>((Func<KeyValuePair<string, string>, userTagParam>)(a => new userTagParam()
        {
            name = a.Key,
            value = a.Value
        })).ToArray<userTagParam>() : (userTagParam[])null;
        string path = "/";
        Dictionary<string, object> uriParams = new Dictionary<string, object>();
        uriParams.Add("Action", (object)"CreateUser");
        if (iamUserName != null)
            uriParams.Add("UserName", (object)iamUserName);
        if (path != null)
            uriParams.Add("Path", (object)path);
        if (permissionBoundaryArn != null && permissionBoundaryArn != string.Empty)
            uriParams.Add("PermissionsBoundary", (object)permissionBoundaryArn);
        if (tags != null)
            uriParams.Add("Tags", (object)tagsArray);
        string[] arrayUriParams = uriParams.Select<KeyValuePair<string, object>, string>((Func<KeyValuePair<string, object>, string>)(d => string.Format("{0}={1}", (object)d.Key, d.Value))).ToArray<string>();
        string uriParamsResult = arrayUriParams.Length != 0 ? "?" + string.Join("&", arrayUriParams) : string.Empty;
        this._httpClient.DefaultRequestHeaders.Add("x-emc-namespace", nameSpace);
        HttpResponseMessage resp = await this._httpClient.PostAsync(new Uri(string.Format("{0}/iam{1}", (object)this.Client.EndPoint, (object)uriParamsResult)), (HttpContent)null);
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
        {
            this._httpClient.DefaultRequestHeaders.Remove("x-emc-namespace");
            throw new InvalidOperationException(string.Format("Failed to create IAM user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        }
        this._httpClient.DefaultRequestHeaders.Remove("x-emc-namespace");
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        createIamUserResp payload = await resp.Content.ReadAsAsync<createIamUserResp>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        createIamUserResp iamUser = payload;
        tagsArray = (userTagParam[])null;
        path = (string)null;
        uriParams = (Dictionary<string, object>)null;
        arrayUriParams = (string[])null;
        uriParamsResult = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (createIamUserResp)null;
        return iamUser;
    }

    /// <summary>Deletes an IAM user in ECS.</summary>
    /// <param name="nameSpace">The namespace under which to create the new IAM user.</param>
    /// <param name="iamUserName">The user name for the new IAM user.</param>
    /// <returns>A deleteIamUserResp object.</returns>
    public async Task<deleteIamUserResp> DeleteIamUser(
      string nameSpace,
      string iamUserName)
    {
        iamUserName.AssertIsNotNullOrEmpty(nameof(iamUserName), "Unable to delete IAM user with null or empty iamUserName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to delete IAM user with null or empty nameSpace argument.");
        Dictionary<string, object> uriParams = new Dictionary<string, object>();
        uriParams.Add("Action", (object)"DeleteUser");
        if (iamUserName != null)
            uriParams.Add("UserName", (object)iamUserName);
        string[] arrayUriParams = uriParams.Select<KeyValuePair<string, object>, string>((Func<KeyValuePair<string, object>, string>)(d => string.Format("{0}={1}", (object)d.Key, d.Value))).ToArray<string>();
        string uriParamsResult = arrayUriParams.Length != 0 ? "?" + string.Join("&", arrayUriParams) : string.Empty;
        this._httpClient.DefaultRequestHeaders.Add("x-emc-namespace", nameSpace);
        HttpResponseMessage resp = await this._httpClient.PostAsync(new Uri(string.Format("{0}/iam{1}", (object)this.Client.EndPoint, (object)uriParamsResult)), (HttpContent)null);
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
        {
            this._httpClient.DefaultRequestHeaders.Remove("x-emc-namespace");
            throw new InvalidOperationException(string.Format("Failed to delete IAM user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        }
        this._httpClient.DefaultRequestHeaders.Remove("x-emc-namespace");
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        deleteIamUserResp payload = await resp.Content.ReadAsAsync<deleteIamUserResp>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        deleteIamUserResp deleteIamUserResp = payload;
        uriParams = (Dictionary<string, object>)null;
        arrayUriParams = (string[])null;
        uriParamsResult = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (deleteIamUserResp)null;
        return deleteIamUserResp;
    }

    /// <summary>Creates an Access Key for an IAM user in ECS.</summary>
    /// <param name="nameSpace">The namespace under which to create the new Access Key.</param>
    /// <param name="iamUserName">The name of the IAM user to create an Access Key for.</param>
    public async Task<createIamAccessKeyResp> CreateIamUserAccessKey(
      string nameSpace,
      string iamUserName)
    {
        iamUserName.AssertIsNotNullOrEmpty(nameof(iamUserName), "Unable to create an Access Key for an IAM user with null or empty iamUserName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to create an Access Key for IAM user with null or empty nameSpace argument.");
        Dictionary<string, object> uriParams = new Dictionary<string, object>();
        if (iamUserName != null)
            uriParams.Add("UserName", (object)iamUserName);
        uriParams.Add("Action", (object)"CreateAccessKey");
        string[] arrayUriParams = uriParams.Select<KeyValuePair<string, object>, string>((Func<KeyValuePair<string, object>, string>)(d => string.Format("{0}={1}", (object)d.Key, d.Value))).ToArray<string>();
        string uriParamsResult = arrayUriParams.Length != 0 ? "?" + string.Join("&", arrayUriParams) : string.Empty;
        this._httpClient.DefaultRequestHeaders.Add("x-emc-namespace", nameSpace);
        HttpResponseMessage resp = await this._httpClient.PostAsync(new Uri(string.Format("{0}/iam{1}", (object)this.Client.EndPoint, (object)uriParamsResult)), (HttpContent)null);
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
        {
            this._httpClient.DefaultRequestHeaders.Remove("x-emc-namespace");
            throw new InvalidOperationException(string.Format("Failed to create an Access Key for IAM user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        }
        this._httpClient.DefaultRequestHeaders.Remove("x-emc-namespace");
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        createIamAccessKeyResp payload = await resp.Content.ReadAsAsync<createIamAccessKeyResp>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        createIamAccessKeyResp iamUserAccessKey = payload;
        uriParams = (Dictionary<string, object>)null;
        arrayUriParams = (string[])null;
        uriParamsResult = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (createIamAccessKeyResp)null;
        return iamUserAccessKey;
    }

    /// <summary>Deletes an Access Key for an IAM user in ECS.</summary>
    /// <param name="nameSpace">The namespace under which to create the new Access Key.</param>
    /// <param name="iamUserName">The name of the IAM user to create an Access Key for.</param>
    /// <param name="iamAccessKeyId">The name of the IAM user to create an Access Key for.</param>
    public async Task<deleteIamAccessKeyResp> DeleteIamUserAccessKey(
      string nameSpace,
      string iamUserName,
      string iamAccessKeyId)
    {
        iamUserName.AssertIsNotNullOrEmpty(nameof(iamUserName), "Unable to delete an Access Key for an IAM user with null or empty iamUserName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to delete an Access Key for IAM user with null or empty nameSpace argument.");
        iamAccessKeyId.AssertIsNotNullOrEmpty(nameof(iamAccessKeyId), "Unable to delete an Access Key for IAM user with null or empty delete argument.");
        Dictionary<string, object> uriParams = new Dictionary<string, object>();
        if (iamUserName != null)
            uriParams.Add("UserName", (object)iamUserName);
        if (iamAccessKeyId != null)
            uriParams.Add("AccessKeyId", (object)iamAccessKeyId);
        uriParams.Add("Action", (object)"DeleteAccessKey");
        string[] arrayUriParams = uriParams.Select<KeyValuePair<string, object>, string>((Func<KeyValuePair<string, object>, string>)(d => string.Format("{0}={1}", (object)d.Key, d.Value))).ToArray<string>();
        string uriParamsResult = arrayUriParams.Length != 0 ? "?" + string.Join("&", arrayUriParams) : string.Empty;
        this._httpClient.DefaultRequestHeaders.Add("x-emc-namespace", nameSpace);
        HttpResponseMessage resp = await this._httpClient.PostAsync(new Uri(string.Format("{0}/iam{1}", (object)this.Client.EndPoint, (object)uriParamsResult)), (HttpContent)null);
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
        {
            this._httpClient.DefaultRequestHeaders.Remove("x-emc-namespace");
            throw new InvalidOperationException(string.Format("Failed to delete an Access Key for IAM user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        }
        this._httpClient.DefaultRequestHeaders.Remove("x-emc-namespace");
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        deleteIamAccessKeyResp payload = await resp.Content.ReadAsAsync<deleteIamAccessKeyResp>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        deleteIamAccessKeyResp iamAccessKeyResp = payload;
        uriParams = (Dictionary<string, object>)null;
        arrayUriParams = (string[])null;
        uriParamsResult = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (deleteIamAccessKeyResp)null;
        return iamAccessKeyResp;
    }

    /// <summary>Attach Managed Policy to an IAM user in ECS.</summary>
    /// <param name="nameSpace">The namespace for the IAM User.</param>
    /// <param name="iamUserName">The name of the IAM user to attach a Policy To.</param>
    /// <param name="iamPolicyArn">The ARN of the IAM User Policy.</param>
    public async Task<attachIamUserPolicyResp> AttachIamUserPolicy(
      string nameSpace,
      string iamUserName,
      string iamPolicyArn)
    {
        iamUserName.AssertIsNotNullOrEmpty(nameof(iamUserName), "Unable to attach an IAM User Policy for an IAM user with null or empty iamUserName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to attach an IAM User Policy for IAM user with null or empty nameSpace argument.");
        iamPolicyArn.AssertIsNotNullOrEmpty(nameof(iamPolicyArn), "Unable to attach an IAM User Policy for IAM user with null or empty iamPolicyArn argument.");
        Dictionary<string, object> uriParams = new Dictionary<string, object>();
        if (iamUserName != null)
            uriParams.Add("UserName", (object)iamUserName);
        if (iamPolicyArn != null)
            uriParams.Add("PolicyArn", (object)iamPolicyArn);
        uriParams.Add("Action", (object)"AttachUserPolicy");
        string[] arrayUriParams = uriParams.Select<KeyValuePair<string, object>, string>((Func<KeyValuePair<string, object>, string>)(d => string.Format("{0}={1}", (object)d.Key, d.Value))).ToArray<string>();
        string uriParamsResult = arrayUriParams.Length != 0 ? "?" + string.Join("&", arrayUriParams) : string.Empty;
        this._httpClient.DefaultRequestHeaders.Add("x-emc-namespace", nameSpace);
        HttpResponseMessage resp = await this._httpClient.PostAsync(new Uri(string.Format("{0}/iam{1}", (object)this.Client.EndPoint, (object)uriParamsResult)), (HttpContent)null);
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
        {
            this._httpClient.DefaultRequestHeaders.Remove("x-emc-namespace");
            throw new InvalidOperationException(string.Format("Failed to attach a User Policy for IAM user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        }
        this._httpClient.DefaultRequestHeaders.Remove("x-emc-namespace");
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        attachIamUserPolicyResp payload = await resp.Content.ReadAsAsync<attachIamUserPolicyResp>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        attachIamUserPolicyResp iamUserPolicyResp = payload;
        uriParams = (Dictionary<string, object>)null;
        arrayUriParams = (string[])null;
        uriParamsResult = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (attachIamUserPolicyResp)null;
        return iamUserPolicyResp;
    }

    /// <summary>Detach Managed Policy from an IAM user in ECS.</summary>
    /// <param name="nameSpace">The namespace for the IAM User.</param>
    /// <param name="iamUserName">The name of the IAM user to detach a Policy From.</param>
    /// <param name="iamPolicyArn">The ARN of the IAM User Policy.</param>
    public async Task<detachIamUserPolicyResp> DetachIamUserPolicy(
      string nameSpace,
      string iamUserName,
      string iamPolicyArn)
    {
        iamUserName.AssertIsNotNullOrEmpty(nameof(iamUserName), "Unable to detach an IAM User Policy for an IAM user with null or empty iamUserName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to detach an IAM User Policy for IAM user with null or empty nameSpace argument.");
        iamPolicyArn.AssertIsNotNullOrEmpty(nameof(iamPolicyArn), "Unable to detach an IAM User Policy for IAM user with null or empty iamPolicyArn argument.");
        Dictionary<string, object> uriParams = new Dictionary<string, object>();
        if (iamUserName != null)
            uriParams.Add("UserName", (object)iamUserName);
        if (iamPolicyArn != null)
            uriParams.Add("PolicyArn", (object)iamPolicyArn);
        uriParams.Add("Action", (object)"DetachUserPolicy");
        string[] arrayUriParams = uriParams.Select<KeyValuePair<string, object>, string>((Func<KeyValuePair<string, object>, string>)(d => string.Format("{0}={1}", (object)d.Key, d.Value))).ToArray<string>();
        string uriParamsResult = arrayUriParams.Length != 0 ? "?" + string.Join("&", arrayUriParams) : string.Empty;
        this._httpClient.DefaultRequestHeaders.Add("x-emc-namespace", nameSpace);
        HttpResponseMessage resp = await this._httpClient.PostAsync(new Uri(string.Format("{0}/iam{1}", (object)this.Client.EndPoint, (object)uriParamsResult)), (HttpContent)null);
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
        {
            this._httpClient.DefaultRequestHeaders.Remove("x-emc-namespace");
            throw new InvalidOperationException(string.Format("Failed to detach a User Policy for IAM user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        }
        this._httpClient.DefaultRequestHeaders.Remove("x-emc-namespace");
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        detachIamUserPolicyResp payload = await resp.Content.ReadAsAsync<detachIamUserPolicyResp>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        detachIamUserPolicyResp iamUserPolicyResp = payload;
        uriParams = (Dictionary<string, object>)null;
        arrayUriParams = (string[])null;
        uriParamsResult = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (detachIamUserPolicyResp)null;
        return iamUserPolicyResp;
    }

    /// <summary>Get IAM user in ECS.</summary>
    /// <param name="nameSpace">The namespace for the IAM User.</param>
    /// <param name="iamUserName">The name of the IAM user to get.</param>
    public async Task<GetUserResponse> GetIamUser(
      string nameSpace,
      string iamUserName)
    {
        iamUserName.AssertIsNotNullOrEmpty(nameof(iamUserName), "Unable to retrieve IAM User with null or empty iamUserName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to retrieve IAM User with null or empty nameSpace argument.");
        Dictionary<string, object> uriParams = new Dictionary<string, object>();
        if (iamUserName != null)
            uriParams.Add("UserName", (object)iamUserName);
        uriParams.Add("Action", (object)"GetUser");
        string[] arrayUriParams = uriParams.Select<KeyValuePair<string, object>, string>((Func<KeyValuePair<string, object>, string>)(d => string.Format("{0}={1}", (object)d.Key, d.Value))).ToArray<string>();
        string uriParamsResult = arrayUriParams.Length != 0 ? "?" + string.Join("&", arrayUriParams) : string.Empty;
        this._httpClient.DefaultRequestHeaders.Add("x-emc-namespace", nameSpace);
        HttpResponseMessage resp = await this._httpClient.PostAsync(new Uri(string.Format("{0}/iam{1}", (object)this.Client.EndPoint, (object)uriParamsResult)), (HttpContent)null);
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
        {
            this._httpClient.DefaultRequestHeaders.Remove("x-emc-namespace");
            throw new InvalidOperationException(string.Format("Failed to retrieve IAM user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        }
        this._httpClient.DefaultRequestHeaders.Remove("x-emc-namespace");
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        GetUserResponse payload = await resp.Content.ReadAsAsync<GetUserResponse>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        GetUserResponse iamUser = payload;
        uriParams = (Dictionary<string, object>)null;
        arrayUriParams = (string[])null;
        uriParamsResult = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (GetUserResponse)null;
        return iamUser;
    }

    /// <summary>List IAM Uset Access Keys.</summary>
    /// <param name="nameSpace">The namespace for the IAM User.</param>
    /// <param name="iamUserName">The name of the IAM user to retrieve access keys for.</param>
    public async Task<ListAccessKeysResponse> ListIamUserAccessKeys(
      string nameSpace,
      string iamUserName)
    {
        iamUserName.AssertIsNotNullOrEmpty(nameof(iamUserName), "Unable to retrieve access keys for an IAM User with null or empty iamUserName argument.");
        nameSpace.AssertIsNotNullOrEmpty(nameof(nameSpace), "Unable to retrieve access keys for an IAM User with null or empty nameSpace argument.");
        Dictionary<string, object> uriParams = new Dictionary<string, object>();
        if (iamUserName != null)
            uriParams.Add("UserName", (object)iamUserName);
        uriParams.Add("Action", (object)"ListAccessKeys");
        string[] arrayUriParams = uriParams.Select<KeyValuePair<string, object>, string>((Func<KeyValuePair<string, object>, string>)(d => string.Format("{0}={1}", (object)d.Key, d.Value))).ToArray<string>();
        string uriParamsResult = arrayUriParams.Length != 0 ? "?" + string.Join("&", arrayUriParams) : string.Empty;
        this._httpClient.DefaultRequestHeaders.Add("x-emc-namespace", nameSpace);
        HttpResponseMessage resp = await this._httpClient.PostAsync(new Uri(string.Format("{0}/iam{1}", (object)this.Client.EndPoint, (object)uriParamsResult)), (HttpContent)null);
        if (resp.StatusCode != HttpStatusCode.OK && resp.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
        {
            this._httpClient.DefaultRequestHeaders.Remove("x-emc-namespace");
            throw new InvalidOperationException(string.Format("Failed to retrieve access keys for IAM user. The remote server returned the following status code: '{0}'.", (object)resp.StatusCode));
        }
        this._httpClient.DefaultRequestHeaders.Remove("x-emc-namespace");
        XmlMediaTypeFormatter xmlFormatter = new XmlMediaTypeFormatter()
        {
            UseXmlSerializer = true
        };
        List<MediaTypeFormatter> formatters = new List<MediaTypeFormatter>()
      {
        (MediaTypeFormatter) xmlFormatter
      };
        ListAccessKeysResponse payload = await resp.Content.ReadAsAsync<ListAccessKeysResponse>((IEnumerable<MediaTypeFormatter>)formatters);
        resp.Dispose();
        ListAccessKeysResponse accessKeysResponse = payload;
        uriParams = (Dictionary<string, object>)null;
        arrayUriParams = (string[])null;
        uriParamsResult = (string)null;
        resp = (HttpResponseMessage)null;
        xmlFormatter = (XmlMediaTypeFormatter)null;
        formatters = (List<MediaTypeFormatter>)null;
        payload = (ListAccessKeysResponse)null;
        return accessKeysResponse;
    }
}