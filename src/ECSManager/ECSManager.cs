using DataSchemas;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Coscine.ECSManager
{
    /// <summary>
    /// Calls for the interaction with the Ecs
    /// </summary>
    public class EcsManager
    {
        /// <summary>
        /// Configuration Object for the EcsManager
        /// </summary>
        public EcsManagerConfiguration EcsManagerConfiguration { get; set; }

        /// <summary>
        /// Creates a bucket with the provided bucket name and quota [GiB].
        /// </summary>
        /// <param name="bucketName">Name of the bucket.</param>
        /// <param name="quota">Quota for the bucket in GibiBYTE [GiB].</param>
        /// <param name="retention">Default retention period in seconds [s].</param>
        /// <returns>Nothing</returns>
        /// <exception cref="Exception"></exception>
        public async Task CreateBucket(string bucketName, long quota, long retention = 0L)
        {
            if (quota == 0 || quota < EcsManagerConfiguration.BucketQuotaMin || quota > EcsManagerConfiguration.BucketQuotaMax)
            {
                throw new Exception($"The quota must be greater or equal than {EcsManagerConfiguration.BucketQuotaMin}, less or equal to {EcsManagerConfiguration.BucketQuotaMax} and should not be 0. Your quota is {quota}.");
            }

            // create new management client USING NAMESPACE ADMINISTRATOR to obtain token
            using var client = new CoscineECSManagementClient(EcsManagerConfiguration.NamespaceAdminName, EcsManagerConfiguration.NamespaceAdminPassword, EcsManagerConfiguration.ManagerApiEndpoint);
            // authenticate
            await client.Authenticate();

            // using authenticated client, obtain new instance of service client
            var service = client.CreateServiceClient();

            // create bucket for object user
            var createBucketResult = await service.CreateBucket(bucketName, EcsManagerConfiguration.ReplicationGroupId, false, "S3", EcsManagerConfiguration.NamespaceName, false, EcsManagerConfiguration.ObjectUserName, retention);
            // update bucket quota
            var updateBucketResult = await service.UpdateBucketQuota(bucketName, EcsManagerConfiguration.NamespaceName, quota, -1);

            // log out
            await client.LogOut();
        }

        /// <summary>
        /// Deletes a bucket by its bucket name.
        /// </summary>
        /// <param name="bucketName">Name of the bucket.</param>
        /// <returns>Operation success boolean.</returns>
        public async Task<bool> DeleteBucket(string bucketName)
        {
            // create new management client USING NAMESPACE ADMINISTRATOR to obtain token
            using var client = new CoscineECSManagementClient(EcsManagerConfiguration.NamespaceAdminName, EcsManagerConfiguration.NamespaceAdminPassword, EcsManagerConfiguration.ManagerApiEndpoint);
            // authenticate
            await client.Authenticate();

            // using authenticated client, obtain new instance of service client
            var service = client.CreateServiceClient();

            var deleteBucketResult = await service.DeleteBucket(bucketName, EcsManagerConfiguration.NamespaceName);

            // log out
            await client.LogOut();

            return deleteBucketResult;
        }

        /// <summary>
        /// Sets the quota for the bucket [GiB].
        /// </summary>
        /// <param name="bucketName">Name of the bucket.</param>
        /// <param name="quota">Quota for the bucket in GibiBYTE [GiB].</param>
        /// <returns>Operation success boolean.</returns>
        /// <exception cref="Exception">Quota minimum compared to quota bucket maximum.</exception>
        public async Task<bool> SetBucketQuota(string bucketName, long quota)
        {
            if (quota == 0 || quota < EcsManagerConfiguration.BucketQuotaMin || quota > EcsManagerConfiguration.BucketQuotaMax)
            {
                throw new Exception($"The quota must be greater or equal than {EcsManagerConfiguration.BucketQuotaMin}, less or equal to {EcsManagerConfiguration.BucketQuotaMax} and should not be 0. Your quota is {quota}.");
            }

            // create new management client USING NAMESPACE ADMINISTRATOR to obtain token
            using var client = new CoscineECSManagementClient(EcsManagerConfiguration.NamespaceAdminName, EcsManagerConfiguration.NamespaceAdminPassword, EcsManagerConfiguration.ManagerApiEndpoint);
            // authenticate
            await client.Authenticate();

            // using authenticated client, obtain new instance of service client
            var service = client.CreateServiceClient();

            // update bucket quota
            var updateBucketQuotaResult = await service.UpdateBucketQuota(bucketName, EcsManagerConfiguration.NamespaceName, quota, -1);

            // log out
            await client.LogOut();

            return updateBucketQuotaResult;
        }

        /// <summary>
        /// Gets the reserved quota for the bucket [GiB].
        /// </summary>
        /// <param name="bucketName">Name of the bucket.</param>
        /// <returns>Reserved bucket quota in GibiBYTE (GiB).</returns>
        public async Task<long> GetBucketQuota(string bucketName)
        {
            // create new management client USING NAMESPACE ADMINISTRATOR to obtain token
            using var client = new CoscineECSManagementClient(EcsManagerConfiguration.NamespaceAdminName, EcsManagerConfiguration.NamespaceAdminPassword, EcsManagerConfiguration.ManagerApiEndpoint);
            // authenticate
            await client.Authenticate();

            // using authenticated client, obtain new instance of service client
            var service = client.CreateServiceClient();

            long bucketQuota = 0;

            try
            {
                var getBucketQuotaResult = await service.GetBucketQuota(bucketName, EcsManagerConfiguration.NamespaceName);
                if (getBucketQuotaResult != null)
                {
                    bucketQuota = getBucketQuotaResult.blockSize; // [GiB]
                }
            }
            catch
            {
            }
            finally
            {
                await client.LogOut();
            }

            return bucketQuota;
        }

        public async Task<secretKeyInfoRep> CreateObjectUser(string userName, string secretKey)
        {
            // create new management client USING NAMESPACE ADMINISTRATOR to obtain token
            using var client = new CoscineECSManagementClient(EcsManagerConfiguration.NamespaceAdminName, EcsManagerConfiguration.NamespaceAdminPassword, EcsManagerConfiguration.ManagerApiEndpoint);
            // authenticate
            await client.Authenticate();

            // using authenticated client, obtain new instance of service client
            var service = client.CreateServiceClient();

            // Create object user
            var createObjectUserResult = await service.CreateObjectUser(userName.ToLower(), EcsManagerConfiguration.NamespaceName);

            // Update password
            var CreateNewKeyForUserResult = await service.CreateNewKeyForUser(userName.ToLower(), EcsManagerConfiguration.NamespaceName, secretKey);

            // log out
            await client.LogOut();

            return CreateNewKeyForUserResult;
        }

        public async Task<secretKeyInfoRep> CreateObjectUser(string userName)
        {
            // create new management client USING NAMESPACE ADMINISTRATOR to obtain token
            using var client = new CoscineECSManagementClient(EcsManagerConfiguration.NamespaceAdminName, EcsManagerConfiguration.NamespaceAdminPassword, EcsManagerConfiguration.ManagerApiEndpoint);
            // authenticate
            await client.Authenticate();

            // using authenticated client, obtain new instance of service client
            var service = client.CreateServiceClient();

            // Create object user
            var createObjectUserResult = await service.CreateObjectUser(userName.ToLower(), EcsManagerConfiguration.NamespaceName);

            // log out
            await client.LogOut();

            return createObjectUserResult;
        }

        public async Task<secretKeyInfoRep> UpdateObjectUserKey(string userName, string secretKey)
        {
            // create new management client USING NAMESPACE ADMINISTRATOR to obtain token
            using var client = new CoscineECSManagementClient(EcsManagerConfiguration.NamespaceAdminName, EcsManagerConfiguration.NamespaceAdminPassword, EcsManagerConfiguration.ManagerApiEndpoint);
            // authenticate
            await client.Authenticate();

            // using authenticated client, obtain new instance of service client
            var service = client.CreateServiceClient();

            // Create object user
            var createObjectUserResult = await service.CreateNewKeyForUser(userName.ToLower(), EcsManagerConfiguration.NamespaceName, secretKey);

            // log out
            await client.LogOut();

            return createObjectUserResult;
        }

        public async Task<bool> DeleteObjectUser(string userName)
        {
            // create new management client USING NAMESPACE ADMINISTRATOR to obtain token
            using var client = new CoscineECSManagementClient(EcsManagerConfiguration.NamespaceAdminName, EcsManagerConfiguration.NamespaceAdminPassword, EcsManagerConfiguration.ManagerApiEndpoint);
            // authenticate
            await client.Authenticate();

            // using authenticated client, obtain new instance of service client
            var service = client.CreateServiceClient();

            // Create object user
            var deleteObjectUserResult = await service.DeleteObjectUser(userName.ToLower(), EcsManagerConfiguration.NamespaceName);

            // log out
            await client.LogOut();

            return deleteObjectUserResult;
        }

        public async Task<userInfoRestRep> GetObjectUser(string userName)
        {
            // create new management client USING NAMESPACE ADMINISTRATOR to obtain token
            using var client = new CoscineECSManagementClient(EcsManagerConfiguration.NamespaceAdminName, EcsManagerConfiguration.NamespaceAdminPassword, EcsManagerConfiguration.ManagerApiEndpoint);
            // authenticate
            await client.Authenticate();

            // using authenticated client, obtain new instance of service client
            var service = client.CreateServiceClient();

            // Create object user
            var getObjectUserResult = await service.GetObjectUser(userName.ToLower(), EcsManagerConfiguration.NamespaceName);

            // log out
            await client.LogOut();

            return getObjectUserResult;
        }

        public async Task<bool> SetUserAcl(string userName, string bucketName, IEnumerable<string> permissions)
        {
            // create new management client USING NAMESPACE ADMINISTRATOR to obtain token
            using var client = new CoscineECSManagementClient(EcsManagerConfiguration.NamespaceAdminName, EcsManagerConfiguration.NamespaceAdminPassword, EcsManagerConfiguration.ManagerApiEndpoint);
            // authenticate
            await client.Authenticate();

            // using authenticated client, obtain new instance of service client
            var service = client.CreateServiceClient();

            // Create object user
            var getAclResult = await service.GetBucketACL(bucketName, EcsManagerConfiguration.NamespaceName);

            var owner = getAclResult.acl.owner;
            var namespaceName = getAclResult.@namespace;

            var userAcl = new Dictionary<string, IEnumerable<string>>();
            if (getAclResult.acl.user_acl != null)
            {
                foreach (var user in getAclResult.acl.user_acl)
                {
                    userAcl.Add(user.user, user.permission);
                }
            }
            var groupAcl = new Dictionary<string, IEnumerable<string>>();
            if (getAclResult.acl.group_acl != null)
            {
                foreach (var group in getAclResult.acl.group_acl)
                {
                    groupAcl.Add(group.group, group.permission);
                }
            }
            var customGroupAcl = new Dictionary<string, IEnumerable<string>>();
            if (getAclResult.acl.customgroup_acl != null)
            {
                foreach (var customgroup in getAclResult.acl.customgroup_acl)
                {
                    customGroupAcl.Add(customgroup.customgroup, customgroup.permission);
                }
            }

            userAcl[userName] = permissions;

            var getObjectUserResult = await service.SetBucketACL(bucketName, namespaceName, owner, userAcl, groupAcl, customGroupAcl);

            // log out
            await client.LogOut();

            return getObjectUserResult;
        }

        public async Task<bucketACLRep> GetAcl(string bucketName)
        {
            // create new management client USING NAMESPACE ADMINISTRATOR to obtain token
            using var client = new CoscineECSManagementClient(EcsManagerConfiguration.NamespaceAdminName, EcsManagerConfiguration.NamespaceAdminPassword, EcsManagerConfiguration.ManagerApiEndpoint);
            // authenticate
            await client.Authenticate();

            // using authenticated client, obtain new instance of service client
            var service = client.CreateServiceClient();

            // Create object user
            var getAclResult = await service.GetBucketACL(bucketName, EcsManagerConfiguration.NamespaceName);

            // log out
            await client.LogOut();

            return getAclResult;
        }

        /// <summary>
        /// Gets the total used quota for the bucket [Byte].
        /// </summary>
        /// <param name="bucketName">Name of the bucket.</param>
        /// <returns>Total used quota by all files in the bucket in BYTE (Byte).</returns>
        public async Task<long> GetBucketTotalUsedQuota(string bucketName)
        {
            // create new management client USING NAMESPACE ADMINISTRATOR to obtain token
            using var client = new CoscineECSManagementClient(EcsManagerConfiguration.NamespaceAdminName, EcsManagerConfiguration.NamespaceAdminPassword, EcsManagerConfiguration.ManagerApiEndpoint);
            // authenticate
            await client.Authenticate();

            // using authenticated client, obtain new instance of service client
            var service = client.CreateServiceClient();

            decimal bucketQuota = 0;

            try
            {
                var getBucketBillingInfo = await service.GetBucketBillingInfo(EcsManagerConfiguration.NamespaceName, bucketName, "KB");
                if (getBucketBillingInfo != null)
                {
                    bucketQuota = getBucketBillingInfo.total_size; // [KiB]
                }
            }
            catch
            {
            }
            finally
            {
                await client.LogOut();
            }

            // Value update time has a delay of 30 sec to a minute.
            return Convert.ToInt64(bucketQuota * 1024); // KiB to Byte
        }
    }
}